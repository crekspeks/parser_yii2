<?php

use yii\db\Migration;

/**
 * Class m180522_060916_addTeamColumn
 */
class m180522_060916_addTeamColumn extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `match_marks`
              ADD COLUMN `team` TINYINT NOT NULL AFTER `pic`;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180522_060916_addTeamColumn cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180522_060916_addTeamColumn cannot be reverted.\n";

        return false;
    }
    */
}
