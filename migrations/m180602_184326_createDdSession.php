<?php

use yii\db\Migration;

/**
 * Class m180602_184326_createDdSession
 */
class m180602_184326_createDdSession extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE session(
                id CHAR(40) NOT NULL PRIMARY KEY,
                expire INTEGER,
                user_id INTEGER,
                data LONGBLOB
          )
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180602_184326_createDdSession cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180602_184326_createDdSession cannot be reverted.\n";

        return false;
    }
    */
}
