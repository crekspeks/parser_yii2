<?php

use yii\db\Migration;

/**
 * Class m180621_154939_addUserSubscribe
 */
class m180621_154939_addUserSubscribe extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE `user2matches_lineups` (
                `user_id` INT NOT NULL,
                `match_id` INT NOT NULL,
                PRIMARY KEY (`user_id`, `match_id`)
            )
            ;
            ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180621_154939_addUserSubscribe cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180621_154939_addUserSubscribe cannot be reverted.\n";

        return false;
    }
    */
}
