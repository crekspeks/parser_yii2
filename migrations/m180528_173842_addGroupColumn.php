<?php

use yii\db\Migration;

/**
 * Class m180528_173842_addGroupColumn
 */
class m180528_173842_addGroupColumn extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `user_match_marks`
              ADD COLUMN `group_id` INT(11) NULL DEFAULT NULL AFTER `user_id`;
            
            ALTER TABLE `comment`
	          ADD COLUMN `group_id` INT(11) NULL DEFAULT NULL AFTER `user_id`;
	          
            INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('Admin', 1, NULL, NULL, NULL, 1527519590, 1527534564);
            INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('Group1', 1, 'Общие метки и комментарии', NULL, NULL, 1527519598, 1527533417);
            INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('Group2', 1, 'Персональные метки и комментарии', NULL, NULL, 1527532916, 1527533151);
            INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('sAdminMenu', 2, NULL, NULL, NULL, 1527519102, 1527519102);
            INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('shareMarks', 2, NULL, NULL, NULL, 1527533408, 1527533408);
            
            INSERT INTO `auth_item_child` (`parent`, `child`) VALUES ('Admin', 'sAdminMenu');
            INSERT INTO `auth_item_child` (`parent`, `child`) VALUES ('Admin', 'shareMarks');
            INSERT INTO `auth_item_child` (`parent`, `child`) VALUES ('Group1', 'shareMarks');
            
            INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('Admin', '1', 1527519631);
            INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('Group1', '1', 1527519631);
            INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('Group1', '2', 1527533094);
            INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('Group2', '3', 1527533102);
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180528_173842_addGroupColumn cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180528_173842_addGroupColumn cannot be reverted.\n";

        return false;
    }
    */
}
