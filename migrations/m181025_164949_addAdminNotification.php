<?php

use yii\db\Migration;

/**
 * Class m181025_164949_addAdminNotification
 */
class m181025_164949_addAdminNotification extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE `admin_notification` (
                `id` INT(11) NOT NULL AUTO_INCREMENT,
                `admin_id` INT(11) NULL DEFAULT NULL,
                `user_id` INT(11) NULL DEFAULT NULL,
                PRIMARY KEY (`id`)
            )
            ENGINE=InnoDB
            ;

        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181025_164949_addAdminNotification cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181025_164949_addAdminNotification cannot be reverted.\n";

        return false;
    }
    */
}
