<?php

use yii\db\Migration;

/**
 * Class m180522_063832_fixTables
 */
class m180522_063832_fixTables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
          ALTER TABLE `match_marks`
            ADD UNIQUE INDEX `player_id_match_id` (`player_id`, `match_id`);
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180522_063832_fixTables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180522_063832_fixTables cannot be reverted.\n";

        return false;
    }
    */
}
