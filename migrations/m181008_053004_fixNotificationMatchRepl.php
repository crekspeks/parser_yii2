<?php

use yii\db\Migration;

/**
 * Class m181008_053004_fixNotificationMatchRepl
 */
class m181008_053004_fixNotificationMatchRepl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `user2teams_replacements`
                ALTER `team_replacement_id` DROP DEFAULT;
            ALTER TABLE `user2teams_replacements`
                CHANGE COLUMN `team_replacement_id` `replacement_match_id` INT(11) NOT NULL AFTER `user_id`;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181008_053004_fixNotificationMatchRepl cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181008_053004_fixNotificationMatchRepl cannot be reverted.\n";

        return false;
    }
    */
}
