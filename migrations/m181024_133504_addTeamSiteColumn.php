<?php

use yii\db\Migration;

/**
 * Class m181024_133504_addTeamSiteColumn
 */
class m181024_133504_addTeamSiteColumn extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
        ALTER TABLE `team`
            ADD COLUMN `team_site` VARCHAR(255) NULL DEFAULT NULL AFTER `besoccer_name`;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181024_133504_addTeamSiteColumn cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181024_133504_addTeamSiteColumn cannot be reverted.\n";

        return false;
    }
    */
}
