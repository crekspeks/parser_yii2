<?php

use yii\db\Migration;

/**
 * Class m180712_211759_addLineupsMarks
 */
class m180712_211759_addLineupsMarks extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
        ALTER TABLE `matches_lineups`
            ADD COLUMN `besoccer_name_mark_team_1` TINYINT(1) NOT NULL DEFAULT '0' AFTER `date`,
            ADD COLUMN `besoccer_name_mark_team_2` TINYINT(1) NOT NULL DEFAULT '0' AFTER `besoccer_name_mark_team_1`,
            ADD COLUMN `sofascore_name_mark_team_1` TINYINT(1) NOT NULL DEFAULT '0' AFTER `besoccer_name_mark_team_2`,
            ADD COLUMN `sofascore_name_mark_team_2` TINYINT(1) NOT NULL DEFAULT '0' AFTER `sofascore_name_mark_team_1`;
            
        ALTER TABLE `user`
	        ADD COLUMN `lineups_subscribe` TINYINT(1) NOT NULL DEFAULT '0' AFTER `timezone`;

        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180712_211759_addLineupsMarks cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180712_211759_addLineupsMarks cannot be reverted.\n";

        return false;
    }
    */
}
