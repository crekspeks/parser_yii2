<?php

use yii\db\Migration;

/**
 * Class m180608_212407_addTransfermarktColumn
 */
class m180608_212407_addTransfermarktColumn extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('team', 'transfermarkt_name', 'varchar(255)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180608_212407_addTransfermarktColumn cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180608_212407_addTransfermarktColumn cannot be reverted.\n";

        return false;
    }
    */
}
