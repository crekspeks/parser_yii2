<?php

use yii\db\Migration;

/**
 * Class m181021_132457_addSysTable
 */
class m181021_132457_addSysTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE `system` (
                `id` INT NOT NULL AUTO_INCREMENT,
                `param` VARCHAR(50) NOT NULL,
                `status` TINYINT NOT NULL DEFAULT '0',
                PRIMARY KEY (`id`)
            )
            COLLATE='utf8_general_ci'
            ;

          INSERT INTO `pars`.`system` (`param`) VALUES ('check-lineups');
          INSERT INTO `pars`.`system` (`param`) VALUES ('check-incidents');
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181021_132457_addSysTable cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181021_132457_addSysTable cannot be reverted.\n";

        return false;
    }
    */
}
