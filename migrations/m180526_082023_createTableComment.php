<?php

use yii\db\Migration;

/**
 * Class m180526_082023_createTableComment
 */
class m180526_082023_createTableComment extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%comment}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'match_id' => $this->integer(),
            'team' => $this->integer(1)->notNull(),
            'text' => $this->text()->notNull(),
            'date_create' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->execute("
            ALTER TABLE `comment`
                ADD CONSTRAINT `FK_comment_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
                ADD CONSTRAINT `FK_comment_match` FOREIGN KEY (`match_id`) REFERENCES `match` (`id`) ON DELETE CASCADE;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180526_082023_createTableComment cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180526_082023_createTableComment cannot be reverted.\n";

        return false;
    }
    */
}
