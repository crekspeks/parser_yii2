<?php

use yii\db\Migration;

/**
 * Class m180607_055447_addTeamTable
 */
class m180607_055447_addTeamTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%team}}', [
            'id' => $this->primaryKey(),
            'soccerway_id' => $this->integer()->notNull(),
            'soccerway_url' => $this->string(255)->notNull(),
            'betexplorer_name' => $this->string(255),
            'oddsportal_name' => $this->string(255),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%team}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180607_055447_addTeamTable cannot be reverted.\n";

        return false;
    }
    */
}
