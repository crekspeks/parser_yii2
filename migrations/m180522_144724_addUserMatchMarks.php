<?php

use yii\db\Migration;

/**
 * Class m180522_144724_addUserMatchMarks
 */
class m180522_144724_addUserMatchMarks extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE `user_match_marks` (
                `id` INT NOT NULL AUTO_INCREMENT,
                `match_id` INT NULL,
                `user_id` INT NULL,
                `marks` TEXT NOT NULL,
                PRIMARY KEY (`id`)
            )
            COLLATE='utf8_general_ci';
            
            ALTER TABLE `user_match_marks`
                ADD CONSTRAINT `FK_user_match_marks_match` FOREIGN KEY (`match_id`) REFERENCES `match` (`id`) ON DELETE CASCADE,
                ADD CONSTRAINT `FK_user_match_marks_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180522_144724_addUserMatchMarks cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180522_144724_addUserMatchMarks cannot be reverted.\n";

        return false;
    }
    */
}
