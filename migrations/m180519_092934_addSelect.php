<?php

use yii\db\Migration;

/**
 * Class m180519_092934_addSelect
 */
class m180519_092934_addSelect extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%select}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'url' => $this->string(255)->notNull(),
            'parent_id' => $this->integer()->null(),
        ], $tableOptions);

        $this->createIndex('idx-parent_id-id', '{{%select}}', 'parent_id');
        $this->addForeignKey(
            'fk-parent_id-id',
            '{{%select}}',
            'parent_id',
            '{{%select}}',
            'id',
            'CASCADE'
        );


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-parent_id-id', '{{%select}}');
        $this->dropIndex('idx-parent_id-id', '{{%select}}');
        $this->dropTable('{{%select}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180519_092934_addSelect cannot be reverted.\n";

        return false;
    }
    */
}
