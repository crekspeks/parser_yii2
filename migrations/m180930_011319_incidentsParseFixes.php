<?php

use yii\db\Migration;

/**
 * Class m180930_011319_incidentsParseFixes
 */
class m180930_011319_incidentsParseFixes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `select`
            	ADD COLUMN `yellow_cards_set` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `parent_id`;
            ALTER TABLE `select`
	            ADD COLUMN `replacements_parse` TINYINT(1) NULL DEFAULT '0' AFTER `yellow_cards_set`;
        ");

        $this->execute("
            CREATE TABLE `user2teams_replacements` (
            `user_id` INT(11) NOT NULL,
            `team_replacement_id` INT(11) NOT NULL,
            `status` TINYINT(4) NOT NULL DEFAULT '0',
            PRIMARY KEY (`user_id`, `team_replacement_id`)
        )
        ENGINE=InnoDB
        ;
        ");

        $this->execute("
            INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('incidentsMatches', 2, 'Инструмент событий матча', NULL, NULL, 1538242591, 1538242591);
            INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('Incidents', 1, 'Доступ к инструменту событий матча', NULL, NULL, 1538242662, 1538242702);
            INSERT INTO `auth_item_child` (`parent`, `child`) VALUES ('Incidents', 'addForeignName');
            INSERT INTO `auth_item_child` (`parent`, `child`) VALUES ('Incidents', 'incidentsMatches');
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180930_011319_incidentsParseFixes cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180930_011319_incidentsParseFixes cannot be reverted.\n";

        return false;
    }
    */
}
