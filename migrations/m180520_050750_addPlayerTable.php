<?php

use yii\db\Migration;

/**
 * Class m180520_050750_addPlayerTable
 */
class m180520_050750_addPlayerTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%player}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'url' => $this->string(255)->notNull(),
            'player_id' => $this->integer()->notNull(),
            'flag' => $this->string(255)->notNull(),
            'minutes_played' => $this->integer()->notNull(),
            'appearances' => $this->integer()->notNull(),
            'age' => $this->integer(3)->notNull(),
            'position' => $this->string(1)->notNull(),
            'lineups' => $this->integer()->notNull(),
            'subs_on_bench' => $this->integer()->notNull(),
            'goals' => $this->integer()->notNull(),
            'assists'=> $this->integer()->notNull(),
            'yellow_cards' => $this->integer()->notNull(),
            'nd_yellow_cards' => $this->integer()->notNull(),
            'red_cards' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%player}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180520_050750_addPlayerTable cannot be reverted.\n";

        return false;
    }
    */
}
