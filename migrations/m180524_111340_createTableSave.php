<?php

use yii\db\Migration;

/**
 * Class m180524_111340_createTableSave
 */
class m180524_111340_createTableSave extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%favorite_matches}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'match_id' => $this->integer(),
            'url' => $this->string(),
        ], $tableOptions);

        $this->execute("
            ALTER TABLE `favorite_matches`
                ADD CONSTRAINT `FK_favorite_matches_match` FOREIGN KEY (`match_id`) REFERENCES `match` (`id`) ON DELETE CASCADE,
                ADD CONSTRAINT `FK_favorite_matches_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180524_111340_createTableSave cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180524_111340_createTableSave cannot be reverted.\n";

        return false;
    }
    */
}
