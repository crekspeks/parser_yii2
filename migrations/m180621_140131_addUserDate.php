<?php

use yii\db\Migration;

/**
 * Class m180621_140131_addUserDate
 */
class m180621_140131_addUserDate extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
        ALTER TABLE `user`
          ADD COLUMN `active_until` INT(11) NOT NULL AFTER `updated_at`;
          
          INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('Lineups', 1, 'Доступ к инструменту стартовых составов', NULL, NULL, 1529589992, 1529589992);
          INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('lineupsMatches', 2, 'Инструмент стартовых составов', NULL, NULL, 1529589938, 1529589938);
          
          INSERT INTO `auth_item_child` (`parent`, `child`) VALUES ('Admin', 'lineupsMatches');
          INSERT INTO `auth_item_child` (`parent`, `child`) VALUES ('Lineups', 'lineupsMatches');
        
        
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180621_140131_addUserDate cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180621_140131_addUserDate cannot be reverted.\n";

        return false;
    }
    */
}
