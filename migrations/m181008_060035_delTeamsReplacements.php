<?php

use yii\db\Migration;

/**
 * Class m181008_060035_delTeamsReplacements
 */
class m181008_060035_delTeamsReplacements extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            DROP TABLE `teams_replacements`;
            
            ALTER TABLE `replacement_matches`
                ADD COLUMN `team_1_incidents` TEXT NULL AFTER `team_2_yellow_cards`,
                ADD COLUMN `team_2_incidents` TEXT NULL AFTER `team_1_incidents`;

        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181008_060035_delTeamsReplacements cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181008_060035_delTeamsReplacements cannot be reverted.\n";

        return false;
    }
    */
}
