<?php

use yii\db\Migration;

/**
 * Class m180520_114233_addMatchMarksTable
 */
class m180520_114233_addMatchMarksTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%match_marks}}', [
            'id' => $this->primaryKey(),
            'player_id' => $this->integer(),
            'match_id' => $this->integer(),
            'name_color' => $this->string(),
            'black_dot' => $this->tinyInteger()->notNull()->defaultValue(0),
            'light_blue_dot' => $this->tinyInteger()->notNull()->defaultValue(0),
            'blue_dot' => $this->tinyInteger()->notNull()->defaultValue(0),
            'red_dot' => $this->tinyInteger()->notNull()->defaultValue(0),
            'pic' => $this->string(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%match_marks}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180520_114233_addMatchMarksTable cannot be reverted.\n";

        return false;
    }
    */
}
