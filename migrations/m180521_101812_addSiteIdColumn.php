<?php

use yii\db\Migration;

/**
 * Class m180521_101812_addSiteIdColumn
 */
class m180521_101812_addSiteIdColumn extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `match`
                ADD COLUMN `site_id` VARCHAR(100) NOT NULL AFTER `id_club_2`;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('match', 'site_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180521_101812_addSiteIdColumn cannot be reverted.\n";

        return false;
    }
    */
}
