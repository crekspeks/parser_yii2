<?php

use yii\db\Migration;

/**
 * Class m180928_194932_addTeamReplacementsTable
 */
class m180928_194932_addTeamReplacementsTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE `teams_replacements` (
                `id` INT(11) NOT NULL AUTO_INCREMENT,
                `team_id` INT(11) NOT NULL,
                `match_id` INT(11) NOT NULL,
                `incidents` TEXT NULL,
                PRIMARY KEY (`id`)
            )
            ENGINE=InnoDB
            ;
        ");

        $this->execute("
        ALTER TABLE `teams_replacements`
            ADD CONSTRAINT `FK_teams_replacements_replacement_matches` FOREIGN KEY (`match_id`) REFERENCES `replacement_matches` (`id`) ON DELETE CASCADE;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180928_194932_addTeamReplacementsTable cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180928_194932_addTeamReplacementsTable cannot be reverted.\n";

        return false;
    }
    */
}
