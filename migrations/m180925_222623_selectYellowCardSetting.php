<?php

use yii\db\Migration;

/**
 * Class m180925_222623_selectYellowCardSetting
 */
class m180925_222623_selectYellowCardSetting extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE `replacement_matches` (
                `id` INT(11) NOT NULL AUTO_INCREMENT,
                `soccerway_id` INT(11) NOT NULL,
                `team_1` VARCHAR(250) NOT NULL,
                `team_1_id` INT(11) NOT NULL,
                `team_2` VARCHAR(250) NOT NULL,
                `team_2_id` INT(11) NOT NULL,
                `url` VARCHAR(250) NOT NULL,
                `start_time` INT(11) NOT NULL,
                `select_id` INT(11) NOT NULL,
                `team_1_yellow_cards` TEXT NULL,
                `team_2_yellow_cards` TEXT NULL,
                PRIMARY KEY (`id`),
                UNIQUE INDEX `soccerway_id` (`soccerway_id`),
                INDEX `FK_replacement_matches_select` (`select_id`)
            )
            ENGINE=InnoDB
            ;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180925_222623_selectYellowCardSetting cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180925_222623_selectYellowCardSetting cannot be reverted.\n";

        return false;
    }
    */
}
