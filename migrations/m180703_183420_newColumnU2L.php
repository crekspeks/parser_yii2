<?php

use yii\db\Migration;

/**
 * Class m180703_183420_newColumnU2L
 */
class m180703_183420_newColumnU2L extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user2matches_lineups', 'status', "TINYINT NOT NULL DEFAULT '0'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180703_183420_newColumnU2L cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180703_183420_newColumnU2L cannot be reverted.\n";

        return false;
    }
    */
}
