<?php

use yii\db\Migration;

/**
 * Class m181009_182817_newTeamYellowCards
 */
class m181009_182817_newTeamYellowCards extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE `team_yellow_cards` (
                `id` INT NOT NULL AUTO_INCREMENT,
                `team_id` INT NOT NULL,
                `select_id` INT NOT NULL,
                `cards` TEXT NULL DEFAULT NULL,
                `last_check_time` INT(11) NOT NULL,
                PRIMARY KEY (`id`)
            )
            COLLATE='utf8_general_ci'
            ;
            
            ALTER TABLE `replacement_matches`
                DROP COLUMN `team_1_yellow_cards`,
                DROP COLUMN `team_2_yellow_cards`;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181009_182817_newTeamYellowCards cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181009_182817_newTeamYellowCards cannot be reverted.\n";

        return false;
    }
    */
}
