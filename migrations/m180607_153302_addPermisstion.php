<?php

use yii\db\Migration;

/**
 * Class m180607_153302_addPermisstion
 */
class m180607_153302_addPermisstion extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('addForeignName', 2, 'Возможность добавлять и менять названия команд, по которым будут искаться матчи на сторонних сайтах.', NULL, NULL, 1528385749, 1528385749);
            INSERT INTO `auth_item_child` (`parent`, `child`) VALUES ('Group1', 'addForeignName');
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180607_153302_addPermisstion cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180607_153302_addPermisstion cannot be reverted.\n";

        return false;
    }
    */
}
