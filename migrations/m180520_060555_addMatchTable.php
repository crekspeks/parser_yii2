<?php

use yii\db\Migration;

/**
 * Class m180520_060555_addMatchTable
 */
class m180520_060555_addMatchTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%match}}', [
            'id' => $this->primaryKey(),
            'team_1' => $this->string(255)->notNull(),
            'team_2' => $this->string(255)->notNull(),
            'id_club_1' => $this->integer()->notNull(),
            'id_club_2' => $this->integer()->notNull(),
            'date_game' => $this->dateTime()->notNull(),
            'date_save' => $this->dateTime()->notNull(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%match}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180520_060555_addMatchTable cannot be reverted.\n";

        return false;
    }
    */
}
