<?php

use yii\db\Migration;

/**
 * Class m180522_060033_changeDateFields
 */
class m180522_060033_changeDateFields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
        ALTER TABLE `match`
            ALTER `date_game` DROP DEFAULT,
            ALTER `date_save` DROP DEFAULT;
        ALTER TABLE `match`
            CHANGE COLUMN `date_game` `date_game` INT NOT NULL AFTER `site_id`,
            CHANGE COLUMN `date_save` `date_save` INT NOT NULL AFTER `date_game`;

        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180522_060033_changeDateFields cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180522_060033_changeDateFields cannot be reverted.\n";

        return false;
    }
    */
}
