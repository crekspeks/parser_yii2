<?php

use yii\db\Migration;

/**
 * Class m180804_150000_fixUser
 */
class m180804_150000_fixUser extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `user`
            	CHANGE COLUMN `active_until` `active_until` INT(11) NOT NULL DEFAULT '0' AFTER `updated_at`;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180804_150000_fixUser cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180804_150000_fixUser cannot be reverted.\n";

        return false;
    }
    */
}
