<?php

use yii\db\Migration;

/**
 * Class m180731_112051_addPlayerNamesColumn
 */
class m180731_112051_addPlayerNamesColumn extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
        ALTER TABLE `player`
          ADD COLUMN `names` TEXT NULL AFTER `red_cards`;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180731_112051_addPlayerNamesColumn cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180731_112051_addPlayerNamesColumn cannot be reverted.\n";

        return false;
    }
    */
}
