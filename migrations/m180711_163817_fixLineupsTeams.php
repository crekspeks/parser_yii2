<?php

use yii\db\Migration;

/**
 * Class m180711_163817_fixLineupsTeams
 */
class m180711_163817_fixLineupsTeams extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
        ALTER TABLE `team`
	      ADD UNIQUE INDEX `soccerway_id` (`soccerway_id`);
        
        ALTER TABLE `matches_lineups`
            ADD COLUMN `team_1_id` INT NOT NULL AFTER `team_1`,
            ADD COLUMN `team_2_id` INT NOT NULL AFTER `team_2`;
            
        ALTER TABLE `team`
            ADD COLUMN `sofascore_name` VARCHAR(255) NULL DEFAULT NULL AFTER `transfermarkt_name`,
            ADD COLUMN `besoccer_name` VARCHAR(255) NULL DEFAULT NULL AFTER `sofascore_name`;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180711_163817_fixLineupsTeams cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180711_163817_fixLineupsTeams cannot be reverted.\n";

        return false;
    }
    */
}
