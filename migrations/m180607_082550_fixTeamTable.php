<?php

use yii\db\Migration;

/**
 * Class m180607_082550_fixTeamTable
 */
class m180607_082550_fixTeamTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('team', 'soccerway_url');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180607_082550_fixTeamTable cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180607_082550_fixTeamTable cannot be reverted.\n";

        return false;
    }
    */
}
