<?php

use yii\db\Migration;

/**
 * Class m180709_133635_addUserTimezone
 */
class m180709_133635_addUserTimezone extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'timezone', 'VARCHAR(50) NOT NULL DEFAULT \'Europe/Moscow\'');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180709_133635_addUserTimezone cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180709_133635_addUserTimezone cannot be reverted.\n";

        return false;
    }
    */
}
