<?php

use yii\db\Migration;

/**
 * Class m180521_031713_addMatchMarksForeignKey
 */
class m180521_031713_addMatchMarksForeignKey extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
          ALTER TABLE `match_marks`
            ADD CONSTRAINT `FK_match_marks_player` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`) ON DELETE CASCADE,
            ADD CONSTRAINT `FK_match_marks_match` FOREIGN KEY (`match_id`) REFERENCES `match` (`id`) ON DELETE CASCADE;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute("
        ALTER TABLE `match_marks`
            DROP INDEX `FK_match_marks_player`,
            DROP INDEX `FK_match_marks_match`,
            DROP FOREIGN KEY `FK_match_marks_match`,
            DROP FOREIGN KEY `FK_match_marks_player`;
        ");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180521_031713_addMatchMarksForeignKey cannot be reverted.\n";

        return false;
    }
    */
}
