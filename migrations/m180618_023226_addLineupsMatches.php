<?php

use yii\db\Migration;

/**
 * Class m180618_023226_addLineupsMatches
 */
class m180618_023226_addLineupsMatches extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("

            DROP TABLE `select`;
            CREATE TABLE `select` (
                `id` INT(11) NOT NULL AUTO_INCREMENT,
                `name` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
                `url` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
                `site_id` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                `lineups_parse` TINYINT(1) NOT NULL DEFAULT '1',
                `parent_id` INT(11) NULL DEFAULT NULL,
                PRIMARY KEY (`id`),
                INDEX `idx-parent_id-id` (`parent_id`),
                CONSTRAINT `fk-parent_id-id` FOREIGN KEY (`parent_id`) REFERENCES `select` (`id`) ON DELETE CASCADE
            )
            COLLATE='utf8_unicode_ci';
            
            
          CREATE TABLE `matches_lineups` (
            `id` INT(11) NOT NULL AUTO_INCREMENT,
            `team_1` VARCHAR(255) NOT NULL,
            `team_2` VARCHAR(255) NOT NULL,
            `url` VARCHAR(255) NOT NULL,
            `soccerway_id` INT(11) NOT NULL,
            `team_1_changes` INT(11) NULL DEFAULT NULL,
            `team_2_changes` INT(11) NULL DEFAULT NULL,
            `source` VARCHAR(255) NULL DEFAULT NULL,
            `players_json` MEDIUMTEXT NULL,
            `select_id` INT(11) NULL DEFAULT NULL,
            `date` INT(11) NOT NULL,
            PRIMARY KEY (`id`),
            UNIQUE INDEX `soccerway_id` (`soccerway_id`),
            INDEX `FK_matches_lineups_select` (`select_id`),
            CONSTRAINT `FK_matches_lineups_select` FOREIGN KEY (`select_id`) REFERENCES `select` (`id`) ON DELETE CASCADE
        )
        COLLATE='utf8_general_ci'
        ;


        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180618_023226_addLineupsMatches cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180618_023226_addLineupsMatches cannot be reverted.\n";

        return false;
    }
    */
}
