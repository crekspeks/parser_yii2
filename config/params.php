<?php

return [
    'mail_from' => 'noreply@matchtoolpro.com',
    'site_url' => 'matchtoolpro.com',
    'adminEmail' => 'admin@example.com',
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
    ],
];
