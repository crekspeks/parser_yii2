<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\components\LineupsParser;
use app\components\ReplacementParser;
use app\components\ReplacementsMatchParser;
use app\models\ReplacementMatches;
use app\models\Select;
use app\models\System;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";

        return ExitCode::OK;
    }

    public function actionGenSelect()
    {
        LineupsParser::getMatches();
    }

    public function actionDelOld()
    {
        LineupsParser::delOldMatches();
    }

    public function actionCheckLineups()
    {
        $systemParam = System::findOne(['param' => 'check-lineups']);
        if ($systemParam->status !== 1) {
            try {
                $systemParam->status = 1;
                $systemParam->save();
                LineupsParser::checkLineups();
            } catch (\Exception $e) {
                throw $e;
            } finally {
                $systemParam->status = 0;
                $systemParam->save();
            }
        }
    }

    public function actionGetIncidentsMatches()
    {
        $selects = Select::findAll(['replacements_parse' => 1]);
        foreach ($selects as $select) {
            ReplacementParser::getMatches($select);
        }
    }

    public function actionCheckIncidents()
    {
        $systemParam = System::findOne(['param' => 'check-incidents']);
        if ($systemParam->status !== 1) {
            try {
                $systemParam->status = 1;
                $systemParam->save();
                $matches = ReplacementMatches::find()->where(['and', ['<', 'start_time', time()], ['>', 'start_time', time() - (120 * 60)]])->all();
                foreach ($matches as $match) {
                    $replacements = new ReplacementsMatchParser($match);
                    $replacements->check();
                }
            } catch (\Exception $e) {
                throw $e;
            } finally {
                $systemParam->status = 0;
                $systemParam->save();
            }
        }
    }

    public function actionDelOldReplace()
    {
        $ids = ReplacementMatches::find()
            ->select('id')
            ->where(['<', 'start_time', time() - (130 * 60)])
            ->column();
        ReplacementMatches::deleteAll(['in', 'id', $ids]);
        \Yii::$app->db->createCommand()
            ->delete('user2teams_replacements', ['in', 'replacement_match_id', $ids])
            ->execute();
    }
}
