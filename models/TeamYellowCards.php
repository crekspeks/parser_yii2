<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "team_yellow_cards".
 *
 * @property int $id
 * @property int $team_id
 * @property int $select_id
 * @property string $cards
 * @property int $last_check_time
 */
class TeamYellowCards extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'team_yellow_cards';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['team_id', 'select_id', 'last_check_time'], 'required'],
            [['team_id', 'select_id', 'last_check_time'], 'integer'],
            [['cards'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'team_id' => 'Team ID',
            'select_id' => 'Select ID',
            'cards' => 'Cards',
            'last_check_time' => 'Last Check Time',
        ];
    }

    public function needUpdate()
    {
        if ($this->last_check_time < time() - (20 * 60 * 60)) {
            return true;
        }
        return false;
    }
}
