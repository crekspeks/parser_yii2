<?php

namespace app\models;

use app\components\LineupsMatchParser;
use Yii;

/**
 * This is the model class for table "player".
 *
 * @property int $id
 * @property string $name
 * @property string $url
 * @property int $player_id
 * @property string $flag
 * @property int $minutes_played
 * @property int $appearances
 * @property int $age
 * @property string $position
 * @property int $lineups
 * @property int $subs_on_bench
 * @property int $goals
 * @property int $assists
 * @property int $yellow_cards
 * @property int $nd_yellow_cards
 * @property int $red_cards
 * @property string $names
 *
 * @property MatchMarks[] $matchMarks
 */
class Player extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'player';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'url', 'player_id', 'flag', 'minutes_played', 'appearances', 'age', 'position', 'lineups', 'subs_on_bench', 'goals', 'assists', 'yellow_cards', 'nd_yellow_cards', 'red_cards'], 'required'],
            [['player_id', 'minutes_played', 'appearances', 'age', 'lineups', 'subs_on_bench', 'goals', 'assists', 'yellow_cards', 'nd_yellow_cards', 'red_cards'], 'integer'],
            [['name', 'url', 'flag'], 'string', 'max' => 255],
            [['position', 'names'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'url' => 'Url',
            'player_id' => 'Player ID',
            'flag' => 'Flag',
            'minutes_played' => 'Minutes Played',
            'appearances' => 'Appearances',
            'age' => 'Age',
            'position' => 'Position',
            'lineups' => 'Lineups',
            'subs_on_bench' => 'Subs On Bench',
            'goals' => 'Goals',
            'assists' => 'Assists',
            'yellow_cards' => 'Yellow Cards',
            'nd_yellow_cards' => 'Nd Yellow Cards',
            'red_cards' => 'Red Cards',
            'names' => 'Names',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatchMarks()
    {
        return $this->hasMany(MatchMarks::className(), ['player_id' => 'id']);
    }

    static public function checkName($name, $nameArr)
    {
        if (in_array($name, $nameArr)) {
            return true;
        }
        if (in_array(LineupsMatchParser::normolizeName($name), $nameArr)) {
            return true;
        }

        return false;
    }
}
