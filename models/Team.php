<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "team".
 *
 * @property int $id
 * @property int $soccerway_id
 * @property string $betexplorer_name
 * @property string $oddsportal_name
 * @property string $transfermarkt_name
 * @property string $sofascore_name
 * @property string $besoccer_name
 * @property string $team_site
 */
class Team extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'team';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['soccerway_id'], 'required'],
            [['soccerway_id'], 'integer'],
            [['betexplorer_name', 'oddsportal_name', 'transfermarkt_name', 'sofascore_name', 'besoccer_name', 'team_site'], 'string', 'max' => 255],
            [['soccerway_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'soccerway_id' => 'Soccerway ID',
            'betexplorer_name' => 'Betexplorer Name',
            'oddsportal_name' => 'Oddsportal Name',
            'transfermarkt_name' => 'Transfermarkt Name',
            'sofascore_name' => 'Sofascore Name',
            'besoccer_name' => 'Besoccer Name',
            'team_site' => 'Team Site',
        ];
    }
}
