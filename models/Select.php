<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "select".
 *
 * @property int $id
 * @property string $name
 * @property string $url
 * @property string $site_id
 * @property int $lineups_parse
 * @property int $replacements_parse
 * @property int $parent_id
 * @property string $yellow_cards_set
 *
 * @property MatchesLineups[] $matchesLineups
 * @property Select $parent
 * @property Select[] $selects
 */
class Select extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'select';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'url'], 'required'],
            [['lineups_parse', 'parent_id', 'replacements_parse'], 'integer'],
            [['name', 'url', 'site_id', 'yellow_cards_set'], 'string', 'max' => 255],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Select::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'url' => 'Url',
            'site_id' => 'Site ID',
            'lineups_parse' => 'Lineups Parse',
            'parent_id' => 'Parent ID',
            'yellow_cards_set' => 'Yellow Cards Set',
            'replacements_parse' => 'Replacements Parse',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Select::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSelects()
    {
        return $this->hasMany(Select::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatchesLineups()
    {
        return $this->hasMany(MatchesLineups::className(), ['select_id' => 'id']);
    }

    public static function genSelectArray()
    {
        $firstLvl = self::findAll(['parent_id' => null]);
        $selectArr = [];
        foreach ($firstLvl as $firstLvlItem) {
            $secondLvlArr = [];
            foreach ($firstLvlItem->selects as $secondLvlItem) {
                $thirdLvlArr = [];
                foreach ($secondLvlItem->selects as $thirdLvlItem){
                    $thirdLvlArr[] = ['label' => $thirdLvlItem->name, 'url' => $thirdLvlItem->url . '/' . $thirdLvlItem->site_id];
                }
                $secondLvlArr[] = ['label' => $secondLvlItem->name, 'items' => $thirdLvlArr];
            }
            $selectArr[] = ['label' => $firstLvlItem->name, 'items' => $secondLvlArr];
        }
        return $selectArr;
    }
}
