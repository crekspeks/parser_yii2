<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "match_marks".
 *
 * @property int $id
 * @property int $player_id
 * @property int $match_id
 * @property string $name_color
 * @property int $black_dot
 * @property int $light_blue_dot
 * @property int $blue_dot
 * @property int $red_dot
 * @property string $pic
 * @property int $team
 *
 * @property Match $match
 * @property Player $player
 */
class MatchMarks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_marks';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['player_id', 'match_id', 'black_dot', 'light_blue_dot', 'blue_dot', 'red_dot', 'team'], 'integer'],
            [['name_color', 'pic'], 'string', 'max' => 255],
            [['match_id'], 'exist', 'skipOnError' => true, 'targetClass' => Match::class, 'targetAttribute' => ['match_id' => 'id']],
            [['player_id'], 'exist', 'skipOnError' => true, 'targetClass' => Player::class, 'targetAttribute' => ['player_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'player_id' => 'Player ID',
            'match_id' => 'Match ID',
            'name_color' => 'Name Color',
            'black_dot' => 'Black Dot',
            'light_blue_dot' => 'Light Blue Dot',
            'blue_dot' => 'Blue Dot',
            'red_dot' => 'Red Dot',
            'pic' => 'Pic',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatch()
    {
        return $this->hasOne(Match::class, ['id' => 'match_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayer()
    {
        return $this->hasOne(Player::class, ['id' => 'player_id']);
    }
}
