<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "system".
 *
 * @property int $id
 * @property string $param
 * @property int $status
 */
class System extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'system';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['param'], 'required'],
            [['status'], 'integer'],
            [['param'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'param' => 'Param',
            'status' => 'Status',
        ];
    }
}
