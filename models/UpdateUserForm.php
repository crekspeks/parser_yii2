<?php

namespace app\models;


use yii\base\Model;
use app\models\User;

/**
 * Class UpdateUserForm
 * @package backend\models
 *
 * @property User $objUser
 */
class UpdateUserForm extends Model
{
    private $objUser;

    public $id;
    public $username;
    public $email;
    public $password;
    public $status;
    public $active_until;
    public $timezone;

    public $isNewRecord = false;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            [
                'username',
                'unique',
                'targetClass' => '\app\models\User',
                'message' => 'This username has already been taken.',
                'filter' => ['not', ['id' => $this->id]]
            ],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            [
                'email',
                'unique',
                'targetClass' => '\app\models\User',
                'message' => 'This email address has already been taken.',
                'filter' => ['not', ['id' => $this->id]]
            ],

            ['password', 'string', 'min' => 6],
            ['timezone', 'string'],
            [['status', 'lineups_subscribe'], 'integer'],
            [['active_until'], 'safe'],
        ];
    }

    public function setAttr(User $user)
    {
        $this->objUser = $user;
        $this->id = $user->id;
        $this->username = $user->username;
        $this->email = $user->email;
        $this->status = $user->status;
        $this->timezone = $user->timezone;
        $this->active_until = (new \DateTime('@'.$user->active_until))->format('j-M-Y H:i');
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function update()
    {
        if (!$this->validate()) {
            return null;
        }

        $this->objUser->username = $this->username;
        $this->objUser->email = $this->email;
        $this->objUser->status = $this->status;
        $this->objUser->timezone = $this->timezone;
        $this->objUser->active_until = \DateTime::createFromFormat('j-M-Y H:i', $this->active_until)
            ->setTimezone(new \DateTimeZone(\Yii::$app->user->identity->timezone))
            ->getTimestamp();
        if (!empty($this->password)) $this->objUser->setPassword($this->password);

        return $this->objUser->save() ? $this->objUser : null;
    }

    public function haveActiveSession()
    {
        return $this->objUser->haveActiveSession();
    }
}