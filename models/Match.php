<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "match".
 *
 * @property int $id
 * @property string $team_1
 * @property string $team_2
 * @property int $id_club_1
 * @property int $id_club_2
 * @property int $site_id
 * @property string $date_game
 * @property string $date_save
 *
 * @property MatchMarks[] $matchMarks
 */
class Match extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['team_1', 'team_2', 'id_club_1', 'id_club_2', 'date_game', 'date_save', 'site_id'], 'required'],
            [['id_club_1', 'id_club_2'], 'integer'],
            [['date_game', 'date_save'], 'safe'],
            [['team_1', 'team_2'], 'string', 'max' => 255],
            [['site_id'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'team_1' => 'Team 1',
            'team_2' => 'Team 2',
            'id_club_1' => 'Id Club 1',
            'id_club_2' => 'Id Club 2',
            'date_game' => 'Date Game',
            'date_save' => 'Date Save',
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->date_save = time();
        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatchMarks()
    {
        return $this->hasMany(MatchMarks::class, ['match_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayers()
    {
        return $this->hasMany(Player::class, ['id' => 'player_id'])
            ->viaTable('match_marks', ['match_id' => 'id']);
    }
}
