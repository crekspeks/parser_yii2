<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "matches_lineups".
 *
 * @property int $id
 * @property string $team_1
 * @property int $team_1_id
 * @property string $team_2
 * @property int $team_2_id
 * @property string $url
 * @property int $soccerway_id
 * @property int $team_1_changes
 * @property int $team_2_changes
 * @property string $source
 * @property string $players_json
 * @property int $select_id
 * @property int $date
 * @property int $besoccer_name_mark_team_1
 * @property int $besoccer_name_mark_team_2
 * @property int $sofascore_name_mark_team_1
 * @property int $sofascore_name_mark_team_2
 *
 * @property Select $select
 */
class MatchesLineups extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'matches_lineups';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['team_1', 'team_2', 'url', 'soccerway_id', 'date'], 'required'],
            [['soccerway_id', 'team_1_id', 'team_2_id', 'team_1_changes', 'team_2_changes', 'select_id', 'date', 'sofascore_name_mark_team_1', 'sofascore_name_mark_team_2', 'besoccer_name_mark_team_1', 'besoccer_name_mark_team_2'], 'integer'],
            [['players_json'], 'string'],
            [['team_1', 'team_2', 'url', 'source'], 'string', 'max' => 255],
            [['soccerway_id'], 'unique'],
            [['select_id'], 'exist', 'skipOnError' => true, 'targetClass' => Select::className(), 'targetAttribute' => ['select_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'team_1' => 'Team 1',
            'team_1_id' => 'Team 1 Id',
            'team_2' => 'Team 2',
            'team_2_id' => 'Team 2 Id',
            'url' => 'Url',
            'soccerway_id' => 'Soccerway ID',
            'team_1_changes' => 'Team 1 Changes',
            'team_2_changes' => 'Team 2 Changes',
            'source' => 'Source',
            'players_json' => 'Players Json',
            'select_id' => 'Select ID',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSelect()
    {
        return $this->hasOne(Select::className(), ['id' => 'select_id']);
    }

    public function getSubscribeUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])
            ->viaTable('user2matches_lineups', ['match_id' => 'id']);
    }

    public function getUserSubscribe()
    {
        if ((new \yii\db\Query())
            ->from('user2matches_lineups')
            ->where(['match_id' => $this->id, 'user_id' => Yii::$app->user->id])
            ->one()){
            return true;
        }
        return false;
    }

    public function getStatus()
    {
        $query = (new Query())
            ->select('status')
            ->from('user2matches_lineups')
            ->where(['match_id' => $this->id, 'user_id' => \Yii::$app->user->id])
            ->column();
        return (!empty($query)) ? $query[0] : false;
    }

    public function getTeam1Name()
    {
        return $this->hasOne(Team::className(), ['team_1_id' => 'id']);
    }

    public function getTeam2Name()
    {
        return $this->hasOne(Team::className(), ['team_2_id' => 'id']);
    }
}
