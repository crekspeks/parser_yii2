<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Query;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            if ($this->getUser()->active_until > 0 && $this->getUser()->active_until < time()) {
                $this->addError('username', 'Окончен срок действия аккаунта');
                return false;
            }
            if ($this->getUser()->haveActiveSession()) {
                $this->addError('username', 'Залогинен на другом устройстве');
                $admins = Yii::$app->authManager->getUserIdsByRole('Admin');
                foreach ($admins as $admin) {
                    $notification = (new Query())
                        ->from('admin_notification')
                        ->where(['user_id' => $this->getUser()->id, 'admin_id' => $admin,])
                        ->count();
                    if (!$notification) {
                        \Yii::$app->db->createCommand()->insert('admin_notification', [
                            'user_id' => $this->getUser()->id,
                            'admin_id' => $admin,
                        ])->execute();
                    }
                }
                return false;
            }
            return Yii::$app->user->login($this->getUser());
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
}
