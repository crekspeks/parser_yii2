<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "replacement_matches".
 *
 * @property int $id
 * @property int $soccerway_id
 * @property string $team_1
 * @property int $team_1_id
 * @property string $team_2
 * @property int $team_2_id
 * @property string $url
 * @property int $start_time
 * @property int $select_id
 * @property string $team_1_incidents
 * @property string $team_2_incidents
 *
 * @property Select $select
 */
class ReplacementMatches extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'replacement_matches';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['soccerway_id', 'team_1', 'team_1_id', 'team_2', 'team_2_id', 'url', 'start_time', 'select_id'], 'required'],
            [['soccerway_id', 'team_1_id', 'team_2_id', 'start_time', 'select_id'], 'integer'],
            [['team_1_incidents', 'team_2_incidents'], 'string'],
            [['team_1', 'team_2', 'url'], 'string', 'max' => 250],
            [['soccerway_id'], 'unique'],
            [['select_id'], 'exist', 'skipOnError' => true, 'targetClass' => Select::className(), 'targetAttribute' => ['select_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'soccerway_id' => 'Soccerway ID',
            'team_1' => 'Team 1',
            'team_1_id' => 'Team 1 ID',
            'team_2' => 'Team 2',
            'team_2_id' => 'Team 2 ID',
            'url' => 'Url',
            'start_time' => 'Start Time',
            'select_id' => 'Select ID',
            'team_1_incidents' => 'Team 1 Incidents',
            'team_2_incidents' => 'Team 2 Incidents',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $this->createUsersNotification();
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function getSelect()
    {
        return $this->hasOne(Select::className(), ['id' => 'select_id']);
    }

    public function getTeam1()
    {
        return Team::findOne(['soccerway_id' => $this->team_1_id]);
    }

    public function getTeam2()
    {
        return Team::findOne(['soccerway_id' => $this->team_2_id]);
    }

    public function getTeam1IncidentsArray()
    {
        if (!is_null($this->team_1_incidents)) {
            return json_decode($this->team_1_incidents, true)['match_marks'];
        }
        return false;
    }

    public function getTeam2IncidentsArray()
    {
        if (!is_null($this->team_2_incidents)) {
            return json_decode($this->team_2_incidents, true)['match_marks'];
        }
        return false;
    }

    public function haveNewNotifications()
    {
        if ($this->status == 1 || $this->status == 2) {
            return true;
        }
        return false;
    }

    /**
     * @return array|null|ReplacementMatches
     */
    public function getNextMatchTeam1()
    {
       return $nextMatch = self::find()
            ->where(['or', ['team_1_id' => $this->team_1_id], ['team_2_id' => $this->team_1_id]])
            ->andWhere(['>',  'start_time', $this->start_time])
            ->andWhere(['select_id' => $this->select_id])
            ->one();
    }

    /**
     * @return array|null|ReplacementMatches
     */
    public function getNextMatchTeam2()
    {
        return $nextMatch = self::find()
            ->where(['or', ['team_1_id' => $this->team_2_id], ['team_2_id' => $this->team_2_id]])
            ->andWhere(['>',  'start_time', $this->start_time])
            ->andWhere(['select_id' => $this->select_id])
            ->one();
    }

    public function getNextMatchTeam1IncidentsArr()
    {
        $nextMatch = $this->getNextMatchTeam1();

        if ($nextMatch) {
            if ($nextMatch->team_1_id == $this->team_1_id && !is_null($nextMatch->team_1_incidents)) {
                return json_decode($nextMatch->team_1_incidents, true)['match_marks'];
            } elseif ($nextMatch->team_2_id == $this->team_1_id &&!is_null($nextMatch->team_2_incidents)) {
                return json_decode($nextMatch->team_2_incidents, true)['match_marks'];
            }
        }
        return false;
    }


    public function getNextMatchTeam2IncidentsArr()
    {
        $nextMatch = $this->getNextMatchTeam2();

        if ($nextMatch) {
            if ($nextMatch->team_1_id == $this->team_2_id && !is_null($nextMatch->team_1_incidents)) {
                return json_decode($nextMatch->team_1_incidents, true)['match_marks'];
            } elseif ($nextMatch->team_2_id == $this->team_2_id &&!is_null($nextMatch->team_2_incidents)) {
                return json_decode($nextMatch->team_2_incidents, true)['match_marks'];
            }
        }
        return false;
    }

    public function saveNextMatchTeam1Incidents(array $incidents)
    {
        $nextMatch = $this->getNextMatchTeam1();

        if (!$nextMatch) return false;

        $incidents = json_encode($incidents);

        if ($nextMatch->team_1_id == $this->team_1_id) {
            $nextMatch->team_1_incidents = $incidents;
        } else if ($nextMatch->team_2_id == $this->team_1_id) {
            $nextMatch->team_2_incidents = $incidents;
        }
        $nextMatch->save();
    }

    public function saveNextMatchTeam2Incidents(array $incidents)
    {
        $nextMatch = $this->getNextMatchTeam2();

        if (!$nextMatch) return false;

        $incidents = json_encode($incidents);

        if ($nextMatch->team_1_id == $this->team_2_id) {
            $nextMatch->team_1_incidents = $incidents;
        } else if ($nextMatch->team_2_id == $this->team_2_id) {
            $nextMatch->team_2_incidents = $incidents;
        }
        $nextMatch->save();
    }

    public function createUsersNotification()
    {
        $subscribeUsersIds = Yii::$app->authManager->getUserIdsByRole('Incidents');

        foreach ($subscribeUsersIds as $id) {
            \Yii::$app->db->createCommand()->insert('user2teams_replacements', [
                'replacement_match_id' => $this->id,
                'user_id' => $id,
            ])->execute();
        }
    }

    public function updateUsersNotification()
    {
        $subscribeUsersIds = Yii::$app->authManager->getUserIdsByRole('Incidents');

        foreach ($subscribeUsersIds as $id) {
            \Yii::$app->db->createCommand()
                ->update('user2teams_replacements', ['status' => 1], ['replacement_match_id' => $this->id, 'user_id' => $id])
                ->execute();
        }
    }

    public function getStatus()
    {
        $query = (new Query())
            ->select('status')
            ->from('user2teams_replacements')
            ->where(['replacement_match_id' => $this->id, 'user_id' => \Yii::$app->user->id])
            ->column();
        return (!empty($query)) ? $query[0] : false;
    }

    public function itLiveTeam1()
    {
        $query = (new Query())
            ->from('replacement_matches')
            ->where(['or', ['team_1_id' => $this->team_1_id], ['team_2_id' => $this->team_1_id]])
            ->andWhere(['and', ['<', 'start_time', time()], ['>', 'start_time', time() - (100 * 60)]])
            ->count();
        return ($query);
    }

    public function itLiveTeam2()
    {
        $query = (new Query())
            ->from('replacement_matches')
            ->where(['or', ['team_1_id' => $this->team_2_id], ['team_2_id' => $this->team_2_id]])
            ->andWhere(['and', ['<', 'start_time', time()], ['>', 'start_time', time() - (100 * 60)]])
            ->count();
        return ($query);
    }

    /**
     *
     */
    public function team1YellowCardsNeedUpdate()
    {
        $yellowCards = TeamYellowCards::findOne(['team_id' => $this->team_1_id, 'select_id' => $this->select_id]);
        if (!$yellowCards || $yellowCards->needUpdate()) {
            return true;
        }
        return false;
    }

    public function team2YellowCardsNeedUpdate()
    {
        $yellowCards = TeamYellowCards::findOne(['team_id' => $this->team_2_id, 'select_id' => $this->select_id]);
        if (!$yellowCards || $yellowCards->needUpdate()) {
            return true;
        }
        return false;
    }

    public function saveTeam1YellowCards(array $yellow_cards)
    {
        $yellowCards = TeamYellowCards::findOne(['team_id' => $this->team_1_id, 'select_id' => $this->select_id]);
        if (!$yellowCards) {
            $yellowCards = new TeamYellowCards();
            $yellowCards->team_id = $this->team_1_id;
            $yellowCards->select_id = $this->select_id;
            $yellowCards->last_check_time = time();
        }

        $yellowCards->cards = json_encode($yellow_cards);
        $yellowCards->save();
    }

    public function saveTeam2YellowCards(array $yellow_cards)
    {
        $yellowCards = TeamYellowCards::findOne(['team_id' => $this->team_2_id, 'select_id' => $this->select_id]);
        if (!$yellowCards) {
            $yellowCards = new TeamYellowCards();
            $yellowCards->team_id = $this->team_2_id;
            $yellowCards->select_id = $this->select_id;
            $yellowCards->last_check_time = time();
        }

        $yellowCards->cards = json_encode($yellow_cards);
        $yellowCards->save();
    }

    /**
     * @return false|array
     */
    public function getTeam1YellowCards()
    {
        $yellowCards = TeamYellowCards::findOne(['team_id' => $this->team_1_id, 'select_id' => $this->select_id]);
        if (!$yellowCards) {
            return false;
        }
        return $yellowCards->cards = json_decode($yellowCards->cards, true);
    }

    /**
     * @return false|array
     */
    public function getTeam2YellowCards()
    {
        $yellowCards = TeamYellowCards::findOne(['team_id' => $this->team_2_id, 'select_id' => $this->select_id]);
        if (!$yellowCards) {
            return false;
        }
        return $yellowCards->cards = json_decode($yellowCards->cards, true);
    }
}
