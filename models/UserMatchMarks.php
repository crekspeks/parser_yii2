<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_match_marks".
 *
 * @property int $id
 * @property int $match_id
 * @property int $user_id
 * @property array $marks
 *
 * @property Match $match
 * @property User $user
 */
class UserMatchMarks extends \yii\db\ActiveRecord
{
    const NAME_NONE = 0;
    const NAME_OLD = 1;
    const NAME_NEW = 2;
    const NAME_RETURN = 3;
    const NAME_DOUBTFUL = 4;

    const PIC_NONE = 1;
    const PIC_QUESTION = 2;
    const PIC_RED_CROSS = 3;
    const PIC_GREEN_CROSS = 4;
    const PIC_RED_CARD = 5;
    const PIC_YELLOW_CARD = 6;
    const PIC_GREEN_CARD = 7;
    const PIC_RED_AIRPLANE = 8;
    const PIC_GREEN_AIRPLANE = 9;
    const PIC_CARDS = 10;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_match_marks';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['match_id', 'user_id', 'group_id'], 'integer'],
            [['marks'], 'required'],
            [['marks'], 'safe'],
            [['match_id'], 'exist', 'skipOnError' => true, 'targetClass' => Match::className(), 'targetAttribute' => ['match_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'match_id' => 'Match ID',
            'user_id' => 'User ID',
            'marks' => 'Marks',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatch()
    {
        return $this->hasOne(Match::className(), ['id' => 'match_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
