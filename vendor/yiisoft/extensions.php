<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'cebe/yii2-gravatar' => 
  array (
    'name' => 'cebe/yii2-gravatar',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@cebe/gravatar' => $vendorDir . '/cebe/yii2-gravatar/cebe/gravatar',
    ),
  ),
  'johnitvn/yii2-ajaxcrud' => 
  array (
    'name' => 'johnitvn/yii2-ajaxcrud',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@johnitvn/ajaxcrud' => $vendorDir . '/johnitvn/yii2-ajaxcrud/src',
    ),
    'bootstrap' => 'johnitvn\\ajaxcrud\\Bootstrap',
  ),
  'johnitvn/yii2-rbac-plus' => 
  array (
    'name' => 'johnitvn/yii2-rbac-plus',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@johnitvn/rbacplus' => $vendorDir . '/johnitvn/yii2-rbac-plus/src',
    ),
    'bootstrap' => 'johnitvn\\rbacplus\\Bootstrap',
  ),
  'garyjl/yii2-simple_html_dom' => 
  array (
    'name' => 'garyjl/yii2-simple_html_dom',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@garyjl/simplehtmldom' => $vendorDir . '/garyjl/yii2-simple_html_dom',
    ),
  ),
  'rmrevin/yii2-fontawesome' => 
  array (
    'name' => 'rmrevin/yii2-fontawesome',
    'version' => '3.9999999.9999999.9999999-dev',
    'alias' => 
    array (
      '@rmrevin/yii/fontawesome' => $vendorDir . '/rmrevin/yii2-fontawesome',
    ),
  ),
  'dmstr/yii2-adminlte-asset' => 
  array (
    'name' => 'dmstr/yii2-adminlte-asset',
    'version' => '2.7.0.0-beta1',
    'alias' => 
    array (
      '@dmstr' => $vendorDir . '/dmstr/yii2-adminlte-asset',
    ),
  ),
  'kartik-v/yii2-nav-x' => 
  array (
    'name' => 'kartik-v/yii2-nav-x',
    'version' => '1.2.3.0',
    'alias' => 
    array (
      '@kartik/nav' => $vendorDir . '/kartik-v/yii2-nav-x/src',
    ),
  ),
  'kartik-v/yii2-popover-x' => 
  array (
    'name' => 'kartik-v/yii2-popover-x',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/popover' => $vendorDir . '/kartik-v/yii2-popover-x/src',
    ),
  ),
  'kartik-v/yii2-dialog' => 
  array (
    'name' => 'kartik-v/yii2-dialog',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/dialog' => $vendorDir . '/kartik-v/yii2-dialog/src',
    ),
  ),
  'kartik-v/yii2-dropdown-x' => 
  array (
    'name' => 'kartik-v/yii2-dropdown-x',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/dropdown' => $vendorDir . '/kartik-v/yii2-dropdown-x',
    ),
  ),
  'kartik-v/yii2-editable' => 
  array (
    'name' => 'kartik-v/yii2-editable',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/editable' => $vendorDir . '/kartik-v/yii2-editable/src',
    ),
  ),
  'kartik-v/yii2-grid' => 
  array (
    'name' => 'kartik-v/yii2-grid',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/grid' => $vendorDir . '/kartik-v/yii2-grid/src',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base/src',
    ),
  ),
  'kartik-v/yii2-widget-datetimepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-datetimepicker',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/datetime' => $vendorDir . '/kartik-v/yii2-widget-datetimepicker/src',
    ),
  ),
  'kartik-v/yii2-widget-select2' => 
  array (
    'name' => 'kartik-v/yii2-widget-select2',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/select2' => $vendorDir . '/kartik-v/yii2-widget-select2/src',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap/src',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug/src',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker/src',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii/src',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui/src',
    ),
  ),
  'kartik-v/yii2-detail-view' => 
  array (
    'name' => 'kartik-v/yii2-detail-view',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/detail' => $vendorDir . '/kartik-v/yii2-detail-view/src',
    ),
  ),
  'kartik-v/yii2-mpdf' => 
  array (
    'name' => 'kartik-v/yii2-mpdf',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/mpdf' => $vendorDir . '/kartik-v/yii2-mpdf/src',
    ),
  ),
  'kartik-v/yii2-widget-activeform' => 
  array (
    'name' => 'kartik-v/yii2-widget-activeform',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/form' => $vendorDir . '/kartik-v/yii2-widget-activeform/src',
    ),
  ),
);
