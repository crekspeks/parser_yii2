<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 26.09.2018
 * Time: 1:39
 */

namespace app\controllers;


use app\components\ReplacementParser;
use app\components\ReplacementsMatchParser;
use app\models\ReplacementMatches;
use app\models\Select;
use yii\web\Controller;
use yii\filters\AccessControl;

class ReplacementsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'settings',
                            'check-select',
                            'yellow-cards',
                            'get-new-matches',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        if (\Yii::$app->user->can('incidentsMatches')) {
            $matchesArr = ReplacementMatches::find()
                ->where(['>', 'start_time', time() - (120 * 60)])
                ->andWhere(['or', ['not', ['team_1_incidents' => null]], ['not', ['team_2_incidents' => null]]])
                ->orderBy('start_time')
                ->all();
            return $this->render('index', ['matchesArr' => $matchesArr]);
        }
    }

    public function actionSettings()
    {
        if (\Yii::$app->user->can('sAdminMenu')) {
            $selects = Select::findAll(['parent_id' => null, 'name' => 'CLUB DOMESTIC']);
            return $this->render('settings', ['selects' => $selects]);
        }
    }

    public function actionCheckSelect()
    {
        if (\Yii::$app->user->can('sAdminMenu')) {
            $id = \Yii::$app->request->get('id');
            $state = \Yii::$app->request->get('state');

            $select = Select::findOne($id);
            $select->replacements_parse = $state;
            $select->save();
        }
    }

    public function actionYellowCards()
    {
        if (\Yii::$app->user->can('sAdminMenu')){
            $select_id = \Yii::$app->request->get('select_id');
            $yellow_cards = \Yii::$app->request->get('yellow_cards', false);
            $select = Select::findOne($select_id);
            if ($yellow_cards) {
                $select->yellow_cards_set = $yellow_cards;
                $select->save();
            }

            return $this->renderPartial('_yellow_cards', ['select' => $select]);
        }
    }

    public function actionGetNewMatches()
    {
        if (\Yii::$app->user->can('sAdminMenu')) {
            $selects = Select::findAll(['replacements_parse' => 1]);
            foreach ($selects as $select) {
                ReplacementParser::getMatches($select);
            }

            /*$matches = ReplacementMatches::find()->where(['and', ['<', 'start_time', time()], ['>', 'start_time', time() - (100 * 60)]])->all();
            foreach ($matches as $match) {
                $replacements = new ReplacementsMatchParser($match);
                $replacements->check();
            }*/

            return 'Готово';
        }
    }
}