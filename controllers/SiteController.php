<?php
namespace app\controllers;

use app\components\LineupsParser;
use app\components\MarksAccess;
use app\components\ReplacementParser;
use app\components\TransfermarktParser;
use app\models\Comment;
use app\models\FavoriteMatches;
use app\models\MatchesLineups;
use app\models\Player;
use app\models\ReplacementMatches;
use app\models\Select;
use app\models\Team;
use app\models\TeamsReplacements;
use app\models\User;
use app\models\UserMatchMarks;
use Yii;
use yii\db\Query;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\LoginForm;
use app\components\Parser;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true,
                    ],
                    [
                        'actions' => [
                            'logout',
                            'index',
                            'gen-select',
                            'get-matches',
                            'get-match-info',
                            'set-match-marks',
                            'get-match-marks',
                            'get-transfermarkt-info',
                            'save-match',
                            'del-match',
                            'add-comment',
                            'del-comment',
                            'edit-comment',
                            'get-odds-link',
                            'get-betexplorer-link',
                            'get-team-form',
                            'save-team',
                            'ping',
                            'get-player-names',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $matchUrl = Yii::$app->request->get('matchUrl', false);
        $matchCount = (int)Yii::$app->request->get('matchCount', 1);
        $transfermarkt = (int)Yii::$app->request->get('transfermarkt', 0);
        $lineups = Yii::$app->request->get('lineups', false);
        $replacement = Yii::$app->request->get('replacement', false);
        if (!is_integer($matchCount) || $matchCount == 1){
            $matchCount = isset($_COOKIE['matchCount']) ? $_COOKIE['matchCount'] : 1;
        }

        if (!Yii::$app->request->isAjax) {
            $selectArr = Select::genSelectArray();
            Yii::$app->view->params['selectArr'] = $selectArr;
            Yii::$app->view->params['matchCount'] = $matchCount;
            Yii::$app->view->params['transfermarkt'] = $transfermarkt;
        }

        if ($matchUrl) {
            $parser = new Parser($matchUrl, $matchCount, $transfermarkt, $lineups, $replacement);
            $info = $parser->getMatchInfo();

            if (Yii::$app->user->can('shareMarks')) {
                $comments['team_1'] = Comment::findAll(['match_id' => $info['matchInfo']['id'], 'team' => 1, 'group_id' => 1]);
                $comments['team_2'] = Comment::findAll(['match_id' => $info['matchInfo']['id'], 'team' => 2, 'group_id' => 1]);
            } else {
                $comments['team_1'] = Comment::findAll([
                    'match_id' => $info['matchInfo']['id'],
                    'team' => 1,
                    'user_id' => Yii::$app->user->id
                ]);
                $comments['team_2'] = Comment::findAll([
                    'match_id' => $info['matchInfo']['id'],
                    'team' => 2,
                    'user_id' => Yii::$app->user->id
                ]);
            }

            if (Yii::$app->request->isAjax) {
                return $this->renderPartial('_match', [
                    'dataProviderTeam1' => $info['team_1'],
                    'dataProviderTeam2' => $info['team_2'],
                    'matchInfo' => $info['matchInfo'],
                    'matchCount' => $matchCount,
                    'transfermarkt' => $transfermarkt,
                    'comments' => $comments,
                    'lineups' => $lineups,
                    'replacement' => $replacement,
                ]);
            }

            return $this->render('index', [
                'dataProviderTeam1' => $info['team_1'],
                'dataProviderTeam2' => $info['team_2'],
                'matchInfo' => $info['matchInfo'],
                'matchCount' => $matchCount,
                'transfermarkt' => $transfermarkt,
                'comments' => $comments,
                'lineups' => $lineups,
                'replacement' => $replacement,
            ]);
        }
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->renderPartial('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /*public function actionGenSelect()
    {
        Parser::updateSelect();
    }*/

    public function actionGetMatches()
    {
        $url = Yii::$app->request->get('url');

        Yii::$app->response->format = Response::FORMAT_JSON;
        return Parser::getMatches($url);
    }

    public function actionSetMatchMarks()
    {
        $matchMarks = Yii::$app->request->post('matchMarks');
        $matchId = Yii::$app->request->post('matchId');

        if (Yii::$app->user->can('shareMarks')) {
            $marks = UserMatchMarks::findOne(['match_id' => $matchId, 'group_id' => 1]);
        } else {
            $marks = UserMatchMarks::findOne(['match_id' => $matchId, 'user_id' => Yii::$app->user->id]);
        }

        if (!$marks) {
            $marks = new UserMatchMarks();
            if (Yii::$app->user->can('shareMarks')) {
                $marks->group_id = 1;
            } else {
                $marks->user_id = Yii::$app->user->id;
            }
            $marks->match_id = $matchId;
        }

        $marks->marks = $matchMarks;
        $marks->save();
    }

    public function actionGetMatchMarks()
    {
        $matchId = Yii::$app->request->get('matchId');
        if (Yii::$app->user->can('shareMarks')) {
            $marks = UserMatchMarks::findOne(['match_id' => $matchId, 'group_id' => 1]);
        } else {
            $marks = UserMatchMarks::findOne(['match_id' => $matchId, 'user_id' => Yii::$app->user->id]);
        }

        if ($marks) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $marks->marks;
        }
    }

    public function actionGetTransfermarktInfo()
    {
        $matchJson = Yii::$app->request->post('matchInfo');
        $matchArr = json_decode($matchJson, true);

        $team_1_soccerway_id = Yii::$app->request->post('team_1_soccerway_id');
        $team_2_soccerway_id = Yii::$app->request->post('team_2_soccerway_id');

        $team_1 = Team::findOne(['soccerway_id' => $team_1_soccerway_id]);
        $team_2 = Team::findOne(['soccerway_id' => $team_2_soccerway_id]);

        if ($team_1 && !empty($team_1->transfermarkt_name)) $matchArr['team1']['name'] = $team_1->transfermarkt_name;
        if ($team_2 && !empty($team_2->transfermarkt_name)) $matchArr['team2']['name'] = $team_2->transfermarkt_name;

        $transfermarktParser = new TransfermarktParser($matchArr);
        $matchInfo = $transfermarktParser->getMatchInfo();

        Yii::$app->response->format = Response::FORMAT_JSON;
        return json_encode($matchInfo);
    }

    public function actionSaveMatch()
    {
        $matchId = Yii::$app->request->get('matchId');
        $matchUrl = Yii::$app->request->get('matchUrl');
        $match = FavoriteMatches::findOne(['match_id' => $matchId, 'user_id' => Yii::$app->user->id]);
        if (!$match) {
            $match = new FavoriteMatches();
            $match->match_id = $matchId;
            $match->user_id = Yii::$app->user->id;
            $match->url = $matchUrl;
            $match->save();
        }

        $matches = MarksAccess::getSavedMatches();
        return $this->renderPartial('_saved_matches', ['savedMatches' => $matches]);
    }

    public function actionDelMatch()
    {
        $id = Yii::$app->request->get('id');
        $match = FavoriteMatches::findOne($id);
        if ($match) {
            $match->delete();
        }

        $matches = MarksAccess::getSavedMatches();
        return $this->renderPartial('_saved_matches', ['savedMatches' => $matches]);
    }

    public function actionAddComment()
    {
        $matchId = Yii::$app->request->post('matchId');
        $team = Yii::$app->request->post('team');
        $text = Yii::$app->request->post('text');

        $comment = new Comment();
        $comment->match_id = $matchId;
        if (Yii::$app->user->can('shareMarks')) {
            $comment->group_id = 1;
        }
        $comment->user_id = Yii::$app->user->id;
        $comment->team = $team;
        $comment->text = $text;

        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($comment->save()) {
            $commentHtml = $this->renderPartial('_comment', [
                'comment' => $comment
            ]);
            return json_encode(['success' => 1, 'commentHtml' => $commentHtml]);
        }
        return json_encode(['success' => 0]);
    }

    public function actionDelComment()
    {
        $commentId = Yii::$app->request->get('commentId');

        $comment = Comment::findOne($commentId);
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($comment && $comment->isAuthor()) {
            $comment->delete();
            return json_encode(['success' => 1]);
        }
        return json_encode(['success' => 0]);
    }

    public function actionEditComment()
    {
        $commentId = Yii::$app->request->post('commentId');
        $text = Yii::$app->request->post('text');

        $comment = Comment::findOne($commentId);
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($comment && $comment->isAuthor()) {
            $comment->text = $text;
            if ($comment->save()) {
                return json_encode(['success' => 1]);
            }
        }
        return json_encode(['success' => 0]);
    }

    public function actionGetOddsLink()
    {
        $team_1_name = Yii::$app->request->get('team_1_name');
        $team_2_name = Yii::$app->request->get('team_2_name');
        $team_1_soccerway_id = Yii::$app->request->get('team_1_soccerway_id');
        $team_2_soccerway_id = Yii::$app->request->get('team_2_soccerway_id');
        $date = Yii::$app->request->get('date');

        $team_1 = Team::findOne(['soccerway_id' => $team_1_soccerway_id]);
        $team_2 = Team::findOne(['soccerway_id' => $team_2_soccerway_id]);

        if ($team_1 && !empty($team_1->oddsportal_name)) $team_1_name = $team_1->oddsportal_name;
        if ($team_2 && !empty($team_2->oddsportal_name)) $team_2_name = $team_2->oddsportal_name;

        return Parser::getOddsLink(trim($team_1_name), trim($team_2_name), $date);
    }

    public function actionGetBetexplorerLink()
    {
        $team_1_name = Yii::$app->request->get('team_1_name');
        $team_2_name = Yii::$app->request->get('team_2_name');
        $team_1_soccerway_id = Yii::$app->request->get('team_1_soccerway_id');
        $team_2_soccerway_id = Yii::$app->request->get('team_2_soccerway_id');
        $date = Yii::$app->request->get('date');

        $team_1 = Team::findOne(['soccerway_id' => $team_1_soccerway_id]);
        $team_2 = Team::findOne(['soccerway_id' => $team_2_soccerway_id]);

        if ($team_1 && !empty($team_1->betexplorer_name)) $team_1_name = $team_1->betexplorer_name;
        if ($team_2 && !empty($team_2->betexplorer_name)) $team_2_name = $team_2->betexplorer_name;

        return Parser::getBetexplorerLink(trim($team_1_name), trim($team_2_name), $date);
    }

    public function actionGetTeamForm()
    {
        if (Yii::$app->user->can('addForeignName')){
            $soccerway_id = Yii::$app->request->get('soccerway_id');
            $team = Team::findOne(['soccerway_id' => $soccerway_id]);
            if (!$team) {
                $team = new Team();
                $team->soccerway_id = $soccerway_id;
            }

            return $this->renderPartial('_team_form', ['team' => $team]);
        }
    }

    public function actionSaveTeam()
    {
        if (Yii::$app->user->can('addForeignName')) {
            $teamArr = Yii::$app->request->post('Team');
            $team = Team::findOne(['soccerway_id' => $teamArr['soccerway_id']]);
            if (!$team) $team = new Team();

            if ($teamArr['besoccer_name'] != '') {
                foreach (MatchesLineups::findAll(['team_1_id' => $teamArr['soccerway_id']]) as $match) {
                    $match->besoccer_name_mark_team_1 = 0;
                    $match->save();
                }

                foreach (MatchesLineups::findAll(['team_2_id' => $teamArr['soccerway_id']]) as $match) {
                    $match->besoccer_name_mark_team_2 = 0;
                    $match->save();
                }
            }

            if ($teamArr['sofascore_name'] != '') {
                foreach (MatchesLineups::findAll(['team_1_id' => $teamArr['soccerway_id']]) as $match) {
                    $match->sofascore_name_mark_team_1 = 0;
                    $match->save();
                }

                foreach (MatchesLineups::findAll(['team_2_id' => $teamArr['soccerway_id']]) as $match) {
                    $match->sofascore_name_mark_team_2 = 0;
                    $match->save();
                }
            }
            $team->setAttributes($teamArr);
            $team->save();
        }
    }

    public function actionGetPlayerNames()
    {
        if (Yii::$app->user->can('addForeignName')){
            $player_id = Yii::$app->request->get('player_id');
            $player_names = Yii::$app->request->get('player_names', false);
            $player = Player::findOne($player_id);
            if ($player_names) {
                $player->names = $player_names;
                $player->save();
            }

            return $this->renderPartial('_player_names', ['player' => $player]);
        }
    }

    public function actionPing()
    {
        if (!Yii::$app->user->isGuest) {
            Yii::$app->db->createCommand()
                ->update(Yii::$app->session->sessionTable, ['expire' => time() + 40], 'user_id = '.Yii::$app->user->id)
                ->execute();

            $last_lineups = Yii::$app->request->get('lastLineups', false);
            $replacements_page = Yii::$app->request->get('replacements_page', false);

            $responseArr = [];
            if (\Yii::$app->user->can('lineupsMatches')) {
                if ($last_lineups) {
                    $matches = MatchesLineups::find()->where(['and', ['>', 'date', time() - 7200], ['<', 'date', time() + 14400]])->orderBy('date')->all();
                    $newMatchesHtml = $this->renderPartial('../lineups/_lineups_matches', ['matchesArr' => $matches]);

                    $responseArr = ['newMatchesHtml' => $newMatchesHtml];
                }

                $newMatchesNotifications = (new Query())
                    ->select(['u2l.match_id', 'ml.team_1', 'ml.team_2', 'ml.url', 'ml.team_1_changes', 'ml.team_2_changes'])
                    ->from('user2matches_lineups u2l')
                    ->innerJoin(MatchesLineups::tableName() . ' ml', 'u2l.match_id = ml.id')
                    ->where(['u2l.status' => 1])//1 - новый
                    ->andWhere(['u2l.user_id' => Yii::$app->user->id])
                    ->all();

                if ($newMatchesNotifications) {
                    foreach ($newMatchesNotifications as $match) {
                        Yii::$app->db->createCommand()
                            ->update('user2matches_lineups', ['status' => 2], ['user_id' => Yii::$app->user->id, 'match_id' => $match['match_id']])
                            ->execute();
                    }

                    $responseArr += ['newMatchesNotifications' => $newMatchesNotifications];
                }

                $notificationsArr = (new Query())
                    ->select(['u2l.match_id', 'ml.team_1', 'ml.team_2', 'ml.url', 'ml.team_1_changes', 'ml.team_2_changes'])
                    ->from('user2matches_lineups u2l')
                    ->innerJoin(MatchesLineups::tableName() . ' ml', 'u2l.match_id = ml.id')
                    ->where(['u2l.status' => 2])//2 - не новый, но еще не просмотренный
                    ->andWhere(['u2l.user_id' => Yii::$app->user->id])
                    ->all();

                $noticationsHtml = $this->renderPartial('_notifications_header', ['notificationsArr' => $notificationsArr]);

                $responseArr += ['notificationsHtml' => $noticationsHtml, 'notificationsCount' => count($notificationsArr)];
            }

            if (\Yii::$app->user->can('incidentsMatches')) {
                if ($replacements_page) {
                    $matches = ReplacementMatches::find()
                        ->where(['>', 'start_time', time() - (100 * 60)])
                        ->andWhere(['or', ['not', ['team_1_incidents' => null]], ['not', ['team_2_incidents' => null]]])
                        ->orderBy('start_time')
                        ->all();
                    $newReplacementsHtml = $this->renderPartial('../replacements/_replacement_matches', ['matchesArr' => $matches]);

                    $responseArr += ['newReplacementsHtml' => $newReplacementsHtml];
                }

                $newReplacementsNotifications = (new Query())
                    ->select(['u2tr.replacement_match_id', 'rp.team_1_incidents', 'rp.team_2_incidents', 'rp.team_1', 'rp.team_2', 'rp.url'])
                    ->from('user2teams_replacements u2tr')
                    ->innerJoin(ReplacementMatches::tableName() . ' rp', 'u2tr.replacement_match_id = rp.id')
                    ->where(['u2tr.status' => 1])//1 - новый
                    ->andWhere(['u2tr.user_id' => Yii::$app->user->id])
                    ->all();

                if ($newReplacementsNotifications) {
                    foreach ($newReplacementsNotifications as $replacement) {
                        Yii::$app->db->createCommand()
                            ->update('user2teams_replacements', ['status' => 2], ['user_id' => Yii::$app->user->id, 'replacement_match_id' => $replacement['replacement_match_id']])
                            ->execute();
                    }

                    $newReplacementsNotifications = $this->renderPartial('_notifications_replacements_modal', ['replacements' => $newReplacementsNotifications]);
                    $responseArr += ['newReplacementsNotifications' => $newReplacementsNotifications];
                }

                $notificationsReplacementsArr = (new Query())
                    ->select(['u2tr.replacement_match_id', 'rp.team_1_incidents', 'rp.team_2_incidents', 'rp.team_1', 'rp.team_2', 'rp.url'])
                    ->from('user2teams_replacements u2tr')
                    ->innerJoin(ReplacementMatches::tableName() . ' rp', 'u2tr.replacement_match_id = rp.id')
                    ->where(['u2tr.status' => 2])//2 - не новый, но еще не просмотренный
                    ->andWhere(['u2tr.user_id' => Yii::$app->user->id])
                    ->all();

                $notificationsReplacementsHtml = $this->renderPartial('_notifications_replacements_header', ['notificationsReplacementsArr' => $notificationsReplacementsArr]);

                $responseArr += ['notificationsReplacementsHtml' => $notificationsReplacementsHtml, 'notificationsReplacementsCount' => count($notificationsReplacementsArr)];
            }

            if (\Yii::$app->user->can('sAdminMenu')) {
                $notificationsAdminArr = (new Query())
                    ->select(['an.user_id', 'an.admin_id', 'u.username'])
                    ->from('admin_notification an')
                    ->innerJoin(User::tableName() . ' u', 'an.user_id = u.id')
                    ->where(['an.admin_id' => Yii::$app->user->id])
                    ->all();

                \Yii::$app->db->createCommand()
                    ->delete('admin_notification', ['admin_id' => Yii::$app->user->id])
                    ->execute();

                if ($notificationsAdminArr) {
                    $responseArr += ['notificationsAdminArr' => $notificationsAdminArr];
                }
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return json_encode($responseArr);
        }
    }
}
