<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 15.06.2018
 * Time: 7:44
 */

namespace app\controllers;

use app\components\LineupsParser;
use app\models\MatchesLineups;
use app\models\Select;
use app\models\User;
use yii\db\Query;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class LineupsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'settings',
                            'check-select',
                            'match-subscribe',
                            'match-subscribe-all',
                            'match-subscribe-all',
                            'test'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        if (\Yii::$app->user->can('lineupsMatches')) {
            $matches = MatchesLineups::find()->where(['and', ['>', 'date', time() - 7200], ['<', 'date', time() + 14400]])->orderBy('date')->all();

            return $this->render('index', ['matchesArr' => $matches]);
        }
        return $this->redirect('/');
    }

    public function actionSettings()
    {
        if (\Yii::$app->user->can('sAdminMenu')) {
            $selects = Select::findAll(['parent_id' => null]);
            return $this->render('settings', ['selects' => $selects]);
        }
    }

    public function actionCheckSelect()
    {
        if (\Yii::$app->user->can('sAdminMenu')) {
            $id = \Yii::$app->request->get('id');
            $state = \Yii::$app->request->get('state');

            $select = Select::findOne($id);
            $select->lineups_parse = $state;
            $select->save();
        }
    }

    public function actionMatchSubscribe()
    {
        if (\Yii::$app->user->can('lineupsMatches')) {
            $id = \Yii::$app->request->get('id');
            $state = \Yii::$app->request->get('state');

            if ($state == 1) {
                \Yii::$app->db->createCommand()->insert('user2matches_lineups', [
                    'match_id' => $id,
                    'user_id' => \Yii::$app->user->id,
                ])->execute();
            } else {
                \Yii::$app->db->createCommand()->delete('user2matches_lineups', [
                    'match_id' => $id,
                    'user_id' => \Yii::$app->user->id,
                ])->execute();
            }
        }
    }

    public function actionMatchSubscribeAll()
    {
        if (\Yii::$app->user->can('lineupsMatches')) {
            $state = \Yii::$app->request->get('state');

            $matches = MatchesLineups::find()
                ->select('id')
                ->where(['source' => null])
                ->all();
            $matchesArr = [];
            $matchesIds = [];
            if ($state == 1) {
                foreach ($matches as $match) {
                    if ($match->status === false) {
                        $matchesArr[] = [\Yii::$app->user->id, $match->id, 0];
                        $matchesIds[] = $match->id;
                    }
                }
                \Yii::$app->db->createCommand()->batchInsert('user2matches_lineups', ['user_id', 'match_id', 'status'], $matchesArr)
                    ->execute();
                $user = User::findOne(\Yii::$app->user->id);
                $user->lineups_subscribe = 1;
                $user->save();
            } else {
                foreach ($matches as $match) {
                    $matchesArr[] = [\Yii::$app->user->id, $match->id, 0];
                    $matchesIds[] = $match->id;
                }

                \Yii::$app->db->createCommand()->delete('user2matches_lineups', [
                    'user_id' => \Yii::$app->user->id,
                ])->execute();

                $user = User::findOne(\Yii::$app->user->id);
                $user->lineups_subscribe = 0;
                $user->save();
            }
            return json_encode($matchesIds);
        }
    }

    public function actionTest()
    {
        $matches = MatchesLineups::find()->where([
            'and', ['source' => null], ['and', ['>', 'date', time()], ['<', 'date', time() + 5400]]
        ])->all();

        foreach ($matches as $match) {
            $lineupsMatchParser = new LineupsMatchParser($match);
            $lineupsMatchParser->check();
        }
    }
}