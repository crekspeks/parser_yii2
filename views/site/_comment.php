<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 26.05.2018
 * Time: 12:46
 */

/* @var $comment \app\models\Comment */
?>

<div class="direct-chat-msg" data-id="<?= $comment->id ?>">
    <div class="direct-chat-info clearfix">
        <span class="direct-chat-name pull-left"><?= $comment->user->username ?></span>
        <span class="direct-chat-timestamp pull-right">
            <?= \Yii::$app->formatter->asDate($comment->date_create) ?>
            <?php if ($comment->isAuthor()): ?>
            <i class="fa fa-trash" style="cursor: pointer"></i>
            <i class="fa fa-pencil" style="cursor: pointer"></i>
            <?php endif; ?>
        </span>
    </div>
    <!-- /.direct-chat-info -->
    <img class="direct-chat-img" src="./img/user.png" alt="Message User Image"><!-- /.direct-chat-img -->
    <div class="direct-chat-text">
        <?= $comment->text ?>
    </div>
    <!-- /.direct-chat-text -->
</div>
