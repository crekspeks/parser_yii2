<?php
use app\models\UserMatchMarks;

if ($lineups >= 1) {
    $redDotColumn = [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fl',
        'width' => '25px',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'header' => '<i class="fa fa-refresh fa-plus show-column"></i>',
        'visible' => ($lineups >= 1),
        'value' => function($model) {
            if (!isset($model['fl'])) {
                return '<div class="">&nbsp;</div>';
            } elseif ($model['fl'] == 1) {
                return '<div class="red-dot">&nbsp;</div>';
            } else if ($model['fl'] == 2) {
                return '<div class="yellow-dot">&nbsp;</div>';
            }
        },
        'format' => 'raw',
    ];
} else {
    $redDotColumn = [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'red_dot',
        'header' => '<i class="fa fa-refresh fa-plus show-column"></i>',
        'width' => '25px',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'value' => function($model) {
            return '<div class="mark" data-value="0" data-mark="red_dot">&nbsp;</div>';
        },
        'format' => 'raw',
    ];
}

if ($replacement >= 1){
    $replacementColumn = [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'red_dot',
        'header' => '&nbsp;',
        'width' => '25px',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'value' => function($model) {
            if (isset($model['rp'])) {
                return '<img src="/img/' . $model['rp'] . '.png" alt="">';
            } else {
                return '';
            }
        },
        'format' => 'raw',
    ];
} else {
    $replacementColumn = [
        'visible' => false,
    ];
}

$columnArr = [
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'shirtnumber',
        'header' => '&nbsp;',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'name',
        'headerOptions' => ['style' => 'text-align: center;'],
        'header' => $historyMarks,
        'value' => function($model) {
            return '
              <li class="dropdown" style="list-style: none; float: left;">
                <a class="dropdown-toggle name-select-label" data-toggle="dropdown" href="#" aria-expanded="false" data-value="0" title="' . $model['tooltip_name'] . '">
                  '. $model['name'] .'
                </a>
                <ul class="dropdown-menu">
                  <li class="name-select" data-value="' . UserMatchMarks::NAME_NONE . '"><a class="player_status'. UserMatchMarks::NAME_NONE .'" tabindex="-1" href="#">'. $model['name'] .'</a></li>
                  <li class="name-select" data-value="' . UserMatchMarks::NAME_NEW . '"><a class="player_status'. UserMatchMarks::NAME_NEW .'" tabindex="-1" href="#">'. $model['name'] .'</a></li>
                  <li class="name-select" data-value="' . UserMatchMarks::NAME_OLD . '"><a class="player_status'. UserMatchMarks::NAME_OLD .'" tabindex="-1" href="#">'. $model['name'] .'</a></li>
                  <li class="name-select" data-value="' . UserMatchMarks::NAME_DOUBTFUL . '"><a class="player_status'. UserMatchMarks::NAME_DOUBTFUL .'" tabindex="-1" href="#">'. $model['name'] .'</a></li>
                  <li class="name-select" data-value="' . UserMatchMarks::NAME_RETURN . '"><a class="player_status'. UserMatchMarks::NAME_RETURN .'" tabindex="-1" href="#">'. $model['name'] .'</a></li>
                </ul>
              </li>
              ';
        },
        'format' => 'raw',
    ],
    $replacementColumn,
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'name',
        'header' => '&nbsp;',
        'width' => '25px',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'value' => function($model) {
            return '
              <li class="dropdown" style="list-style: none;">
                <a class="dropdown-toggle pic-select-label" data-toggle="dropdown" href="#" aria-expanded="false" data-value="0">
                  <img src="img/1.png">
                </a>
                <ul class="dropdown-menu" style="min-width: auto;">
                    <li class="pic-select" data-value="' . UserMatchMarks::PIC_NONE . '" style="border-bottom: 1px solid; margin-bottom: 2px;"><img src="img/'. UserMatchMarks::PIC_NONE .'.png"></li>
                    <li class="pic-select" data-value="' . UserMatchMarks::PIC_QUESTION . '" style="border-bottom: 1px solid; margin-bottom: 2px;"><img src="img/'. UserMatchMarks::PIC_QUESTION .'.png"></li>
                    <li class="pic-select" data-value="' . UserMatchMarks::PIC_RED_CROSS . '" style="border-bottom: 1px solid; margin-bottom: 2px;"><img src="img/'. UserMatchMarks::PIC_RED_CROSS .'.png"></li>
                    <li class="pic-select" data-value="' . UserMatchMarks::PIC_GREEN_CROSS . '" style="border-bottom: 1px solid; margin-bottom: 2px;"><img src="img/'. UserMatchMarks::PIC_GREEN_CROSS .'.png"></li>
                    <li class="pic-select" data-value="' . UserMatchMarks::PIC_RED_CARD . '" style="border-bottom: 1px solid; margin-bottom: 2px;"><img src="img/'. UserMatchMarks::PIC_RED_CARD .'.png"></li>
                    <li class="pic-select" data-value="' . UserMatchMarks::PIC_YELLOW_CARD . '" style="border-bottom: 1px solid; margin-bottom: 2px;"><img src="img/'. UserMatchMarks::PIC_YELLOW_CARD .'.png"></li>
                    <li class="pic-select" data-value="' . UserMatchMarks::PIC_GREEN_CARD . '" style="border-bottom: 1px solid; margin-bottom: 2px;"><img src="img/'. UserMatchMarks::PIC_GREEN_CARD .'.png"></li>
                    <li class="pic-select" data-value="' . UserMatchMarks::PIC_RED_AIRPLANE . '" style="border-bottom: 1px solid; margin-bottom: 2px;"><img src="img/'. UserMatchMarks::PIC_RED_AIRPLANE .'.png"></li>
                    <li class="pic-select" data-value="' . UserMatchMarks::PIC_GREEN_AIRPLANE . '" style="border-bottom: 1px solid; margin-bottom: 2px;"><img src="img/'. UserMatchMarks::PIC_GREEN_AIRPLANE .'.png"></li>
                    <li class="pic-select" data-value="' . UserMatchMarks::PIC_CARDS . '" style="border-bottom: 1px solid; margin-bottom: 2px;"><img src="img/'. UserMatchMarks::PIC_CARDS .'.png"></li>
                </ul>
              </li>
              ';
        },
        'format' => 'raw',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'blue_dot',
        'header' => '&nbsp;',
        'contentOptions' => ['class' => 'hidden-column'],
        'headerOptions' => ['class' => 'hidden-column'],
        'hidden' => true,
        'width' => '3.1%',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'value' => function($model) {
            return '<div class="mark" data-value="0" data-mark="blue_dot">&nbsp;</div>';
        },
        'format' => 'raw',
    ],
    $redDotColumn,
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'light_blue_dot',
        'header' => '&nbsp;',
        'width' => '25px',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'value' => function($model) {
            return '<div class="mark" data-value="0" data-mark="light_blue_dot">&nbsp;</div>';
        },
        'format' => 'raw',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'black_dot',
        'header' => '&nbsp;',
        'width' => '25px',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'value' => function($model) {
            return '<div class="mark" data-value="0" data-mark="black_dot">&nbsp;</div>';
        },
        'format' => 'raw',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'flag',
        'width' => '25px',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'value' => function($model) {
            return '<span class="'.$model['flag'].'">&nbsp;</span>';
        },
        'format' => 'raw',
        'header' => '&nbsp;',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'mh0',
        'width' => '25px',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'header' => ($historyColumnCompetition[0] ?? '&nbsp;') . '<br>' . ($historyColumnDates[0] ?? '&nbsp;'),
        'headerOptions' => [
            'style' => 'font-size: 12px;',
        ],
        'visible' => ($matchCount >= 1),
        'value' => function($model) {
            if (!isset($model['mh0'])) {
                return '<div class="">&nbsp;</div>';
            } elseif ($model['mh0'] == 1) {
                return '<div class="green-dot">&nbsp;</div>';
            } else if ($model['mh0'] == 2) {
                return '<div class="yellow-dot">&nbsp;</div>';
            }
        },
        'format' => 'raw',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'mh1',
        'width' => '25px',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'header' => ($historyColumnCompetition[1] ?? '&nbsp;') . '<br>' . ($historyColumnDates[1] ?? '&nbsp;'),
        'headerOptions' => [
            'style' => 'font-size: 12px;',
        ],
        'visible' => ($matchCount >= 2),
        'value' => function($model) {
            if (!isset($model['mh1'])) {
                return '<div class="">&nbsp;</div>';
            } elseif ($model['mh1'] == 1) {
                return '<div class="green-dot">&nbsp;</div>';
            } else if ($model['mh1'] == 2) {
                return '<div class="yellow-dot">&nbsp;</div>';
            }
        },
        'format' => 'raw',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'mh2',
        'width' => '25px',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'header' => ($historyColumnCompetition[2] ?? '&nbsp;') . '<br>' . ($historyColumnDates[2] ?? '&nbsp;'),
        'headerOptions' => [
            'style' => 'font-size: 12px;',
        ],
        'visible' => ($matchCount >= 3),
        'value' => function($model) {
            if (!isset($model['mh2'])) {
                return '<div class="">&nbsp;</div>';
            } elseif ($model['mh2'] == 1) {
                return '<div class="green-dot">&nbsp;</div>';
            } else if ($model['mh2'] == 2) {
                return '<div class="yellow-dot">&nbsp;</div>';
            }
        },
        'format' => 'raw',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'mh3',
        'width' => '25px',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'header' => ($historyColumnCompetition[3] ?? '&nbsp;') . '<br>' . ($historyColumnDates[3] ?? '&nbsp;'),
        'headerOptions' => [
            'style' => 'font-size: 12px;',
        ],
        'visible' => ($matchCount >= 4),
        'value' => function($model) {
            if (!isset($model['mh3'])) {
                return '<div class="">&nbsp;</div>';
            } elseif ($model['mh3'] == 1) {
                return '<div class="green-dot">&nbsp;</div>';
            } else if ($model['mh3'] == 2) {
                return '<div class="yellow-dot">&nbsp;</div>';
            }
        },
        'format' => 'raw',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'mh5',
        'width' => '25px',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'header' => ($historyColumnCompetition[4] ?? '&nbsp;') . '<br>' . ($historyColumnDates[4] ?? '&nbsp;'),
        'headerOptions' => [
            'style' => 'font-size: 12px;',
        ],
        'visible' => ($matchCount >= 5),
        'value' => function($model) {
            if (!isset($model['mh4'])) {
                return '<div class="">&nbsp;</div>';
            } elseif ($model['mh4'] == 1) {
                return '<div class="green-dot">&nbsp;</div>';
            } else if ($model['mh4'] == 2) {
                return '<div class="yellow-dot">&nbsp;</div>';
            }
        },
        'format' => 'raw',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'h2h',
        'width' => '25px',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'header' => ($matchInfo['h2h_competition'] ?? '-') . '<br>' . ($matchInfo['h2h_date'] ?? '-.-'),
        'headerOptions' => [
            'style' => 'font-size: 12px;',
        ],
        'value' => function($model) {
            if (!isset($model['h2h'])) {
                return '<div class="">&nbsp;</div>';
            } elseif ($model['h2h'] == 1) {
                return '<div class="fial-dot">&nbsp;</div>';
            } else if ($model['h2h'] == 2) {
                return '<div class="orange-dot">&nbsp;</div>';
            }
        },
        'format' => 'raw',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'age',
        'header' => '&nbsp;',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'value' => function($model) {
            if ($model['age'] <= 20) {
                return '<div style="background-color: yellow">'.$model['age'].'</div>';
            } else {
                return $model['age'];
            }
        },
        'format' => 'raw',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'position',
        'header' => '&nbsp;',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'minutes_played',
        'header' => '<img src="http://s1.swimg.net/gsmf/578/img/events/minute_played.png" width="13" height="13" alt="Minutes played" title="Minutes played" />',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'appearances',
        'header' => '<img src="http://s1.swimg.net/gsmf/578/img/events/appearance.png" width="13" height="13" alt="Appearances" title="Appearances" />',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'lineups',
        'header' => '<img src="http://s1.swimg.net/gsmf/578/img/events/L.png" width="13" height="13" alt="Lineups" title="Lineups" />',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'goals',
        'contentOptions' => [
            'class' => 'goal-td',
        ],
        'header' => '<img src="http://s1.swimg.net/gsmf/578/img/events/G.png" width="13" height="13" alt="Goal" title="Goal" />',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'assists',
        'contentOptions' => [
            'class' => 'assist-td',
        ],
        'header' => '<img src="http://s1.swimg.net/gsmf/578/img/events/AS.png" width="13" height="13" alt="Assist" title="Assist" />',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'yellow_cards',
        'header' => '<img src="http://s1.swimg.net/gsmf/578/img/events/YC.png" width="13" height="13" alt="Yellow card" title="Yellow card" />',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'nd_yellow_cards',
        'header' => '<img src="http://s1.swimg.net/gsmf/578/img/events/Y2C.png" width="13" height="13" alt="Yellow 2nd/RC" title="Yellow 2nd/RC" />',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'red_cards',
        'header' => '<img src="http://s1.swimg.net/gsmf/578/img/events/RC.png" width="13" height="13" alt="Red card" title="Red card" />',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'contentOptions' => ['style' => 'min-width:20px;'],
    ],
];

return $columnArr;