<div class="row commands-info">
    <div class="col-md-5" style="margin-left: 108px;">
        <div class="col-md-7 pull-right">
            <div class="info-box">
                <span class="info-box-icon bg-aqua pull-right"><img src="<?= $matchInfo['team_1_logo'] ?>"></span>
                <div class="info-box-content" style="margin-left: 0; margin-right: 90px;">
                    <span class="text-right info-box-text team-1-name <?php if (Yii::$app->user->can('addForeignName')): ?>add-foreign-name" style="cursor: pointer"<?php else: ?>"<?php endif; ?> data-team_id="<?= $matchInfo['id_club_1'] ?>" data-name="<?= $matchInfo['team_1'] ?>">
                    <b><?= $matchInfo['team_1'] ?></b> <?php if($matchInfo['neutral_venue']): ?> (n)<?php endif; ?>
                    </span>
                    <p class="text-right"><a target="_blank" href="<?= 'http://'.$matchInfo['team_1_site'] ?>"><?= $matchInfo['team_1_site'] ?></a></p>
                    <p class="text-right"><?= $matchInfo['team_1_city'] ?></p>
                    <p class="text-right" id="cost-team-1"></p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-1" style="margin-left: -44px; margin-right: 36px;">
        <p class="center-block text-center" id="match-info" data-match_date="<?= $matchInfo['date_game'] ?>" data-soccerway_url="<?= $matchInfo['soccerway_url'] ?>" data-match_url="<?= $matchInfo['url'] ?>" data-match_id="<?= $matchInfo['id'] ?>"><?= $matchInfo['goalsOrTime'] ?></p>
        <p class="center-block text-center"><?= (new DateTime('@'.$matchInfo['date_game']))->setTimezone((new \DateTimeZone(Yii::$app->user->identity->timezone)))->format('M-d'); ?></p>
        <p class="center-block text-center"><?= $matchInfo['venue'] ?></p>
    </div>
    <div class="col-md-5" style="margin-left: -80px;">
        <div class="col-md-7">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><img src="<?= $matchInfo['team_2_logo'] ?>"></span>

                <div class="info-box-content">
                    <span class="info-box-text team-2-name <?php if (Yii::$app->user->can('addForeignName')): ?>add-foreign-name" style="cursor: pointer"<?php else: ?>"<?php endif; ?> data-team_id="<?= $matchInfo['id_club_2'] ?>" data-name="<?= $matchInfo['team_2'] ?>">
                        <b><?= $matchInfo['team_2'] ?></b>
                    </span>
                    <p><a target="_blank" href="<?= 'http://'.$matchInfo['team_2_site'] ?>"><?= $matchInfo['team_2_site'] ?></a></p>
                    <p><?= $matchInfo['team_2_city'] ?></p>
                    <p id="cost-team-2"></p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6" id="team-1-table">
        <?php
        if (isset($dataProviderTeam1)) {
            if ($dataProviderTeam1) {
                $historyMarks = '<div class="history-marks-container">';
                foreach ($matchInfo['team_1_history_marks'] as $mark) {
                    if ($mark == 'win') $historyMarks .= '<span class="history-matches-icon history-win">W</span>';
                    if ($mark == 'draw') $historyMarks .= '<span class="history-matches-icon history-draw">W</span>';
                    if ($mark == 'loss') $historyMarks .= '<span class="history-matches-icon history-loss">W</span>';
                }
                $historyMarks .= '</div>';
                $historyColumnDates = $matchInfo['team_1_history_dates'];
                $historyColumnCompetition = $matchInfo['team_1_history_competition'];
                echo \kartik\grid\GridView::widget([
                    'id' => 'crud-datatable',
                    'dataProvider' => $dataProviderTeam1,
                    'columns' => require(__DIR__ . '/_columns.php'),
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'rowOptions' => function($model){
                        return [
                            'style' => "font-size: 12px;",
                            'data-id' => $model['id'],
                            'data-url' => $model['url'],
                            'data-names' => $model['namesStr'],
                            'class' => 'player-row',
                        ];
                    },
                    'containerOptions' => [
                        'style' => 'overflow: inherit;'
                    ],
                    'summary' => '',
                    'hover' => true,
                ]);
            } else {
                echo '<div class="error">Нет информации о команде</div>';
            }
        }
        ?>
    </div>
    <div class="col-md-6" id="team-2-table">
        <?php
        if (isset($dataProviderTeam2)) {
            if ($dataProviderTeam2) {
                $historyMarks = '<div class="history-marks-container">';;
                foreach ($matchInfo['team_2_history_marks'] as $mark) {
                    if ($mark == 'win') $historyMarks .= '<span class="history-matches-icon history-win">W</span>';
                    if ($mark == 'draw') $historyMarks .= '<span class="history-matches-icon history-draw">W</span>';
                    if ($mark == 'loss') $historyMarks .= '<span class="history-matches-icon history-loss">W</span>';
                }
                $historyMarks .= '</div>';
                $historyColumnDates = $matchInfo['team_2_history_dates'];
                $historyColumnCompetition = $matchInfo['team_2_history_competition'];
                echo \kartik\grid\GridView::widget([
                    'id' => 'crud-datatable',
                    'dataProvider' => $dataProviderTeam2,
                    'columns' => require(__DIR__ . '/_columns.php'),
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'rowOptions' => function($model){
                        return [
                            'style' => "font-size: 12px; border: 1px solid #e0e8ea;",
                            'data-id' => $model['id'],
                            'data-url' => $model['url'],
                            'data-names' => $model['namesStr'],
                            'class' => 'player-row',
                        ];
                    },
                    'containerOptions' => [
                        'style' => 'overflow: inherit;'
                    ],
                    'summary' => '',
                    'hover' => true,
                ]);
            } else {
                echo '<div class="error">Нет информации о команде</div>';
            }
        }
        ?>
    </div>
</div>

<div class="row players-cost-container">
    <div class="col-md-6">
        <p id="cost-players-1"></p>
        <p id="cost-line-up-players-1"></p>
        <p id="cost-possible-players-1"></p>
        <p id="cost-possible-2-players-1"></p>
    </div>
    <div class="col-md-6">
        <p id="cost-players-2"></p>
        <p id="cost-line-up-players-2"></p>
        <p id="cost-possible-players-2"></p>
        <p id="cost-possible-2-players-2"></p>
    </div>
</div>

<div class="row">
    <div class="col-md-5"></div>
    <div class="col-md-2 text-center">
        <button type="submit" id="mark-players-button" class="btn btn-primary">Игроки</button>
        <button type="submit" id="orders-forms-button" class="btn btn-primary">Формы заявок</button>
    </div>
    <div class="col-md-5"></div>
</div>
<br>
<div class="row hide" id="mark-players-row">
    <div class="col-md-6" id="team-1-mark-players">
        <div class="box">
            <div class="box-header with-border"></div>
            <div class="box-body">
                <div class="player_status_2">New: <p></p></div>
                <div class="player_status_1">Old: <p></p></div>
                <div class="player_status_4">Doubtfull: <p></p></div>
                <div class="player_status_3">Return: <p></p></div>
            </div>
        </div>
    </div>
    <div class="col-md-6" id="team-2-mark-players">
        <div class="box">
            <div class="box-header with-border"></div>
            <div class="box-body">
                <div class="player_status_2">New: <p></p></div>
                <div class="player_status_1">Old: <p></p></div>
                <div class="player_status_4">Doubtfull: <p></p></div>
                <div class="player_status_3">Return: <p></p></div>
            </div>
        </div>
    </div>
</div>

<div class="row hide" id="orders-forms-row">
    <div class="col-md-6 form-group" id="team-1-order">
        <textarea class="form-control" rows="3"></textarea>
        <div class="btn-group">
            <?php if(!$lineups): ?>
                <button type="button" class="btn btn-danger btn-xs" data-dot="red_dot">Старт</button>
            <?php endif; ?>
            <button type="button" class="btn btn-default btn-xs" data-dot="black_dot">Заявка</button>
            <button type="button" class="btn btn-info btn-xs" data-dot="light_blue_dot">Предв.</button>
            <button type="button" class="btn btn-primary btn-xs" data-dot="blue_dot">Предв. 2</button>
        </div>
    </div>
    <div class="col-md-6 form-group" id="team-2-order">
        <textarea class="form-control" rows="3"></textarea>
        <div class="btn-group">
            <?php if(!$lineups): ?>
                <button type="button" class="btn btn-danger btn-xs" data-dot="red_dot">Старт</button>
            <?php endif; ?>
            <button type="button" class="btn btn-default btn-xs" data-dot="black_dot">Заявка</button>
            <button type="button" class="btn btn-info btn-xs" data-dot="light_blue_dot">Предв.</button>
            <button type="button" class="btn btn-primary btn-xs" data-dot="blue_dot">Предв. 2</button>
        </div>
    </div>
</div>

<div class="row" id="comments">
    <div class="col-md-6" id="comments-team-1">
        <div class="box box-primary direct-chat direct-chat-primary collapsed-box">
            <div class="box-header with-border">
                <h3 class="box-title">Комментарии</h3>

                <div class="box-tools pull-right">
                    <span data-toggle="tooltip" title="" class="badge bg-light-blue" data-original-title="3"><?= count($comments['team_1']) ?? 0 ?></span>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="direct-chat-messages comments-container">
                <?php
                    /* @var $comment \app\models\Comment */
                    if ($comments['team_1']) {
                        foreach ($comments['team_1'] as $comment) {
                            include '_comment.php';
                        }
                    }
                ?>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <div class="input-group">
                    <input type="text" name="message" placeholder="" autocomplete="off" class="form-control">
                    <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary btn-flat" data-team="1">Отправить</button>
                  </span>
                </div>
            </div>
            <!-- /.box-footer-->
        </div>
    </div>
    <div class="col-md-6" id="comments-team-2">
        <div class="box box-primary direct-chat direct-chat-primary collapsed-box">
            <div class="box-header with-border">
                <h3 class="box-title">Комментарии</h3>

                <div class="box-tools pull-right">
                    <span data-toggle="tooltip" title="" class="badge bg-light-blue" data-original-title="3"><?= count($comments['team_2']) ?? 0 ?></span>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="direct-chat-messages comments-container">
                    <?php
                    /* @var $comment \app\models\Comment */
                    if ($comments['team_2']) {
                        foreach ($comments['team_2'] as $comment) {
                            include '_comment.php';
                        }
                    }
                    ?>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <div class="input-group">
                    <input type="text" name="message" placeholder="" autocomplete="off" class="form-control">
                    <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary btn-flat" data-team="2">Отправить</button>
                  </span>
                </div>
            </div>
            <!-- /.box-footer-->
        </div>
    </div>
</div>

    <script>lineupsMatch = <?php if ($lineups >= 1) {echo 'true';} else {echo 'false';} ?></script>
