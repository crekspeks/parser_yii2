<?php
/* @var $this yii\web\View */
use kartik\nav\NavX;

if (isset($matchInfo)) {
    $this->title = $matchInfo['team_1'].' - '.$matchInfo['team_2'];
} else {
    $this->title = Yii::$app->name;
}
$this->blocks['content-header'] = '';
?>

<div id="match-table-container">
<?php
    if (isset($matchInfo)) {
        include '_match.php';
    }
?>
</div>

<div class="modal fade" id="modal-select-match" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Матчи</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div id="overlay">
    <i class="fa fa-refresh fa-spin"></i>
</div>

<div class="modal fade" id="modal-edit-comment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Изменить комментарий</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <textarea class="form-control rounded-0" id="edit-comment-textarea" rows="3"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="save-edit-comment">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-edit-team" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Данные команды</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="betexplorer_name" class="col-sm-2 control-label">Betexplorer Name</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="betexplorer_name" placeholder="Betexplorer Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="oddsportal_name" class="col-sm-2 control-label">Oddsportal Name</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="oddsportal_name" placeholder="Oddsportal Name">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="save-team">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-player-names" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

</div>

<div id="overlay">
    <i class="fa fa-refresh fa-spin"></i>
</div>