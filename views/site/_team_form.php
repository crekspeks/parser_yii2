<?php

/* @var $team \app\models\Team */
?>

<form class="form-horizontal">
    <div class="box-body">
        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
        <input type="hidden" name="Team[soccerway_id]" class="form-control" id="oddsportal_name" placeholder="Oddsportal Name" value="<?= $team->soccerway_id ?>">
        <div class="form-group">
            <label for="betexplorer_name" class="col-sm-2 control-label">Betexplorer Name</label>

            <div class="col-sm-10">
                <input type="text" name="Team[betexplorer_name]" class="form-control" id="betexplorer_name" placeholder="Betexplorer Name" value="<?= $team->betexplorer_name ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="oddsportal_name" class="col-sm-2 control-label">Oddsportal Name</label>

            <div class="col-sm-10">
                <input type="text" name="Team[oddsportal_name]" class="form-control" id="oddsportal_name" placeholder="Oddsportal Name" value="<?= $team->oddsportal_name ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="transfermarkt_name" class="col-sm-2 control-label">Transfermarkt Name</label>

            <div class="col-sm-10">
                <input type="text" name="Team[transfermarkt_name]" class="form-control" id="transfermarkt_name" placeholder="Transfermarkt Name" value="<?= $team->transfermarkt_name ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="besoccer_name" class="col-sm-2 control-label">Besoccer Name</label>

            <div class="col-sm-10">
                <input type="text" name="Team[besoccer_name]" class="form-control" id="besoccer_name" placeholder="Besoccer Name" value="<?= $team->besoccer_name ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="sofascore_name" class="col-sm-2 control-label">Sofascore Name</label>

            <div class="col-sm-10">
                <input type="text" name="Team[sofascore_name]" class="form-control" id="sofascore_name" placeholder="Sofascore Name" value="<?= $team->sofascore_name ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="sofascore_name" class="col-sm-2 control-label">Team Site</label>

            <div class="col-sm-10">
                <input type="text" name="Team[team_site]" class="form-control" id="team_site" placeholder="Team Site Name" value="<?= $team->team_site ?>">
            </div>
        </div>
    </div>
</form>