<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 03.07.2018
 * Time: 21:30
 */
?>

<ul class="menu">
    <?php foreach ($notificationsReplacementsArr as $replacement): ?>
    <li style="background-color: #f9f27c" class="text-center">
        <a href="/?matchUrl=<?= $replacement['url'] ?>&replacement=<?= $replacement['replacement_match_id']?>">
            <?= $replacement['team_1'] ?>
            -
            <?= $replacement['team_2'] ?>
        </a>
    </li>
    <?php endforeach; ?>
</ul>
