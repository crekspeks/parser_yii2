<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 03.07.2018
 * Time: 21:30
 */
?>

<ul class="menu">
    <?php foreach ($notificationsArr as $match): ?>
    <li style="background-color: #f9f27c" class="text-center">
        <a href="/?matchUrl=<?= $match['url'] ?>&lineups=<?= $match['match_id']?>">
            <i class="fa fa-warning text-yellow"></i><?= $match['team_1_changes']?> <?= $match['team_1'] ?> - <?= $match['team_2_changes']?> <?= $match['team_2'] ?>
        </a>
    </li>
    <?php endforeach; ?>
</ul>
