<?php foreach ($savedMatches as $cup => $matches): ?>
    <span class="control-sidebar-subheading"><?= $cup ?></span>
    <?php foreach ($matches as $match): ?>
        <li>
            <a style="color: white; float: left" data-url="<?= $match['url'] ?>" href="/?matchUrl=<?= $match['url'] ?>"><?= $match['name'] ?></a>
            <span class="text-red del-saved-match" data-id="<?= $match['id'] ?>" style="z-index: 11111111; display: inline-block; cursor: pointer;">
                <i class="fa fa-trash-o"></i>
            </span>
        </li>
    <?php endforeach; ?>
<?php endforeach; ?>