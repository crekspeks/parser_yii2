<?php foreach ($matchesArr as $match): ?>
    <tr data-id="<?= $match->id ?>" class="<?php if($match->status == 1 || $match->status == 2) {echo 'has-lineups ';}?>" data-time="<?= $match->date ?>000">
        <td style="width: 20%;">
            <div class="text-center">
                <?= $match->select->parent->name ?> - <?= $match->select->name ?>
            </div>
        </td>
        <td style="width: 30%;">
            <div class="text-right">
                <?php if ($match->besoccer_name_mark_team_1): ?><img src="/img/besoccer.ico" style="height: 20px"><?php endif; ?>
                <?php if ($match->sofascore_name_mark_team_1): ?><img src="/img/sofascore.ico" style="height: 20px"><?php endif; ?>
                <a href="/?matchUrl=<?= $match->url ?><?php if(!is_null($match->players_json)) {echo '&lineups='.$match->id;} ?>" target="_blank">
                    <span class="text-red"><?= $match->team_1_changes ?? ''?></span> <?= $match->team_1 ?>
                </a>
            </div>
        </td>
        <td style="width: 5%;">
            <div class="text-center">
                <?php if($match) ?>
                <a href="/?matchUrl=<?= $match->url ?><?php if(!is_null($match->players_json)) {echo '&lineups='.$match->id;} ?>" target="_blank">
                    <?= (new DateTime('@'.$match['date']))->setTimezone((new \DateTimeZone(Yii::$app->user->identity->timezone)))->format('H:i'); ?>
                </a>
            </div>
        </td>
        <td style="width: 30%;">
            <div class="text-left">
                <a href="/?matchUrl=<?= $match->url ?><?php if(!is_null($match->players_json)) {echo '&lineups='.$match->id;} ?>" target="_blank">
                    <?= $match->team_2 ?> <span class="text-red"><?= $match->team_2_changes ?? '' ?></span>
                </a>
                <?php if ($match->besoccer_name_mark_team_2): ?><img src="/img/besoccer.ico" style="height: 20px"><?php endif; ?>
                <?php if ($match->sofascore_name_mark_team_2): ?><img src="/img/sofascore.ico" style="height: 20px"><?php endif; ?>
            </div>
        </td>
        <td style="width: 15%;">
            <div class="checkbox no-margin">
                <label>
                    <input id="subscribe-checkbox" class="<?php if($match->source){echo 'has-changes';} ?>" data-id="<?= $match->id ?>" type="checkbox" <?php if($match->userSubscribe):?> checked <?php endif; ?>>
                </label>
            </div>
        </td>
    </tr>

<?php endforeach; ?>