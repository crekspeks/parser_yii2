<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 21.06.2018
 * Time: 8:05
 */

/**@var $match \app\models\MatchesLineups**/
$this->title = 'Lineups - '.Yii::$app->name;
$this->blocks['content-header'] = '';
?>
<div class="row">
    <div class="box">
        <div class="box-body no-padding">
        <table class="table" id="lineups-table">
            <thead>
                <tr>
                    <td style="width: 20%"></td>
                    <td style="width: 30%"></td>
                    <td style="width: 5%"></td>
                    <td style="width: 30%"></td>
                    <td style="width: 15%">
                        <div class="checkbox no-margin">
                            <label><input id="subscribe-checkbox-all" <?php if(\Yii::$app->user->identity->lineups_subscribe): ?>checked<?php endif; ?> type="checkbox">Подписаться на все</label>
                        </div>
                    </td>
                </tr>
            </thead>
            <tbody>
            <?php include '_lineups_matches.php' ?>
            </tbody>
        </table>
        </div>
    </div>
</div>