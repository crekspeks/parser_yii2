<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\UpdateUserForm */
/* @var $form yii\widgets\ActiveForm */

$timezonesArr = [];
foreach (\DateTimeZone::listIdentifiers(\DateTimeZone::EUROPE) as $timezone) {
    $timezonesArr[$timezone] = $timezone;
}
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput() ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'email')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([User::STATUS_ACTIVE => 'Active', User::STATUS_DELETED => 'Delited']) ?>

    <?= $form->field($model, 'timezone')->dropDownList($timezonesArr) ?>

    <?= $form->field($model, 'active_until')->widget(\kartik\datetime\DateTimePicker::class, [
        'pluginOptions' => [
            'startDate' => '-1d',
            'format' => 'dd-M-yyyy hh:ii',
            'todayHighlight' => true
        ]
    ]) ?>

    <?php if (!$model->isNewRecord && $model->haveActiveSession()): ?>
        <p>Есть активная сессия <?= Html::a('Завершить', ['user/end-session', 'id' => $model->id]) ?></p>
    <?php endif; ?>

    <?php ActiveForm::end(); ?>

</div>
