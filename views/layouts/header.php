<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">MTP</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <?php if(Yii::$app->user->can('sAdminMenu')): ?>
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
        <?php endif; ?>
        <div class="col-md-5">
        <?php
        if (Yii::$app->controller->id == 'site' and Yii::$app->controller->action->id = 'index') {
            echo \kartik\nav\NavX::widget([
                'options' => ['class' => 'nav nav-pills', 'id' => 'match-select'],
                'items' => ['label' => '<input id="match-count" class="form-control nav-match-count-input" type="number" min="0" max="5" value="' . $this->params['matchCount'] . '">'] + $this->params['selectArr'],
            ]);
        }
        ?>
        </div>
        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                <?php if (Yii::$app->controller->id == 'site' and Yii::$app->controller->action->id = 'index'): ?>
                    <li>
                        <a href="#" target="_blank" id="soccerwy-link" ><i class="fa fa-refresh fa-spin hidden"></i>Soccerway</a>
                    </li>
                    <li class="disabled">
                        <a href="#" target="_blank" id="betexplorer-link" ><i class="fa fa-refresh fa-spin hidden"></i>Betexplorer</a>
                    </li>
                    <li class="disabled">
                        <a href="#" target="_blank" id="oddsportal-link"><i class="fa fa-refresh fa-spin hidden"></i>Oddsportal</a>
                    </li>
                    <li>
                        <a href="#" id="get-transfermarkt"><i class="fa fa-refresh fa-spin hidden"></i>Transfermarkt</a>
                    </li>
                    <li>
                        <a href="#" id="save">Сохранить</a>
                    </li>
                <?php endif; ?>
                <?php if (\Yii::$app->user->can('incidentsMatches')): ?>
                    <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-flag-o"></i>
                            <span class="label label-warning" id="replacements-count">0</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">Последние события</li>
                            <li id="replacements">

                            </li>
                            <li class="footer"><a href="/replacements">View all</a></li>
                        </ul>
                    </li>
                <?php endif; ?>
                <?php if (\Yii::$app->user->can('lineupsMatches')): ?>
                    <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-warning" id="notifications-count">0</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">Непросмотренные матчи</li>
                            <li id="notifications">

                            </li>
                            <li class="footer"><a href="/lineups">View all</a></li>
                        </ul>
                    </li>
                <?php endif; ?>
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/img/user.png" class="user-image" alt="User Image"/>
                        <span class="hidden-xs"><?= Yii::$app->user->identity->username ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="/img/user.png" class="img-circle"
                                 alt="User Image"/>
                            <p>
                                <?= Yii::$app->user->identity->username ?>
                                <small><?= Yii::$app->user->identity->email ?></small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">

                            <div class="pull-right">
                                <?= Html::a(
                                    'Sign out',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-save"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>
