<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

?>
<div class="content-wrapper">
    <section class="content-header">
        <?php if (isset($this->blocks['content-header'])) { ?>
            <h1><?= $this->blocks['content-header'] ?></h1>
        <?php } else { ?>
            <h1>
                <?php
                if ($this->title !== null) {
                    echo \yii\helpers\Html::encode($this->title);
                } else {
                    echo \yii\helpers\Inflector::camel2words(
                        \yii\helpers\Inflector::id2camel($this->context->module->id)
                    );
                    echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
                } ?>
            </h1>
        <?php } ?>

        <?=
        Breadcrumbs::widget(
            [
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]
        ) ?>
    </section>

    <section class="content">
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>

<footer class="main-footer">

</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark" id="saved-matches-sidebar">
    <!-- Tab panes -->
    <div class="tab-content">
        <!-- Home tab content -->
        <div class="tab-pane active" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Сохраненные матчи</h3>
            <ul class='control-sidebar-menu' id="saved-matches-container" style="margin: 0 0 0 0;">
                <?php if (isset($this->params['savedMatches']) && $this->params['savedMatches']): ?>
                    <?php foreach ($this->params['savedMatches'] as $cup => $matches): ?>
                    <span class="control-sidebar-subheading"><?= $cup ?></span>
                        <?php foreach ($matches as $match): ?>
                            <li>
                                <a style="color: white; float: left" data-url="<?= $match['url'] ?>" href="/?matchUrl=<?= $match['url'] ?>"><?= $match['name'] ?></a>
                                <span class="text-red del-saved-match" data-id="<?= $match['id'] ?>" style="z-index: 11111111; display: inline-block; cursor: pointer;">
                                    <i class="fa fa-trash-o"></i>
                                </span>
                            </li>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>
            <!-- /.control-sidebar-menu -->
            <!-- /.control-sidebar-menu -->

        </div>
    </div>
</aside><!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class='control-sidebar-bg' id="saved-matches-sidebar"></div>