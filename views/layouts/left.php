<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/img/user.png" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->username ?></p>
            </div>
        </div>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    ['label' => 'Users', 'icon' => 'fa fa-user', 'url' => ['/user']],
                    ['label' => 'RBAC',
                        'items' => [
                            ['label' => 'Rule', 'url' => ['/rbac/rule']],
                            ['label' => 'Permission', 'url' => ['/rbac/permission']],
                            ['label' => 'Role', 'url' => ['/rbac/role']],
                            ['label' => 'Assigment', 'url' => ['/rbac/assignment']],
                        ]
                    ],
                    ['label' => 'LineupsSettings', 'icon' => 'fa fa-gear', 'url' => ['/lineups/settings']],
                    ['label' => 'IncidentsSettings', 'icon' => 'fa fa-gear', 'url' => ['/replacements/settings']],
                ],
            ]
        ) ?>

    </section>

</aside>