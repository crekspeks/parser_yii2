<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 21.06.2018
 * Time: 8:05
 */

/**@var $match \app\models\MatchesLineups**/
$this->title = 'Replacements - '.Yii::$app->name;
$this->blocks['content-header'] = '';
?>
<div class="row">
    <div class="box">
        <div class="box-body no-padding">
        <table class="table" id="replacements-table">
            <thead>
                <tr>
                    <td style="width: 15%"></td>
                    <td style="width: 35%"></td>
                    <td style="width: 5%"></td>
                    <td style="width: 35%"></td>
                </tr>
            </thead>
            <tbody>
            <?php include '_replacement_matches.php' ?>
            </tbody>
        </table>
        </div>
    </div>
</div>