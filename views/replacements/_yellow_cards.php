<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Настройка желтых карточек</h4>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <textarea class="form-control rounded-0" id="edit-yellow-cards-textarea" rows="3"><?= $select->yellow_cards_set ?></textarea>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="save-yellow-cards" data-id="<?= $select->id ?>">Сохранить</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        </div>
    </div>
</div>