<?php
/**
 * @var $matchesArr \app\models\ReplacementMatches[]
 */

$date = 0;
foreach ($matchesArr as $match): ?>
    <?php
    $match_date = (new DateTime('@'.$match->start_time))->setTimezone((new \DateTimeZone(Yii::$app->user->identity->timezone)))->format('d F');
    if ($date != $match_date):
        $date = $match_date;
        ?>
        <tr>
            <td style="width: 15%"></td>
            <td style="width: 30%"></td>
            <td style="width: 15%"><div class="text-center"><?= $match_date ?></div></td>
            <td style="width: 30%"></td>
        </tr>
    <?php endif; ?>
    <tr data-id="<?= $match->id ?>" class="<?php if($match->haveNewNotifications()) {echo 'has-lineups ';}?>" data-time="<?= $match->start_time ?>000">
        <td style="width: 15%;">
            <div class="text-center">
                <?= $match->select->parent->name ?> - <?= $match->select->name ?>
            </div>
        </td>
        <td style="width: 35%;">
            <div class="text-right">
                <?php $marks = $match->getTeam1IncidentsArray(); include '_incidents_marks.php' ?>
                <a href="/?matchUrl=<?= $match->url ?><?php if(!is_null($match->team_1_incidents) || !is_null($match->team_2_incidents)) {echo "&replacement=$match->id";}?>" target="_blank">
                    <span class="text-red"><?php if ($match->itLiveTeam1()) echo  'L I V E'; ?></span> <?= $match->team_1 ?>
                </a>
            </div>
        </td>
        <td style="width: 5%;">
            <div class="text-center">
                <?php if($match) ?>
                <a href="/?matchUrl=<?= $match->url ?><?php if(!is_null($match->team_1_incidents) || !is_null($match->team_2_incidents)) {echo "&replacement=$match->id";}?>" target="_blank">
                    <?= (new DateTime('@'.$match->start_time))->setTimezone((new \DateTimeZone(Yii::$app->user->identity->timezone)))->format('H:i'); ?>
                </a>
            </div>
        </td>
        <td style="width: 35%;">
            <div class="text-left">
                <a href="/?matchUrl=<?= $match->url ?><?php if(!is_null($match->team_1_incidents) || !is_null($match->team_2_incidents)) {echo "&replacement=$match->id";}?>" target="_blank">
                    <?= $match->team_2 ?> <span class="text-red"><?php if ($match->itLiveTeam2()) echo  'L I V E'; ?></span>
                </a>
                <?php $marks = $match->getTeam2IncidentsArray(); include '_incidents_marks.php' ?>
            </div>
        </td>
    </tr>

<?php endforeach; ?>