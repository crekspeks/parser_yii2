<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 21.06.2018
 * Time: 6:47
 */
/**@var $select \app\models\Select**/
?>

<div class="row">
    <a href="/replacements/get-new-matches">Загрузить новые матчи</a>
    <?php foreach ($selects as $select): ?>
        <div class="col-md-4">
            <div class="box box-primary collapsed-box">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= $select->name ?></h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body" style="display: none;">
                    <?php foreach ($select->selects as $secondSelect): ?>
                        <div class="box box-success collapsed-box">
                            <div class="box-header with-border">
                                <h3 class="box-title"><?= $secondSelect->name ?></h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body" style="display: none;">
                                <?php foreach ($secondSelect->selects as $thirdSelect): ?>
                                    <div class="box box-default collapsed-box">
                                        <div class="box-header with-border">
                                            <h4 class="box-title"><?= $thirdSelect->name ?></h4>
                                            <div class="box-tools pull-right">
                                                <div class="checkbox badge" style="background-color: inherit">
                                                    <label>
                                                        <input id="replacements-checkbox" data-id="<?= $thirdSelect->id ?>" type="checkbox" <?php if($thirdSelect->replacements_parse):?> checked <?php endif;?>>
                                                    </label>
                                                </div>
                                                <button type="button" data-id="<?= $thirdSelect->id ?>"  class="btn btn-box-tool replacements-yellow-cards">
                                                    <i class="fa fa-credit-card"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
