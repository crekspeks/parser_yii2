var unreadNotification = 0;
var pageTitle;

$(document).ready(function() {
    $(document).on('click', '#select-checkbox', checkSelect);
    $(document).on('click', '#subscribe-checkbox', matchSubscribe);
    $(document).on('click', '#replacements-checkbox', checkReplaceSelect);
    $(document).on('click', '#subscribe-checkbox-all', matchSubscribeAll);

    $(document).on('click', '#match-select li li li', loadMatches);
    if (typeof lineupsMatch !== 'undefined') {
        if (!lineupsMatch) {
            $(document).on('click', '.mark', changeMark);
            $(document).on('click', '.name-select', changeSelectName);
            $(document).on('click', '.pic-select', changeSelectPic);
        }
    }
    $(document).on('click', '#modal-select-match ul li a, #saved-matches-container li a, .box-header a[data-url]', loadMatch);
    $(document).on('click', '#get-transfermarkt', getTransfermarktInfo);
    $(document).on('click', '#save', saveMatch);
    $(document).on('click', '.del-saved-match', delMatch);
    $(document).on('click', '#comments button[type="submit"]', sendComment);
    $(document).on('click', '.direct-chat-msg .fa-pencil', editComment);
    $(document).on('click', '.direct-chat-msg .fa-trash', delComment);
    $(document).on('click', '#save-edit-comment', sendEditComment);
    $(document).on('click', '.add-foreign-name', teamForm);
    $(document).on('click', '.player-row td:first-child', playerNamesForm);
    $(document).on('click', '#save-player-names', savePlayerNames);
    $(document).on('click', '.replacements-yellow-cards', yellowCardsForm);
    $(document).on('click', '#save-yellow-cards', saveYellowCards);
    $(document).on('click', '#save-team', saveTeam);
    $(document).on('click', '#team-1-table .show-column', function () {
        toggleBlueDotColumn('team-1-table')
    });
    $(document).on('click', '#team-2-table .show-column', function () {
        toggleBlueDotColumn('team-2-table')
    });
    $(document).on('click', '#mark-players-button', function () {
        $('#mark-players-row').toggleClass('hide');
        getMarkPlayersList('team-1');
        getMarkPlayersList('team-2');
    });
    $(document).on('click', '#orders-forms-button', function () {
        $('#orders-forms-row').toggleClass('hide');
    });
    $(document).on('click', '#oddsportal-link', function (e) {
        if ($(this).parent().hasClass('disabled')) {
            e.preventDefault();
        }
    });
    $(document).on('click', '#betexplorer-link', function (e) {
        if ($(this).parent().hasClass('disabled')) {
            e.preventDefault();
        }
    });
    $(document).on('click', '#lineups-table tr.has-lineups a', function (e) {
        $(this).parents('.has-lineups').removeClass('has-lineups');
    });

    $('input#match-count').keyup(function (e) {
        if (e.keyCode == 13) {
            var url = $('[data-match_url]').data('match_url');
            if (url !== undefined) {
                matchRequest(url);
            }
        }
    });

    $('#team-1-order button').click(function (e) {
        e.preventDefault();
        setOrders('team-1', $(this).data('dot'));
    });
    $('#team-2-order button').click(function (e) {
        e.preventDefault();
        setOrders('team-2', $(this).data('dot'));
    });

    $('input#match-count').val(getCookie('matchCount'));
    if ($('[data-match_id]').length) {
        onLoadMatch();
    }

    ping();
    setInterval(function () {
        ping();
    }, 30000);

    pageTitle = document.title;

    $(window).focus(function () {
        document.title = pageTitle;
        unreadNotification = 0;
    });
});

dotClasses = {
    black_dot: 'black-dot',
    red_dot: 'red-dot',
    light_blue_dot: 'light-blue-dot',
    blue_dot: 'blue-dot',
};
var matchMarks = {};

function onLoadMatch() {
    getPlayersMarks();
    getBetexplorerLink();
    getOddsLink();

    $('#soccerwy-link').prop('href', $('#match-info').data('soccerway_url'));

    if ($('#team-1-table').length){
        maxGoals('team-1-table');
        maxAssist('team-1-table');
    }
    if ($('#team-2-table').length){
        maxGoals('team-2-table');
        maxAssist('team-2-table');
    }
}

function matchRequest(url) {
    $('#modal-select-match').modal('hide');
    $('#overlay').show();
    var matchCount = $('input#match-count').val();
    setCookie('matchCount', matchCount, {expires: 60 * 60 * 24 * 40});
    window.location = '/?matchUrl='+url+'matchCount='+matchCount;
    /*$.get('/', {matchUrl: url, matchCount: matchCount})
        .done(function(data){
            $('#match-table-container').html(data);
            window.history.replaceState({}, '', '.?matchUrl='+url+'&matchCount='+matchCount);
            onLoadMatch();
            $('#get-transfermarkt').parent().removeClass('disabled');
            $('#get-transfermarkt i').addClass('hidden');
        })
        .always(function () {
            $('#overlay').hide();
    });*/
}

function loadMatches(e) {
    e.preventDefault();
    var jThis = $(this);
    $.get('/site/get-matches', {url: jThis.find('a').attr('href')}, function(data){
        $('#modal-select-match .modal-body').html(data);
        $('#modal-select-match').modal();
    });
}

function loadMatch(e) {
    e.preventDefault();
    var jThis = $(this);
    matchRequest(jThis.data('url'));
}

function changeMark(e) {
    e.preventDefault();
    var jThis = $(this);
    if (jThis.data('value') === 0){
        jThis.addClass(dotClasses[jThis.data('mark')]);
        jThis.data('value', 1);
    } else {
        jThis.removeClass(dotClasses[jThis.data('mark')]);
        jThis.data('value', 0);
    }
    sumCostPlayers();
    sendPlayersMarks();
}

function changeSelectName(e) {
    e.preventDefault();
    var jThis = $(this), jNameLabel = jThis.parents('li').find('.name-select-label');
    jNameLabel.removeClass('player_status'+jNameLabel.data('value'));
    jNameLabel.addClass('player_status'+jThis.data('value')).data('value', jThis.data('value'));

    sendPlayersMarks();
}

function changeSelectPic(e) {
    e.preventDefault();
    var jThis = $(this), jNameLabel = jThis.parents('li').find('.pic-select-label');
    jNameLabel.find('img').attr('src', 'img/'+jThis.data('value')+'.png');
    jNameLabel.data('value', jThis.data('value'));

    sendPlayersMarks();
}

function sendPlayersMarks() {
    var matchId = $('[data-match_id]').data('match_id');

    $('tr[data-id]').each(function () {
        var playerId = $(this).data('id');
        matchMarks[playerId] = {};

        $(this).find('.mark').each(function () {
            matchMarks[playerId][$(this).data('mark')] = $(this).data('value')
        });
        matchMarks[playerId]['pic'] = $(this).find('.pic-select-label').data('value');
        matchMarks[playerId]['name_color'] = $(this).find('.name-select-label').data('value');
    });
    $.post('/site/set-match-marks', {matchId: matchId, matchMarks: JSON.stringify(matchMarks)});
}

function getPlayersMarks() {
    var matchId = $('[data-match_id]').data('match_id');
    $.getJSON('/site/get-match-marks', {matchId: matchId}, function (data) {
        $.each(JSON.parse(data), function (key, value) {
            var jPlayerRow = $('[data-id="'+key+'"');
            if (value.pic > 0) {
                jPlayerRow.find('.pic-select-label').data('value', value.pic).find('img').attr('src', 'img/' + value.pic + '.png');
            }
            if (value.name_color > 0) {
                jPlayerRow.find('.name-select-label').data('value', value.name_color).addClass('player_status' + value.name_color);
            }

            if (value.red_dot > 0) {
                jPlayerRow.find('[data-mark="red_dot"]').data('value', value.red_dot).addClass(dotClasses.red_dot);
            }
            if (value.light_blue_dot > 0) {
                jPlayerRow.find('[data-mark="light_blue_dot"]').data('value', value.light_blue_dot).addClass(dotClasses.light_blue_dot);
            }
            if (value.black_dot > 0) {
                jPlayerRow.find('[data-mark="black_dot"]').data('value', value.black_dot).addClass(dotClasses.black_dot);
            }
            if (value.blue_dot > 0) {
                jPlayerRow.find('[data-mark="blue_dot"]').data('value', value.blue_dot).addClass(dotClasses.blue_dot);
            }
        });
    });
}

function getTransfermarktInfo(e) {
    e.preventDefault();
    if (!$('[data-match_id]').length) return false;
    if ($('#get-transfermarkt').parent().hasClass('disabled')) return;

    var matchInfo  = {
        team1: {
            name: $('.team-1-name').data('name'),
            players: {}
        },
        team2: {
            name: $('.team-2-name').data('name'),
            players: {}
        }
    };
    $('#team-1-table tr[data-id]').each(function () {
        var playerId = $(this).data('id');
        matchInfo.team1.players[playerId] = {};
        matchInfo.team1.players[playerId]['name'] = $(this).find('td[data-col-seq="1"] .name-select-label').text().trim();
        matchInfo.team1.players[playerId]['url'] = $(this).data('url');
        matchInfo.team1.players[playerId]['names'] = $(this).data('names');
    });
    $('#team-2-table tr[data-id]').each(function () {
        var playerId = $(this).data('id');
        matchInfo.team2.players[playerId] = {};
        matchInfo.team2.players[playerId]['name'] = $(this).find('td[data-col-seq="1"] .name-select-label').text().trim();
        matchInfo.team2.players[playerId]['url'] = $(this).data('url');
        matchInfo.team2.players[playerId]['names'] = $(this).data('names');
    });

    var team_1_soccerway_id = $('.team-1-name').data('team_id');
    var team_2_soccerway_id = $('.team-2-name').data('team_id');

    $('#get-transfermarkt').parent().addClass('disabled');
    $('#get-transfermarkt i').removeClass('hidden');
    $.post('/site/get-transfermarkt-info', {team_1_soccerway_id: team_1_soccerway_id, team_2_soccerway_id: team_2_soccerway_id, matchInfo: JSON.stringify(matchInfo)})
        .done(function (data) {
            data = JSON.parse(data);
            $('td.player-cost-column').remove();
            $('th.player-cost-column').remove();
            $('#team-1-table thead tr').append('<th class="kv-align-center player-cost-column" data-col-seq="25">€</th>');
            $('#team-2-table thead tr').append('<th class="kv-align-center player-cost-column" data-col-seq="25">€</th>');
            $.each(data.team1.players, function (key, value) {
                $('#team-1-table tr[data-id="' + key + '"]').append('<td class="kv-align-center player-cost-column" data-col-seq="25">' + value.market_value + '</td>');
                if (value.captain) {
                    $('#team-1-table tr[data-id="' + key + '"] td[data-col-seq="1"]').append('<span title="Captain" class="captain-icon icons_sprite">&nbsp;</span>');
                }
            });
            $.each(data.team2.players, function (key, value) {
                $('#team-2-table tr[data-id="' + key + '"]').append('<td class="kv-align-center player-cost-column" data-col-seq="25">' + value.market_value + '</td>');
                if (value.captain) {
                    $('#team-2-table tr[data-id="' + key + '"] td[data-col-seq="1"]').append('<span title="Captain" class="captain-icon icons_sprite">&nbsp;</span>');
                }
            });
            $('#get-transfermarkt i').addClass('hidden');
            $('#cost-team-1').html('<span class="text-blue">' + data.team1.market_value + '</span>');
            $('#cost-team-2').html('<span class="text-blue">' + data.team2.market_value + '</span>');
            sumCostPlayers();
            maxCost('team-1-table');
            maxCost('team-2-table');
        })
        .fail(function () {
            $('#get-transfermarkt').parent().removeClass('disabled');
            $('#get-transfermarkt i').addClass('hidden');
        });
}

function sumCostPlayers() {
    if (!$('th.player-cost-column').length) return;

    $('#cost-players-1').html('Sum players: <span>' + formatPlayersCost(sumDotCostPlayers(dotClasses.black_dot, 'team-1')) + ' €</span>');
    $('#cost-players-2').html('Sum players: <span>' + formatPlayersCost(sumDotCostPlayers(dotClasses.black_dot, 'team-2')) + ' €</span>');

    $('#cost-line-up-players-1').html('Sum line-up: <span class="text-red">' + formatPlayersCost(sumDotCostPlayers(dotClasses.red_dot, 'team-1')) + ' €</span>');
    $('#cost-line-up-players-2').html('Sum line-up: <span class="text-red">' + formatPlayersCost(sumDotCostPlayers(dotClasses.red_dot, 'team-2')) + ' €</span>');

    $('#cost-possible-players-1').html('Sum possible: <span class="text-light-blue">' + formatPlayersCost(sumDotCostPlayers(dotClasses.light_blue_dot, 'team-1')) + ' €</span>');
    $('#cost-possible-players-2').html('Sum possible: <span class="text-light-blue">' + formatPlayersCost(sumDotCostPlayers(dotClasses.light_blue_dot, 'team-2')) + ' €</span>');

    $('#cost-possible-2-players-1').html('Sum possible 2: <span class="text-blue">' + formatPlayersCost(sumDotCostPlayers(dotClasses.blue_dot, 'team-1')) + ' €</span>');
    $('#cost-possible-2-players-2').html('Sum possible 2: <span class="text-blue">' + formatPlayersCost(sumDotCostPlayers(dotClasses.blue_dot, 'team-2')) + ' €</span>');
}

function sumDotCostPlayers(dot, team) {
    var players_cost = 0;
    $('#'+team+'-table tr[data-id]').each(function () {
        if ($(this).find('.'+dot).length){
            players_cost += parsePlayerCost($(this).find('td:last').text());
        }
    });

    return players_cost;
}

function parsePlayerCost(str) {
    var cost = 0;
    str = str.trim();
    if (str.slice(-1) === 'm') {
        cost += parseInt(str.replace(/[^0-9]/g,'')+'0000');
    } else if (str.slice(-1) === 'k'){
        cost += parseInt(str.replace(/[^0-9]/g,'')+'000');
    }
    return cost;
}

function formatPlayersCost(cost) {
    if (cost === 0) return 0;
    if (cost.toString().length <= '6'){
        return cost.toString().slice(0, -3) + 'k';
    } else {
        return cost.toString().slice(0, -6) + ',' + cost.toString().slice(0, -4).slice(-2) + 'm';
    }
}

function saveMatch(e) {
    e.preventDefault();
    var matchUrl = $('[data-match_url]').data('match_url');
    var matchId = $('[data-match_id]').data('match_id');

    $.get('/site/save-match', {matchUrl: matchUrl, matchId: matchId})
        .done(function (data) {
            $('#saved-matches-container').html(data);
        });
}

function delMatch(e) {
    e.preventDefault();
    var id = $(this).data('id');

    $.get('/site/del-match', {id: id})
        .done(function (data) {
            $('#saved-matches-container').html(data);
        });
}

function toggleBlueDotColumn(table_id) {
    var columnHeader = $('#' + table_id + ' .show-column');
    if (columnHeader.hasClass('fa-plus')) {
        columnHeader.removeClass('fa-plus').addClass('fa-minus');
    } else {
        columnHeader.removeClass('fa-minus').addClass('fa-plus');
    }
    $('#' + table_id + ' td.hidden-column').toggleClass('kv-grid-hide');
    $('#' + table_id + ' th.hidden-column').toggleClass('kv-grid-hide');
}

function sendComment(e) {
    e.preventDefault();
    var jThis = $(this);
    jThis.prop('disabled', true);
    var team = jThis.data('team');
    var text = jThis.parent().siblings('input').val();
    if (text !== '') {
        $.post('/site/add-comment', {team: team, text: text, matchId: $('[data-match_id]').data('match_id')},)
            .done(function (data) {
                data = JSON.parse(data);
                if (data.success === 1) {
                    jThis.parents('div.direct-chat').find('div.comments-container').append(data.commentHtml);
                    jThis.parent().siblings('input').val('');
                } else {
                    jThis.parent().siblings('input').addClass('error');
                }
            })
            .fail(function () {
                jThis.parent().siblings('input').addClass('error');
            })
            .always(function () {
                jThis.prop('disabled', false);
            });
    }
}

function getOddsLink(){
    $('#oddsportal-link').parent().addClass('disabled');
    $('#oddsportal-link i').removeClass('hidden');

    var team_1_name = $('.team-1-name').data('name');
    var team_2_name = $('.team-2-name').data('name');
    var team_1_soccerway_id = $('.team-1-name').data('team_id');
    var team_2_soccerway_id = $('.team-2-name').data('team_id');

    $.get('/site/get-odds-link', {
        team_1_name: team_1_name,
        team_2_name: team_2_name,
        team_1_soccerway_id: team_1_soccerway_id,
        team_2_soccerway_id: team_2_soccerway_id,
        date: $('#match-info').data('match_date')
    })
        .done(function (data) {
            if (data !== ''){
                $('#oddsportal-link').prop('href', 'http://www.oddsportal.com' + data).parent().removeClass('disabled');
            }
        })
        .always(function () {
            $('#oddsportal-link i').addClass('hidden');
        });
}

function getMarkPlayersList(table_id)
{
    $('#' + table_id + '-mark-players').parent().show();
    $('#' + table_id + '-mark-players .box-body div p').html('');
    for (var i = 1; i <= 4; i++) {
        var str = '';
        $('#' + table_id + '-table li.dropdown>.player_status' + i).each(function () {
            var name = $(this).text();
            var position = $(this).parents('tr').find('td[data-col-seq="15"]').text();
            var appearances = $(this).parents('tr').find('td[data-col-seq="17"]').text();
            var goal = $(this).parents('tr').find('td[data-col-seq="20"]').text();
            str += name+'('+position + ' '+appearances+'/'+goal+'),  ';
        });
        $('#' + table_id + '-mark-players .player_status_' + i + ' p').text(str);
    }
}

function getBetexplorerLink(){
    $('#betexplorer-link').parent().addClass('disabled');
    $('#betexplorer-link i').removeClass('hidden');

    var team_1_name = $('.team-1-name').data('name');
    var team_2_name = $('.team-2-name').data('name');
    var team_1_soccerway_id = $('.team-1-name').data('team_id');
    var team_2_soccerway_id = $('.team-2-name').data('team_id');

    $.get('/site/get-betexplorer-link', {
        team_1_name: team_1_name,
        team_2_name: team_2_name,
        team_1_soccerway_id: team_1_soccerway_id,
        team_2_soccerway_id: team_2_soccerway_id,
        date: $('#match-info').data('match_date')
    })
        .done(function (data) {
            if (data !== ''){
                $('#betexplorer-link').prop('href', 'http://www.betexplorer.com'+  data).parent().removeClass('disabled');
            }
        })
        .always(function () {
            $('#betexplorer-link i').addClass('hidden');
        });
}

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

function editComment() {
    var commentBox = $(this).parents('.direct-chat-msg');
    var text = commentBox.find('.direct-chat-text').text().trim();
    $('#edit-comment-textarea').val(text).data('comment_id', commentBox.data('id'));
    $('#modal-edit-comment').modal();
}

function sendEditComment(){
    $(this).prop('disabled', true);
    var text = $('#edit-comment-textarea').val();
    var commentId = $('#edit-comment-textarea').data('comment_id');
    $.post('/site/edit-comment', {commentId: commentId, text: text})
        .done(function (data) {
            data = JSON.parse(data);
            if (data.success === 1){
                $('.direct-chat-msg[data-id="' + commentId + '"] .direct-chat-text').text(text);
                $('#modal-edit-comment').modal('toggle');
            }
        })
        .always(function () {
            $('#save-edit-comment').prop('disabled', false);
        });
}

function delComment(){
    var commentBox = $(this).parents('.direct-chat-msg');
    var commentId = commentBox.data('id');
    $.get('/site/del-comment', {commentId: commentId})
        .done(function (data) {
            data = JSON.parse(data);
            if (data.success === 1){
                $('.direct-chat-msg[data-id="' + commentId + '"]').remove();
            }
        });
}

function maxGoals(table_id) {
    var maxIds = {0: [], 1: [], 2: []};
    var maxIdsArr = [];
    for (var i = 0; i < 3; i++) {
        var max = 0;
        $('#' + table_id + ' td.goal-td').each(function () {
            var id = $(this).parent().data('id');
            var val = parseInt($(this).text());
            if (maxIdsArr.indexOf(id) == -1) {
                if (val > max) {
                    maxIds[i] = [id];
                    max = val;
                } else if (val == max) {
                    maxIds[i].push(id);
                }
            }
        });
        maxIdsArr = maxIdsArr.concat(maxIds[i]);
    }

    for (var i = 0; i < maxIdsArr.length; i++) {
        var td = $('#'+table_id+' tr[data-id="'+ maxIdsArr[i] +'"] td.goal-td');
        td.html('<b style="font-size: 13px;">'+td.text()+'</b>');
    }
}

function maxAssist(table_id) {
    var maxIds = {0: [], 1: []};
    var maxIdsArr = [];
    for (var i = 0; i < 2; i++) {
        var max = 0;
        $('#' + table_id + ' td.assist-td').each(function () {
            var id = $(this).parent().data('id');
            var val = parseInt($(this).text());
            if (maxIdsArr.indexOf(id) == -1) {
                if (val > max) {
                    maxIds[i] = [id];
                    max = val;
                } else if (val == max) {
                    maxIds[i].push(id);
                }
            }
        });
        maxIdsArr = maxIdsArr.concat(maxIds[i]);
    }

    for (var i = 0; i < maxIdsArr.length; i++) {
        var td = $('#'+table_id+' tr[data-id="'+ maxIdsArr[i] +'"] td.assist-td');
        td.html('<b style="font-size: 13px;">'+td.text()+'</b>');
    }
}

function maxCost(table_id) {
    var maxIds = {0: [], 1: [], 2: [], 3: [], 4: []};
    var maxIdsArr = [];
    for (var i = 0; i < 5; i++) {
        var max = 0;
        $('#' + table_id + ' td.player-cost-column').each(function () {
            var id = $(this).parent().data('id');
            var val = parsePlayerCost($(this).text());
            if (maxIdsArr.indexOf(id) == -1) {
                if (val > max) {
                    maxIds[i] = [id];
                    max = val;
                } else if (val == max) {
                    maxIds[i].push(id);
                }
            }
        });
        maxIdsArr = maxIdsArr.concat(maxIds[i]);
    }

    for (var i = 0; i < maxIdsArr.length; i++) {
        var td = $('#'+table_id+' tr[data-id="'+ maxIdsArr[i] +'"] td.player-cost-column');
        td.html('<b style="font-size: 13px;">'+td.text()+'</b>');
    }
}

function teamForm() {
    $.get('/site/get-team-form', {soccerway_id: $(this).data('team_id')})
        .done(function (data) {
            $('#modal-edit-team .modal-body').html(data);
            $('#modal-edit-team').modal();
        });
}

function saveTeam() {
    $.post('/site/save-team', $("#modal-edit-team form").serialize())
        .done(function () {
            getBetexplorerLink();
            getOddsLink();
            $('#get-transfermarkt').parent().removeClass('disabled');
            $('#get-transfermarkt i').addClass('hidden');

            $('#modal-edit-team').modal('hide');
        })
}

function checkSelect() {
    $.get('/lineups/check-select', {id: $(this).data('id'), state: ($(this).prop('checked')) ? 1 : 0});
}

function matchSubscribe() {
    $.get('/lineups/match-subscribe', {id: $(this).data('id'), state: ($(this).prop('checked')) ? 1 : 0});
}

function matchSubscribeAll() {
    var state = $(this).prop('checked') ? 1 : 0;
    $.get('/lineups/match-subscribe-all', {state: state})
        .done(function (data) {
            data = JSON.parse(data);
            data.forEach(function (id) {
                if (state === 1) {
                    $('#subscribe-checkbox[data-id="' + id + '"]').prop('checked', true);
                } else {
                    $('#subscribe-checkbox[data-id="' + id + '"]').prop('checked', false);
                }
            })
        });
}

function ping() {
    var lineups = 0;
    var replacements_page = 0;
    if ($('#lineups-table').length){
        lineups = true;
    }
    if ($('#replacements-table').length){
        replacements_page = true;
    }
    $.get('/site/ping', {lastLineups: lineups, replacements_page: replacements_page})
        .done(function (data) {
            data = JSON.parse(data);
            if (data.newMatchesHtml) {
                $('#lineups-table tbody').html(data.newMatchesHtml);
            }

            if (data.newReplacementsHtml) {
                $('#replacements-table tbody').html(data.newReplacementsHtml);
            }

            $('#notifications').html(data.notificationsHtml);
            $('#notifications-count').text(data.notificationsCount);

            $('#replacements').html(data.notificationsReplacementsHtml);
            $('#replacements-count').text(data.notificationsReplacementsCount);

            if (data.newMatchesNotifications) {
                var modal = $('#modal-notification .modal-body #lineups-container');
                modal.html('');
                data.newMatchesNotifications.forEach(function(element) {
                    if (element['team_1_changes'] == null) element['team_1_changes'] = '-';
                    if (element['team_2_changes'] == null) element['team_2_changes'] = '-';
                    modal.append('<a target="_blank" href="/?matchUrl='+element['url']+'&lineups='+element['match_id']+'">'+element['team_1_changes']+' '+element['team_1']+' - '+element['team_2_changes']+' '+element['team_2']+'</a><br>')
                });
            }

            if (data.newReplacementsNotifications) {
                var modal = $('#modal-notification .modal-body #replacements-container');
                modal.append(data.newReplacementsNotifications);
            }

            if (data.notificationsAdminArr) {
                var modal = $('#modal-notification .modal-body #admin-notification-container');
                data.notificationsAdminArr.forEach(function(element) {
                    modal.append('<p>Попытка одновременного входа: '+element["username"]+'</p><br>');
                });
            }

            if (data.newMatchesNotifications || data.newReplacementsNotifications || data.notificationsAdminArr) {
                $('#modal-notification').modal();
                $('#notification-audio')[0].play();

                unreadNotification++;
                document.title = '('+unreadNotification+') '+pageTitle;
            }
        });
    $('#lineups-table tbody tr').each(function () {
        if ($(this).data('time') < Date.now() - 7200000){
            $(this).remove();
        }
    });
}

function playerNamesForm() {
    $.get('/site/get-player-names', {player_id: $(this).parent().data('id')})
        .done(function (data) {
            var modal = $('#modal-player-names');
            modal.html(data);
            modal.modal();
        });
}

function savePlayerNames() {
    var player_id = $(this).data('id');
    $('.player-row[data-id='+player_id+']').data('names', $("#edit-player-names-textarea").val());
    $.get('/site/get-player-names', {player_id: $(this).data('id'), player_names: $("#edit-player-names-textarea").val()})
        .done(function () {
            $('#modal-player-names').modal('hide');
        });
}

function yellowCardsForm() {
    $.get('/replacements/yellow-cards', {select_id: $(this).data('id')})
        .done(function (data) {
            $('#modal-yellow-cards').html(data);
            $('#modal-yellow-cards').modal();
        });
}

function saveYellowCards() {
    $.get('/replacements/yellow-cards', {select_id: $(this).data('id'), yellow_cards: $("#edit-yellow-cards-textarea").val()})
        .done(function () {
            $('#modal-yellow-cards').modal('hide');
        });
}

function checkReplaceSelect() {
    $.get('/replacements/check-select', {id: $(this).data('id'), state: ($(this).prop('checked')) ? 1 : 0});
}

function setOrders(team_id, dot) {
    var order = $('#'+team_id+'-order textarea').val()+'\n';
    $('#'+team_id+'-table tr[data-id]').each(function () {
        var names = $(this).data('names');
        var jPlayerDot = $(this).find('div[data-mark='+dot+']');
        names.split(/[ ]*,/).forEach(function (name) {
            if (name !== '') {
                name = name.trim().replace('.', '\\.');
                var regex = new RegExp("[ \\n\\r[\\d]*[ .]*]*"+name+"[\\s\\W]*[\\W\\d]*[&-;,\\n\\r]", 'i');
                if (order.match(regex)) {
                    order = order.replace(regex, '');
                    jPlayerDot.addClass(dotClasses[dot]);
                    jPlayerDot.data('value', 1);
                }
            }
        });
    });
    $('#'+team_id+'-order textarea').val(order);
    sendPlayersMarks();
}

