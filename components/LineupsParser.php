<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 15.06.2018
 * Time: 7:51
 */

namespace app\components;


use app\models\MatchesLineups;
use app\models\Select;
use app\models\Team;
use app\models\User;

class LineupsParser
{
    public static function getMatches()
    {
        $html = SimpleHtmlDom::file_get_html(Parser::SOCCERWAY_URL);
        $urlNextDay = $html->find('div[class=top-right] a', 1)->href;
        $table = $html->find('div[class=table-container] tbody tr[stage-value]');
        $date = (new \DateTime())->setTimezone(new \DateTimeZone('Europe/Moscow'))->format('Y-m-d');
        self::parseTable($table, $date, 1);

        $html = SimpleHtmlDom::file_get_html(Parser::SOCCERWAY_URL . $urlNextDay);
        $table = $html->find('div[class=table-container] tbody tr[stage-value]');
        $date = (new \DateTime())->add(new \DateInterval('P1D'))->setTimezone(new \DateTimeZone('Europe/Moscow'))->format('Y-m-d');
        self::parseTable($table, $date, 2);
    }

    private static function parseTable($table, $date, $type)
    {
        foreach ($table as $tr) {
            $stageValue = $tr->getAttribute('stage-value');
            $competition_id = explode('-', $tr->getAttribute('id'))[1];
            $selectUrl = $tr->find('a', 0)->href;
            if (count(explode('/', $selectUrl, -4)) == 2) {
                $select = Select::findOne(['url' => implode('/', explode('/', $selectUrl, -2))]);
            } else {
                $select = Select::findOne(['url' => implode('/', explode('/', $selectUrl, -4))]);
            }
            if ($select && $select->lineups_parse == 1) {
                if ($type == 1) {
                    $json = file_get_contents(Parser::SOCCERWAY_URL . 'a/block_home_matches?block_id=block_home_matches_30&callback_params={"block_service_id":"home_index_block_homematches","date":"' . $date . '","display":"all","stage-value":"' . $stageValue . '"}&action=showMatches&params={"competition_id":' . $competition_id . '}');
                } else {
                    $json = file_get_contents(Parser::SOCCERWAY_URL . '/a/block_date_matches?block_id=page_matches_1_block_date_matches_1&callback_params={"block_service_id":"matches_index_block_datematches","date":"' . $date . '","stage-value":"' . $stageValue . '"}&action=showMatches&params={"competition_id":' . $competition_id . '}');
                }
                $json = json_decode($json);
                $json_content = SimpleHtmlDom::str_get_html($json->{'commands'}[0]->{'parameters'}->{'content'});
                if ($json_content) {
                    foreach ($json_content->find('tr') as $tr) {
                        if (($tr->getAttribute('data-timestamp') > time() - 7200) && $tr->getAttribute('data-timestamp') < time() + 14400) {
                            $arr = [];
                            $arr['url'] = $tr->find('td[class=score-time] a', 0)->href;
                            $explodeUrl = explode('/', $arr['url']);
                            if (count($explodeUrl) < 2) continue;
                            $arr['soccerway_id'] = $explodeUrl[count($explodeUrl) - 2];

                            if (!MatchesLineups::findOne(['soccerway_id' => $arr['soccerway_id']])) {
                                $arr['team_1'] = $tr->find('td[class=team-a] a', 0)->title;
                                $explTeam1 = explode('/', $tr->find('td[class=team-a] a', 0)->href);
                                $arr['team_1_id'] = $explTeam1[count($explTeam1) - 2];
                                $arr['team_2'] = $tr->find('td[class=team-b] a', 0)->title;
                                $explTeam1 = explode('/', $tr->find('td[class=team-b] a', 0)->href);
                                $arr['team_2_id'] = $explTeam1[count($explTeam1) - 2];
                                $arr['date'] = $tr->getAttribute('data-timestamp');

                                $team1 = Team::findOne(['soccerway_id' => $arr['team_1_id']]);
                                $team2 = Team::findOne(['soccerway_id' => $arr['team_2_id']]);
                                if (!$team1 || !$team1->besoccer_name){
                                    $arr['besoccer_name_mark_team_1'] = (self::checkBesoccerTeam($arr['team_1'])) ? 0 : 1;
                                }
                                if (!$team2 || !$team2->besoccer_name){
                                    $arr['besoccer_name_mark_team_2'] = (self::checkBesoccerTeam($arr['team_2'])) ? 0 : 1;
                                }
                                if (!$team1 || !$team1->sofascore_name){
                                    $arr['sofascore_name_mark_team_1'] = (self::checkSofascoreTeam($arr['team_1'])) ? 0 : 1;
                                }
                                if (!$team2 || !$team2->sofascore_name){
                                    $arr['sofascore_name_mark_team_2'] = (self::checkSofascoreTeam($arr['team_2'])) ? 0 : 1;
                                }

                                $matchesLineups = new MatchesLineups();
                                $matchesLineups->setAttributes($arr);
                                $matchesLineups->select_id = $select->id;
                                $matchesLineups->save();

                                $subscribeUsersIds = User::find()
                                    ->select('id')
                                    ->where(['lineups_subscribe' => 1])
                                    ->column();

                                foreach ($subscribeUsersIds as $id) {
                                    \Yii::$app->db->createCommand()->insert('user2matches_lineups', [
                                        'match_id' => $matchesLineups->id,
                                        'user_id' => $id,
                                    ])->execute();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static function checkLineups()
    {
        $matches = MatchesLineups::find()->where([
            'and', ['source' => null], ['and', ['>', 'date', time()], ['<', 'date', time() + 5400]]
        ])->all();

        foreach ($matches as $match) {
            $lineupsMatchParser = new LineupsMatchParser($match);
            $lineupsMatchParser->check();
        }
    }

    public static function delOldMatches()
    {
        $ids = MatchesLineups::find()
            ->select('id')
            ->where(['<', 'date', time() - 7200])
            ->column();
        MatchesLineups::deleteAll(['in', 'id', $ids]);
        \Yii::$app->db->createCommand()
            ->delete('user2matches_lineups', ['in', 'match_id', $ids])
            ->execute();
    }

    private static function checkSofascoreTeam($name)
    {
        $json = file_get_contents('https://www.sofascore.com/search/suggest?q='.urlencode($name));
        $json = json_decode($json, true);
        if (isset($json['teams'])) {
            foreach ($json['teams'] as $team) {
                if ($team == $name) return true;
            }
        }
        return false;
    }

    private static function checkBesoccerTeam($name)
    {
        try {
            $html = SimpleHtmlDom::file_get_html('https://www.besoccer.com/ajax/doKeywords.php?cadena=' . urlencode($name));
        } catch (\Exception $e) {
            return false;
        }
        foreach ($html->find('li') as $li) {
            if ($li->find('span[class=grup]', 0) && trim($li->find('span[class=grup]', 0)->plaintext) == $name) return true;
        }
        return false;
    }
}