<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 22.05.2018
 * Time: 17:58
 */

namespace app\components;


class MarksAccess
{
    public static function getSavedMatches()
    {
        $favoriteMatches = \app\models\FavoriteMatches::findAll(['user_id' => \Yii::$app->user->id]);

        $favoriteMatchesArr = [];
        foreach ($favoriteMatches as $favoriteMatch) {
            $country = explode('/', $favoriteMatch->url)[5];
            $cup = explode('/', $favoriteMatch->url)[6];
            if ($country != 'world') {
                $cup = ucfirst(str_replace('-', ' ', $country)) . '  ' . ucfirst(str_replace('-', ' ', $cup));
            } else {
                $cup = ucfirst(str_replace('-', ' ', $cup));
            }
            $matchLink = [];
            $matchLink['name'] = $favoriteMatch->match->team_1 . ' - ' . $favoriteMatch->match->team_2;
            $matchLink['url'] = $favoriteMatch->url;
            $matchLink['id'] = $favoriteMatch->id;
            $favoriteMatchesArr[$cup][] = $matchLink;
        }

        return $favoriteMatchesArr;
    }
}