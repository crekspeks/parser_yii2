<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 28.09.2018
 * Time: 2:21
 */

namespace app\components;


use app\models\ReplacementMatches;
use app\models\TeamsReplacements;

class ReplacementsMatchParser
{
    const INJURY = 'injury';
    const EARLY_REPLACEMENT = 'early_replacement';
    const YELLOW_CARD = 'yellow_card';
    const YELLOW_CARD_2 = 'yellow_card_2';
    const RED_CARD = 'red_card';
    const MANY_YELLOW_CARD = 'many_yellow_cards';

    private $match;
    private $incidentsArrSofaScore = [];
    private $incidentsArrBesoccer = [];
    private $incidentsArrSoccerWay = [];
    private $incidentsArr = [];
    private $noticeTeam1 = false;
    private $noticeTeam2 = false;
    private $team1YellowCardSet = [];
    private $team2YellowCardSet = [];
    private $yellowCardSet = false;
    private $playersMarks = ['team1' => [], 'team2' => []];

    private $incidentsArrTemplate = [
        self::INJURY => 0,
        self::EARLY_REPLACEMENT => 0,
        self::YELLOW_CARD_2 => 0,
        self::RED_CARD => 0,
        self::MANY_YELLOW_CARD => 0,
    ];

    public function __construct(ReplacementMatches $match)
    {
        $this->match = $match;

        $this->incidentsArr['team1']  = $this->match->getNextMatchTeam1IncidentsArr() ? $this->match->getNextMatchTeam1IncidentsArr() : $this->incidentsArrTemplate;
        $this->incidentsArr['team2']  = $this->match->getNextMatchTeam2IncidentsArr() ? $this->match->getNextMatchTeam2IncidentsArr() : $this->incidentsArrTemplate;

        $this->team1YellowCardSet = $this->match->getTeam1YellowCards();
        $this->team2YellowCardSet = $this->match->getTeam2YellowCards();

        if (!is_null($this->match->select->yellow_cards_set)) {
            foreach (preg_split('/\r\n|[\r\n]/', $this->match->select->yellow_cards_set) as $limit) {
                $this->yellowCardSet[] = trim($limit);
            }
        }
    }

    public function check()
    {
        $this->getSofaScoreIncidents();
        $this->getBesoccerIncidents();
        $this->getSoccerwayIncidents();

        foreach ($this->incidentsArr['team1'] as $incident => $count) {
            if ($count < $this->incidentsArrSofaScore['team1'][$incident]) {
                $this->incidentsArr['team1'][$incident] = $this->incidentsArrSofaScore['team1'][$incident];
                $this->noticeTeam1 = true;
            }

            if ($count < $this->incidentsArrBesoccer['team1'][$incident]) {
                $this->incidentsArr['team1'][$incident] = $this->incidentsArrBesoccer['team1'][$incident];
                $this->noticeTeam1 = true;
            }

            if ($count < $this->incidentsArrSoccerWay['team1'][$incident]) {
                $this->incidentsArr['team1'][$incident] = $this->incidentsArrSoccerWay['team1'][$incident];
                $this->noticeTeam1 = true;
            }
        }

        foreach ($this->incidentsArr['team2'] as $incident => $count) {
            if ($count < $this->incidentsArrSofaScore['team2'][$incident]) {
                $this->incidentsArr['team2'][$incident] = $this->incidentsArrSofaScore['team2'][$incident];
                $this->noticeTeam2 = true;
            }

            if ($count < $this->incidentsArrBesoccer['team2'][$incident]) {
                $this->incidentsArr['team2'][$incident] = $this->incidentsArrBesoccer['team2'][$incident];
                $this->noticeTeam2 = true;
            }

            if ($count < $this->incidentsArrSoccerWay['team2'][$incident]) {
                $this->incidentsArr['team2'][$incident] = $this->incidentsArrSoccerWay['team2'][$incident];
                $this->noticeTeam2 = true;
            }
        }

        if ($this->noticeTeam1) {
            $this->match->saveNextMatchTeam1Incidents([
                'match_marks' => $this->incidentsArr['team1'],
                'players_marks' => $this->playersMarks['team1']
            ]);
            $this->match->getNextMatchTeam1()->updateUsersNotification();
        }

        if ($this->noticeTeam2) {
            $this->match->saveNextMatchTeam2Incidents([
                'match_marks' => $this->incidentsArr['team2'],
                'players_marks' => $this->playersMarks['team2']
            ]);
            $this->match->getNextMatchTeam2()->updateUsersNotification();
        }
    }

    private function getSofaScoreIncidents()
    {
        $json = file_get_contents('https://www.sofascore.com/football/livescore/json');
        $json = json_decode($json, true);

        $this->incidentsArrSofaScore = ['team1' => $this->incidentsArrTemplate, 'team2' => $this->incidentsArrTemplate];

        foreach ($json['sportItem']['tournaments'] as $tournament){
            foreach ($tournament['events'] as $event){
                $team_1_name = $this->match->getTeam1()->sofascore_neme ?? $this->match->team_1;
                $team_2_name = $this->match->getTeam2()->sofascore_name ?? $this->match->team_2;
                if ($event['homeTeam']['name'] == $team_1_name && $event['awayTeam']['name'] == $team_2_name){
                    $json = json_decode(file_get_contents('https://www.sofascore.com/event/'.$event['id'].'/json'), true);
                    if (isset($json['incidents']) && !empty($json['incidents'])){
                        foreach ($json['incidents'] as $incident){

                            if ($incident['incidentType'] == 'substitution' && isset($incident['injury']) && $incident['injury']){
                                if ($incident['isHome']){
                                    $this->incidentsArrSofaScore['team1'][self::INJURY]++;
                                    $this->playersMarks['team1'][$incident['playerOut']['shortName']] = self::INJURY;
                                } else {
                                    $this->incidentsArrSofaScore['team2'][self::INJURY]++;
                                    $this->playersMarks['team2'][$incident['playerOut']['shortName']] = self::INJURY;
                                }
                            } elseif ($incident['incidentType'] == 'substitution' && $incident['time'] <= 45){
                                if ($incident['isHome']){
                                    $this->incidentsArrSofaScore['team1'][self::EARLY_REPLACEMENT]++;
                                    $this->playersMarks['team1'][$incident['playerOut']['shortName']] = self::EARLY_REPLACEMENT;
                                } else {
                                    $this->incidentsArrSofaScore['team2'][self::EARLY_REPLACEMENT]++;
                                    $this->playersMarks['team2'][$incident['playerOut']['shortName']] = self::EARLY_REPLACEMENT;
                                }
                            } elseif ($incident['incidentType'] == 'card' && $incident['type'] == 'Yellow'){
                                if ($incident['isHome']){
                                    $this->checkYellowCards($incident['player']['shortName'], 'SofaScore', 'team1', 1);
                                } else {
                                    $this->checkYellowCards($incident['player']['shortName'], 'SofaScore', 'team2', 1);
                                }
                            } elseif ($incident['incidentType'] == 'card' && $incident['type'] == 'YellowRed'){
                                if ($incident['isHome']){
                                    $this->incidentsArrSofaScore['team1'][self::YELLOW_CARD_2]++;
                                    $this->playersMarks['team1'][$incident['player']['shortName']] = self::YELLOW_CARD_2;
                                    $this->checkYellowCards($incident['player']['shortName'], 'SofaScore', 'team1', 2);
                                } else {
                                    $this->incidentsArrSofaScore['team2'][self::YELLOW_CARD_2]++;
                                    $this->playersMarks['team2'][$incident['player']['shortName']] = self::YELLOW_CARD_2;
                                    $this->checkYellowCards($incident['player']['shortName'], 'SofaScore', 'team2', 2);
                                }
                            } elseif ($incident['incidentType'] == 'card' && $incident['type'] == 'Red'){
                                if ($incident['isHome']){
                                    $this->incidentsArrSofaScore['team1'][self::RED_CARD]++;
                                    $this->playersMarks['team1'][$incident['player']['shortName']] = self::RED_CARD;
                                } else {
                                    $this->incidentsArrSofaScore['team2'][self::RED_CARD]++;
                                    $this->playersMarks['team2'][$incident['player']['shortName']] = self::RED_CARD;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private function getBesoccerIncidents()
    {
        $matches = SimpleHtmlDom::file_get_html('https://www.besoccer.com/livescore_req/'.time().'/')->find('td[class=score bold  playing]');
        $this->incidentsArrBesoccer = ['team1' => $this->incidentsArrTemplate, 'team2' => $this->incidentsArrTemplate];

        foreach($matches as $match) {
            $match = $match->parent();
            $team1 = trim($match->find('td[class=team-home]', 0)->plaintext);
            $team2 = trim($match->find('td[class=team-away]', 0)->plaintext);

            $team_1_name = $this->match->getTeam1()->besoccer_name ?? $this->match->team_1;
            $team_2_name = $this->match->getTeam2()->besoccer_name ?? $this->match->team_2;

            if ($team1 == $team_1_name && $team2 == $team_2_name){
                $url = $match->find('td[class=score] a', 0)->href;
                $html = SimpleHtmlDom::file_get_html('https://www.besoccer.com/'.$url)->find('div[id=listado_eventos]', 0);
                if(!$html) return false;

                foreach ($html->find('div[class=event-content]') as $event) {
                    $playerName = trim($event->find('h5 a', 0)->plaintext);
                    if ($event->find('span[class=left event_8]', 0)) {
                        $this->checkYellowCards($playerName, 'Besoccer', 'team1', 1);
                    } elseif ($event->find('span[class=right event_8]', 0)) {
                        $this->checkYellowCards($playerName, 'Besoccer', 'team2', 1);
                    } elseif ($event->find('span[class=left event_7]', 0)) {
                        $time = $event->find('span[class=left minutos minutosizq]', 0)->plaintext;
                        if (preg_replace("/[^0-9]/", '', $time) <= 45) {
                            $this->incidentsArrBesoccer['team1'][self::EARLY_REPLACEMENT]++;
                            $this->playersMarks['team1'][$playerName] = self::EARLY_REPLACEMENT;
                        }
                    } elseif ($event->find('span[class=right event_7]', 0)) {
                        $time = $event->find('span[class=left minutos minutosizq]', 0)->plaintext;
                        if (preg_replace("/[^0-9]/", '', $time) <= 45) {
                            $this->incidentsArrBesoccer['team2'][self::EARLY_REPLACEMENT]++;
                            $this->playersMarks['team2'][$playerName] = self::EARLY_REPLACEMENT;
                        }
                    } elseif ($event->find('span[class=left event_10]', 0)) {
                        $this->incidentsArrBesoccer['team1'][self::YELLOW_CARD_2]++;
                        $this->playersMarks['team1'][$playerName] = self::YELLOW_CARD_2;
                        $this->checkYellowCards($playerName, 'Besoccer', 'team1', 2);
                    } elseif ($event->find('span[class=right event_10]', 0)) {
                        $this->incidentsArrBesoccer['team2'][self::YELLOW_CARD_2]++;
                        $this->playersMarks['team2'][$playerName] = self::YELLOW_CARD_2;
                        $this->checkYellowCards($playerName, 'Besoccer', 'team2', 2);
                    } elseif ($event->find('span[class=left event_9]', 0)) {
                        $this->incidentsArrBesoccer['team1'][self::RED_CARD]++;
                        $this->playersMarks['team1'][$playerName] = self::RED_CARD;
                    } elseif ($event->find('span[class=right event_9]', 0)) {
                        $this->incidentsArrBesoccer['team2'][self::RED_CARD]++;
                        $this->playersMarks['team2'][$playerName] = self::RED_CARD;
                    } elseif ($event->find('span[class=left event_4]', 0)) {
                        $this->incidentsArrBesoccer['team1'][self::INJURY]++;
                        $this->playersMarks['team1'][$playerName] = self::INJURY;
                        $time = $event->find('span[class=left minutos minutosizq]', 0)->plaintext;
                        if (preg_replace("/[^0-9]/", '', $time) <= 45) {
                            $this->incidentsArrBesoccer['team1'][self::EARLY_REPLACEMENT]--;
                        }
                    } elseif ($event->find('span[class=right event_4]', 0)) {
                        $this->incidentsArrBesoccer['team2'][self::INJURY]++;
                        $this->playersMarks['team2'][$playerName] = self::INJURY;
                        $time = $event->find('span[class=left minutos minutosizq]', 0)->plaintext;
                        if (preg_replace("/[^0-9]/", '', $time) <= 45) {
                            $this->incidentsArrBesoccer['team2'][self::EARLY_REPLACEMENT]--;
                        }
                    }
                }
            }
        }
    }

    private function getSoccerwayIncidents()
    {
        $html = SimpleHtmlDom::file_get_html(Parser::SOCCERWAY_URL . $this->match->url);

        $this->incidentsArrSoccerWay = ['team1' => $this->incidentsArrTemplate, 'team2' => $this->incidentsArrTemplate];

        $lineupsContainer = $html->find('div[class=combined-lineups-container]', 0);
        if ($lineupsContainer) {
            $team_1_table = $lineupsContainer->find('div[class=left] tbody', 0);
            $this->getSoccerwayPlayerIncidents($team_1_table, 'team1');

            $team_2_table = $lineupsContainer->find('div[class=right] tbody', 0);
            $this->getSoccerwayPlayerIncidents($team_2_table, 'team2');
        }
    }

    private function getSoccerwayPlayerIncidents($table, $team)
    {
        foreach ($table->find('tr') as $tr) {
            $mark = $tr->find('[class=bookings]', 0);
            if (!$tr->find('strong', 0) && $mark && $mark->find('img', 0)) {
                foreach ($mark->find('img') as $img) {
                    $src = $img->getAttribute('src');
                    $explodeSrc = explode('/', $src);
                    $markImg = $explodeSrc[count($explodeSrc) - 1];
                    $playerName = trim($tr->find('td a', 0)->plaintext);

                    if ($markImg == 'YC.png') { //желтая
                        $this->checkYellowCards($playerName, 'SoccerWay', $team, 1);
                    } elseif ($markImg == 'Y2C.png') { //двойная желтая
                        $this->incidentsArrSoccerWay[$team][self::YELLOW_CARD_2]++;
                        $this->playersMarks[$team][$playerName] = self::YELLOW_CARD_2;
                        $this->checkYellowCards($playerName, 'SoccerWay', $team, 2);
                    } elseif ($markImg == 'RC.png') { //красная
                        $this->incidentsArrSoccerWay[$team][self::RED_CARD]++;
                        $this->playersMarks[$team][$playerName] = self::RED_CARD;
                    }
                }
            }
        }
    }

    private function checkYellowCards($playerName, $site, $team, $count)
    {
        $arrName = 'incidentsArr' . $site;
        $teamCardSet = $team.'YellowCardSet';
        $teamCardSetArr = $this->$teamCardSet;
        if ($this->yellowCardSet) {
            foreach ($teamCardSetArr as $id => $player) {
                if (isset($player['names'][0]) && !empty($player['names'][0])) {
                    if (in_array($playerName, $player['names'])) {
                        if (in_array($teamCardSetArr[$id]['yellow_cards'] + $count, $this->yellowCardSet)) {
                            $this->{$arrName}[$team][self::MANY_YELLOW_CARD]++;
                            $this->playersMarks[$team][$playerName] = self::MANY_YELLOW_CARD;
                            return;
                        };
                    }
                }
                if (LineupsMatchParser::normolizeName($player['name']) == $playerName) {
                    if (in_array($teamCardSetArr[$id]['yellow_cards'] + $count, $this->yellowCardSet)) {
                        $this->{$arrName}[$team][self::MANY_YELLOW_CARD]++;
                        $this->playersMarks[$team][$playerName] = self::MANY_YELLOW_CARD;
                        return;
                    };
                }
            }
        }
    }
}