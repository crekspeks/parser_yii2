<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 25.09.2018
 * Time: 17:13
 */

namespace app\components;


use app\models\Player;
use app\models\ReplacementMatches;
use app\models\Select;

class ReplacementParser
{
    public static function getMatches(Select $select)
    {
        $html = NetworkQuery::getContent(Parser::SOCCERWAY_URL . $select->url);
        $html = SimpleHtmlDom::str_get_html($html);
        $seasonId = $html->find('select[id=season_id_selector] option[selected=selected]', 0)->getAttribute('value');
        $explodeSeasonId = explode('/', $seasonId);
        $seasonId = substr($explodeSeasonId[count($explodeSeasonId) - 2], 1);

        $level_2 = $html->find('ul[class=level-2 expanded]', 0);
        if (!$level_2) {
            $html = $html->find('div[id=submenu] li', 1);
            $url = $html->find('a', 0)->href;
            $html = SimpleHtmlDom::file_get_html(Parser::SOCCERWAY_URL . $url);

            foreach ($html->find('tr[data-timestamp]') as $tr) {
                $matchInfo = self::getMatchInfo($tr, $seasonId);
                if ($matchInfo) {
                    $replacementMatches = ReplacementMatches::findOne(['soccerway_id' => $matchInfo['soccerway_id']]);
                    if ($replacementMatches) {
                        self::saveYellowCards($replacementMatches, $seasonId);
                    } else {
                        $replacementMatches = new ReplacementMatches();
                        $matchInfo['select_id'] = $select->id;
                        $replacementMatches->setAttributes($matchInfo);
                        $replacementMatches->save();
                        self::saveYellowCards($replacementMatches, $seasonId);
                    }
                }
            }
        } else {
            $level_2 = $level_2->find('li[class=leaf] a');
            foreach ($level_2 as $a) {
                $html = SimpleHtmlDom::file_get_html(Parser::SOCCERWAY_URL . $a->href . 'matches/');
                foreach ($html->find('tr[data-timestamp]') as $tr) {
                    $matchInfo = self::getMatchInfo($tr, $seasonId);
                    if ($matchInfo) {
                        $replacementMatches = ReplacementMatches::findOne(['soccerway_id' => $matchInfo['soccerway_id']]);
                        if ($replacementMatches) {
                            self::saveYellowCards($replacementMatches, $seasonId);
                        } else {
                            $replacementMatches = new ReplacementMatches();
                            $matchInfo['select_id'] = $select->id;
                            $replacementMatches->setAttributes($matchInfo);
                            $replacementMatches->save();
                            self::saveYellowCards($replacementMatches, $seasonId);
                        }
                    }
                }
            }
        }
    }

    private static function getMatchInfo($tr)
    {
        $arr = [];
        $arr['url'] = $tr->find('td[class=score-time] a', 0)->href;
        $explodeUrl = explode('/', $arr['url']);

        if (!isset($explodeUrl[count($explodeUrl) - 2])) return false;

        $arr['soccerway_id'] = $explodeUrl[count($explodeUrl) - 2];
        if ($tr->getAttribute('data-timestamp') > time()) {
            $arr['team_1'] = trim($tr->find('td[class=team-a]', 0)->plaintext);
            $explTeam1 = explode('/', $tr->find('td[class=team-a] a', 0)->href);
            $arr['team_1_id'] = $explTeam1[count($explTeam1) - 2];
            $arr['team_2'] = trim($tr->find('td[class=team-b]', 0)->plaintext);
            $explTeam2 = explode('/', $tr->find('td[class=team-b] a', 0)->href);
            $arr['team_2_id'] = $explTeam2[count($explTeam2) - 2];
            $arr['start_time'] = $tr->getAttribute('data-timestamp');

            return $arr;
        }
        return false;
    }

    private static function saveYellowCards(ReplacementMatches $replacementMatches, $seasonId)
    {
        if ($replacementMatches->team1YellowCardsNeedUpdate()) {
            $replacementMatches->saveTeam1YellowCards(self::getYellowCards($replacementMatches->team_1_id, $seasonId));
        }
        if ($replacementMatches->team2YellowCardsNeedUpdate()) {
            $replacementMatches->saveTeam2YellowCards(self::getYellowCards($replacementMatches->team_2_id, $seasonId));
        }
    }

    private static function getYellowCards($team_id, $season_id)
    {
        $players = [];

        $json = NetworkQuery::getContent('https://us.soccerway.com/a/block_team_squad?block_id=page_team_1_block_team_squad_8&callback_params={"team_id":"'.$team_id.'"}&action=changeSquadSeason&params={"season_id":"'.$season_id.'"}');
        $json = json_decode($json, true);
        $table = SimpleHtmlDom::str_get_html($json['commands'][0]['parameters']['content']);

        if (!$table) return false;
        foreach ($table->find('tbody tr[class!=sub-head]') as $tr) {
            $playerId = $tr->getAttribute('data-people_id');
            $players[$playerId]['name'] = $tr->find('td[class=name large-link] a')[0]->plaintext;
            $players[$playerId]['yellow_cards'] = $tr->find('td[class=yellow-cards]')[0]->plaintext;
            $players[$playerId]['nd_yellow_cards'] = $tr->find('td[class=2nd-yellow-cards]')[0]->plaintext;

            $players[$playerId]['yellow_cards'] = $players[$playerId]['yellow_cards'] - $players[$playerId]['nd_yellow_cards'];
            $players[$playerId]['names'] = [];

            $player = Player::findOne(['player_id' => $playerId]);
            if ($player) {
                foreach (preg_split('/\r\n|[\r\n]/', $player->names) as $name) {
                    $players[$playerId]['names'][] = trim($name);
                }
            }
        }

        return $players;
    }
}