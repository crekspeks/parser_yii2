<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 09.10.2018
 * Time: 20:21
 */

namespace app\components;


class NetworkQuery
{
    public static function getContent($url)
    {
        try {
            $content = file_get_contents($url);
            return $content;
        } catch (\Exception $e) {
            if ($http_response_header[0] == 'HTTP/1.0 503 Service Unavailable') {
                try {
                    $content = file_get_contents($url);
                    return $content;
                } catch (\Exception $e) {
                    throw $e;
                }
            }
            throw $e;
        }
    }
}