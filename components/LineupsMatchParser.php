<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 20.06.2018
 * Time: 9:08
 */

namespace app\components;


use app\models\MatchesLineups;
use function GuzzleHttp\Psr7\str;

class LineupsMatchParser
{
    private $lineupsMatch;
    private $players;
    private $source;

    public function __construct(MatchesLineups $lineupsMatch)
    {
        $this->lineupsMatch = $lineupsMatch;
    }

    public function check()
    {
        if ($this->getSoccerway() || $this->getBesoccer() || $this->getSofaScore()) {
            $this->getChanges();

            $this->lineupsMatch->team_1_changes = $this->players['team_1']['changes'];
            $this->lineupsMatch->team_2_changes = $this->players['team_2']['changes'];
            $this->lineupsMatch->source = $this->source;

            \Yii::$app->db->createCommand()
                ->update('user2matches_lineups', ['status' => 1], ['match_id' => $this->lineupsMatch->id])
                ->execute();
            /*foreach ($this->lineupsMatch->subscribeUsers as $user) {
                \Yii::$app->mailer->compose()
                    ->setFrom(\Yii::$app->params['mail_from'])
                    ->setTo($user->email)
                    ->setSubject('Стартовый состав матча ' . $this->lineupsMatch->team_1 . ' - ' . $this->lineupsMatch->team_2)
                    ->setHtmlBody(
                        '<a href="'.\Yii::$app->params['site_url'].'/?matchUrl='. $this->lineupsMatch->url.'&lineups='.$this->lineupsMatch->id.'">' .\Yii::$app->params['site_url'].'/?matchUrl='. $this->lineupsMatch->url.'&lineups='.$this->lineupsMatch->id. '</a>'
                    )
                    ->send();
            }*/

            $playersMarks = ['team_1' => $this->players['team_1']['lineups'] + $this->players['team_1']['substitutes'], 'team_2' => $this->players['team_2']['lineups'] + $this->players['team_2']['substitutes']];
            $this->lineupsMatch->players_json = json_encode($playersMarks);

            $this->lineupsMatch->save();
        }
    }

    private function getSoccerway()
    {
        $html = SimpleHtmlDom::file_get_html(Parser::SOCCERWAY_URL . $this->lineupsMatch->url);
        if ($this->getSoccerwayLineups($html) && $this->getSoccerwaySubstitutes($html)) {
            $this->source = 'soccerway';
            return true;
        }
        return false;
    }

    private function getChanges()
    {
        $this->players['team_1']['changes'] = null;
        $this->players['team_2']['changes'] = null;
        $html = SimpleHtmlDom::file_get_html(Parser::SOCCERWAY_URL . $this->lineupsMatch->url)
            ->find('div[class=block_match_info] div', 0);

        $team_1_last_match = $html->find('div[class=left] div[class=form] a', 0)->href;
        $team_1_last_match = SimpleHtmlDom::file_get_html(Parser::SOCCERWAY_URL . $team_1_last_match);
        $containers = $team_1_last_match->find('div[class=combined-lineups-container]');

        if ($containers) {
            if ($team_1_last_match->find('div[class=block_match_info] div[class=left] h3 a', 0)->plaintext == $this->lineupsMatch->team_1) {
                $teamPos = 'left';
            } else {
                $teamPos = 'right';
            }

            $this->players['team_1']['changes'] = $this->checkChanges($containers, 'team_1', $teamPos);
        }

        $team_2_last_match = $html->find('div[class=right] div[class=form] a', 0)->href;
        $team_2_last_match = SimpleHtmlDom::file_get_html(Parser::SOCCERWAY_URL . $team_2_last_match);
        $containers = $team_2_last_match->find('div[class=combined-lineups-container]');
        if ($containers) {
            if ($team_2_last_match->find('div[class=block_match_info] div[class=left] h3 a', 0)->plaintext == $this->lineupsMatch->team_2) {
                $teamPos = 'left';
            } else {
                $teamPos = 'right';
            }

            $this->players['team_2']['changes'] = $this->checkChanges($containers, 'team_2', $teamPos);
        }


    }

    private function checkChanges($containers, $team, $teamPos)
    {
        $changes = 0;

        foreach ($containers[0]->find('div[class='.$teamPos.'] tr') as $tr){ //lineups
            if (!$tr->find('a', 0) || $tr->find('strong', 0)) continue;
            $name = self::normolizeName($tr->find('a', 0)->plaintext);
            $shirtnumber = $tr->find('td[class=shirtnumber]', 0) ? $tr->find('td[class=shirtnumber]', 0)->plaintext : false;
            $i = 0;
            if (array_key_exists($name, $this->players[$team]['lineups'])) {
                $i++;
            }
            if (!$i) {
                $changes++;
                continue;
            }
            foreach ($this->players[$team]['lineups'] as $p) {
                if ($shirtnumber && $p['shirtnumber'] && $p['shirtnumber'] != '-' && $p['shirtnumber'] == $shirtnumber) {
                    $i++;
                }
            }
            if (!$i) $changes++;
        }

        return $changes;
    }

    private function getSoccerwayLineups($html)
    {
        $lineupsContainer = $html->find('div[class=combined-lineups-container]', 0);
        if ($lineupsContainer) {
            $team_1_table = $lineupsContainer->find('div[class=left] tbody', 0);
            $this->players['team_1']['lineups'] = $this->getSoccerwayPlayers($team_1_table, 1);

            $team_2_table = $lineupsContainer->find('div[class=right] tbody', 0);
            $this->players['team_2']['lineups'] = $this->getSoccerwayPlayers($team_2_table, 1);

            return true;
        }
        return false;
    }

    private function getSoccerwaySubstitutes($html)
    {
        $substitutesContainer = $html->find('div[class=combined-lineups-container]', 1);
        if ($substitutesContainer) {

            $team_1_table = $substitutesContainer->find('div[class=left] tbody', 0);
            $this->players['team_1']['substitutes'] = self::getSoccerwayPlayers($team_1_table, 2);

            $team_2_table = $substitutesContainer->find('div[class=right] tbody', 0);
            $this->players['team_2']['substitutes'] = self::getSoccerwayPlayers($team_2_table, 2);

            return true;
        }
        return false;
    }

    private function getSoccerwayPlayers($table, $mark)
    {
        $playersArr = [];
        foreach ($table->find('tr') as $tr) {
            $a = $tr->find('td a', 0);
            if ($a && !$tr->find('strong', 0)) {
                $name = self::normolizeName($a->plaintext);
                $shirtnumber = ($tr->find('td[class=shirtnumber]', 0)) ? $tr->find('td[class=shirtnumber]', 0)->plaintext : '-';
                $playersArr[$name] = ['mark' => $mark, 'shirtnumber' => $shirtnumber];
            }
        }
        return $playersArr;
    }

    private function getBesoccer()
    {
        $matches = SimpleHtmlDom::file_get_html('https://www.besoccer.com/livescore_req/'.time().'/')->find('div[class=otrospartidos]');
        foreach($matches as $match) {
            $team1 = trim($match->find('td[class=team-home]', 0)->plaintext);
            $team2 = trim($match->find('td[class=team-away]', 0)->plaintext);

            $team_1_name = $this->lineupsMatch->getTeam1Name()->besoccer_name ?? $this->lineupsMatch->team_1;
            $team_2_name = $this->lineupsMatch->getTeam2Name()->besoccer_name ?? $this->lineupsMatch->team_2;

            if ($team1 == $team_1_name && $team2 == $team_2_name){
                $url = $match->find('td[class=score] a', 0)->href;
                $html = SimpleHtmlDom::file_get_html('https://www.besoccer.com/'.$url)->find('div[id=tab_match_teams]', 0);

                if(!$html) return false;

                $this->players['team_1']['lineups'] = $this->getBesoccerPlayers($html->find('div[class=team1] ul', 0), 1);
                $this->players['team_1']['substitutes'] = $this->getBesoccerPlayers($html->find('div[class=team1] ul', 1), 2);

                $this->players['team_2']['lineups'] = $this->getBesoccerPlayers($html->find('div[class=team2] ul', 0), 1);
                $this->players['team_2']['substitutes'] = $this->getBesoccerPlayers($html->find('div[class=team2] ul', 1), 2);

                $this->source = 'besoccer';
                return true;
            }
        }
        return false;
    }

    private function getBesoccerPlayers($table, $mark)
    {
        $playersArr = [];
        foreach ($table->find('li') as $li) {
            $name = self::normolizeName($li->find('h5[class=align-player]', 0)->plaintext);
            $shirtnumber = $li->find('small', 0)->plaintext;
            $playersArr[$name] = ['mark' => $mark, 'shirtnumber' => $shirtnumber];
        }
        return $playersArr;
    }

    private function getSofaScore()
    {
        $json = file_get_contents('https://www.sofascore.com/football//'.(new \DateTime())->format('Y-m-d').'/json');
        $json = json_decode($json, true);
        foreach ($json['sportItem']['tournaments'] as $tournament){
            foreach ($tournament['events'] as $event){
                $team_1_name = $this->lineupsMatch->getTeam1Name()->sofascore_neme ?? $this->lineupsMatch->team_1;
                $team_2_name = $this->lineupsMatch->getTeam2Name()->sofascore_name ?? $this->lineupsMatch->team_2;
                if ($event['homeTeam']['name'] == $team_1_name && $event['awayTeam']['name'] == $team_2_name){
                    $json = json_decode(file_get_contents('https://www.sofascore.com/event/'.$event['id'].'/json'), true);
                    if ($json['event']['hasLineups'] == 'true'){
                        $json = json_decode(file_get_contents('https://www.sofascore.com/event/'.$event['id'].'/lineups/json'), true);
                        foreach ($json['homeTeam']['lineupsSorted'] as $player){
                            $name = self::normolizeName($player['player']['shortName']);
                            if ($player['substitute'] === false) {
                                $this->players['team_1']['lineups'][$name] = ['mark' => 1, 'shirtnumber' => $player['shirtNumber']];
                            } else {
                                $this->players['team_1']['substitutes'][$name] = ['mark' => 2, 'shirtnumber' => $player['shirtNumber']];
                            }
                        }
                        foreach ($json['awayTeam']['lineupsSorted'] as $player){
                            $name = self::normolizeName($player['player']['shortName']);
                            if ($player['substitute'] === false) {
                                $this->players['team_2']['lineups'][$name] = ['mark' => 1, 'shirtnumber' => $player['shirtNumber']];
                            } else {
                                $this->players['team_2']['substitutes'][$name] = ['mark' => 2, 'shirtnumber' => $player['shirtNumber']];
                            }
                        }
                        $this->source = 'sofaScore';
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static function normolizeName($str)
    {
        $str = strtr($str,
            [
                'ж'=>'zh','ё'=>'yo','ч'=>'ch','ш'=>'sh','щ'=>'shch','ю'=>'yu','я'=>'ya',
                'Ж'=>'Zh','Ё'=>'Yo','Ч'=>'Ch','Ш'=>'Sh','Щ'=>'Shch','Ю'=>'Yu','Я'=>'Ya','ä'=>'ae','ö'=>'oe','ü'=>'ue','ß'=>'ss','Ä'=>'Ae','Ö'=>'Oe','Ü'=>'Ue',
                'æ'=>'ae','Æ'=>'Ae','Á'=>'A','À'=>'A','Â'=>'A','Ą'=>'A','Å'=>'A','Ç'=>'C','Ć'=>'C','Č'=>'C','É'=>'E','È'=>'E','Ê'=>'E','Ë'=>'E','Ę'=>'E','Ė'=>'E','Ě'=>'E',
                'Ğ'=>'G','Î'=>'I','Ï'=>'I','İ'=>'I','Í'=>'I','I'=>'I','Į'=>'I','Ł'=>'L','Ĺ'=>'L','Ñ'=>'N','Ń'=>'N','Ó'=>'O','Ô'=>'O','Ø'=>'O','Ŕ'=>'R','Ř'=>'R','Ś'=>'S','Š'=>'S','Ş'=>'S',
                'Ú'=>'U','Ù'=>'U','Û'=>'U','Ų'=>'U','Ū'=>'U','Ů'=>'U','Ý'=>'Y','Ź'=>'Z','Ż'=>'Z','Ž'=>'Z','á'=>'a','à'=>'a','â'=>'a','ą'=>'a','å'=>'a','ç'=>'c','ć'=>'c','č'=>'c','é'=>'e',
                'è'=>'e','ê'=>'e','ë'=>'e','ę'=>'e','ė'=>'e','ě'=>'e','ğ'=>'g','î'=>'i','ï'=>'i','i'=>'i','í'=>'i','ı'=>'i','į'=>'i','ł'=>'l','ĺ'=>'l','ñ'=>'n','ń'=>'n','ó'=>'o','ô'=>'o',
                'ø'=>'o','ŕ'=>'r','ř'=>'r','ś'=>'s','š'=>'s','ş'=>'s','ú'=>'u','ù'=>'u','û'=>'u','ų'=>'u','ū'=>'u','ů'=>'u','ý'=>'y','ź'=>'z','ż'=>'z','ž'=>'z','А'=>'A','Б'=>'B','В'=>'V',
                'Г'=>'G','Ґ'=>'G','Д'=>'D','Е'=>'E','Є'=>'E','З'=>'Z',
                'И'=>'I','Й'=>'Y','І'=>'I','Ї'=>'I','К'=>'K','Л'=>'L','М'=>'M','Н'=>'N','О'=>'O','П'=>'P','Р'=>'R','С'=>'S','Т'=>'T','У'=>'U','Ф'=>'F','Х'=>'H','Ц'=>'C','Ы'=>'Y','Э'=>'E',
                'а'=>'a','б'=>'b','в'=>'v','г'=>'g','ґ'=>'g','д'=>'d','е'=>'e','є'=>'e','з'=>'z','и'=>'i','й'=>'y','і'=>'i','ї'=>'i','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p',
                'р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ы'=>'y','э'=>'e','Ъ'=>'','Ь'=>'','ъ'=>'','ь'=>'',
            ]);
        return $str;
    }
}