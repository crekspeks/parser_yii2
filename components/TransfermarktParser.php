<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 23.05.2018
 * Time: 15:30
 */

namespace app\components;


use app\models\Player;

class TransfermarktParser
{
    const ABI_URL_STATUS_UNSUPPORTED = 100;
    const ABI_URL_STATUS_OK = 200;
    const ABI_URL_STATUS_REDIRECT_301 = 301;
    const ABI_URL_STATUS_REDIRECT_302 = 302;
    const ABI_URL_STATUS_NOT_FOUND = 404;
    const MAX_REDIRECTS_NUM = 4;

    private $team_1;
    private $team_2;

    public function __construct(array $matchArr)
    {
        $this->team_1 = $matchArr['team1'];
        $this->team_2 = $matchArr['team2'];
    }

    public function getMatchInfo()
    {
        $this->getTeamInfo('team_1');
        $this->getTeamInfo('team_2');
        $this->searchPlayerInfo('team_1');
        $this->searchPlayerInfo('team_2');

        foreach ($this->team_1['players'] as $key => $player) {
            if ($player['market_value'] != '-') {
                $this->team_1['players'][$key]['market_value'] = str_replace([' Th. €', ' Mill. €'], ['k', 'm'], $player['market_value']);
            }
        }
        foreach ($this->team_2['players'] as $key => $player) {
            if ($player['market_value'] != '-') {
                $this->team_2['players'][$key]['market_value'] = str_replace([' Th. €', ' Mill. €'], ['k', 'm'], $player['market_value']);
            }
        }

        return ['team1' => $this->team_1, 'team2' => $this->team_2];
    }

    private function getTeamInfo($team)
    {
        $html = TransfermarktParser::abi_get_url_object('https://www.transfermarkt.com/schnellsuche/ergebnis/schnellsuche?query='.urlencode(trim($this->$team['name'])).'&x=15&y=10', 'Mozila');
        $html = SimpleHtmlDom::str_get_html($html['content']);
        foreach ($html->find('div[class=box]') as $div){
            $header = $div->find('div[class=table-header]');
            if (!empty($header) && strpos($header[0]->plaintext, 'earch results: Clubs')){
                foreach ($div->find('tr') as $tr) {
                    $a = $tr->find('td[class=hauptlink] a', 0);
                    if ($a && $tr->find('td', 6) && $tr->find('td', 6) != '-' && $a->plaintext == trim($this->$team['name'])) {
                        $teamUrl = $a->href;
                        $this->$team['market_value'] = $tr->find('td', 6)->plaintext;
                        break;
                    }
                }
            }
        }

        if (isset($teamUrl)) {
            $html = TransfermarktParser::abi_get_url_object('https://www.transfermarkt.com' . $teamUrl, 'Mozila');
            $html = SimpleHtmlDom::str_get_html($html['content']);
            $html = $html->find('div[id=yw1]')[0];
            foreach ($html->find('tr') as $tr) {
                if (!$tr->getAttribute('class')) continue;
                $name = $tr->find('td[class=hauptlink] a', 0)->plaintext;
                $captain = !empty($tr->find('td[class=hauptlink] span[title=Captain]', 0));
                foreach ($this->$team['players'] as $key => $player) {
                    if (Player::checkName($name, preg_split('/[ ]*,/', $player['names']))) {
                        $this->$team['players'][$key]['market_value'] = $tr->find('td', 8)->plaintext;
                        $this->$team['players'][$key]['captain'] = $captain;
                    }
                }
            }
        }
    }

    private function searchPlayerInfo($team)
    {
        foreach ($this->$team['players'] as $key => $player){
            if (!isset($player['market_value'])) {
                $soccHtml = SimpleHtmlDom::file_get_html(Parser::SOCCERWAY_URL . $player['url']);
                $playerPassport = $soccHtml->find('div[class=block_player_passport]', 0);
                $playerFirstName = trim($playerPassport->find('dd[data-first_name]', 0)->plaintext);
                $playerLastName = trim($playerPassport->find('dd[data-last_name]', 0)->plaintext ?? '');
                $playerFullName = $playerFirstName . ' ' . $playerLastName;
                $this->$team['players'][$key]['market_value'] = '-';

                $html = self::abi_get_url_object('https://www.transfermarkt.com/schnellsuche/ergebnis/schnellsuche?query=' . urlencode($playerFullName) . '&x=15&y=10', 'Mozila');
                $html = SimpleHtmlDom::str_get_html($html['content']);
                if ($html) {
                    foreach ($html->find('div[class=box]') as $div) {
                        $header = $div->find('div[class=table-header]');
                        if (!empty($header) && strpos($header[0]->plaintext, 'earch results for players')) {
                            $tr = $div->find('tr')[1];
                            $this->$team['players'][$key]['market_value'] = $tr->find('td', 8)->plaintext;
                        }
                    }
                }
            }
        }
    }

    private function abi_get_url_object($url, $user_agent=null)
    {
        $TIME_START = explode(' ', microtime());
        $TRY_ID = 0;
        $URL_RESULT = false;
        do
        {
            //--- parse URL ---
            $URL_PARTS = @parse_url($url);
            if( !is_array($URL_PARTS))
            {
                break;
            };
            $URL_SCHEME = ( isset($URL_PARTS['scheme']))?$URL_PARTS['scheme']:'http';
            $URL_HOST = ( isset($URL_PARTS['host']))?$URL_PARTS['host']:'';
            $URL_PATH = ( isset($URL_PARTS['path']))?$URL_PARTS['path']:'/';
            $URL_PORT = ( isset($URL_PARTS['port']))?intval($URL_PARTS['port']):80;
            if( isset($URL_PARTS['query']) && $URL_PARTS['query']!='' )
            {
                $URL_PATH .= '?'.$URL_PARTS['query'];
            };
            $URL_PORT_REQUEST = ( $URL_PORT == 80 )?'':":$URL_PORT";
            //--- build GET request ---
            $USER_AGENT = ( $user_agent == null )?'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)':strval($user_agent);
            $GET_REQUEST = "GET $URL_PATH HTTP/1.0\r\n"
                ."Host: $URL_HOST$URL_PORT_REQUEST\r\n"
                ."Accept: text/plain\r\n"
                ."Accept-Encoding: identity\r\n"
                ."User-Agent: $USER_AGENT\r\n\r\n";
            //--- open socket ---
            $SOCKET_TIME_OUT = 30;
            $SOCKET = @fsockopen($URL_HOST, $URL_PORT, $ERROR_NO, $ERROR_STR, $SOCKET_TIME_OUT);
            if( $SOCKET )
            {
                if( fputs($SOCKET, $GET_REQUEST))
                {
                    socket_set_timeout($SOCKET, $SOCKET_TIME_OUT);
                    //--- read header ---
                    $header = '';
                    $SOCKET_STATUS = socket_get_status($SOCKET);
                    while( !feof($SOCKET) && !$SOCKET_STATUS['timed_out'] )
                    {
                        $temp = fgets($SOCKET, 128);
                        if( trim($temp) == '' ) break;
                        $header .= $temp;
                        $SOCKET_STATUS = socket_get_status($SOCKET);
                    };
                    //--- get server code ---
                    if( preg_match('~HTTP\/(\d+\.\d+)\s+(\d+)\s+(.*)\s*\\r\\n~si', $header, $res))
                        $SERVER_CODE = $res[2];
                    else
                        break;
                    if( $SERVER_CODE == self::ABI_URL_STATUS_OK )
                    {
                        //--- read content ---
                        $content = '';
                        $SOCKET_STATUS = socket_get_status($SOCKET);
                        while( !feof($SOCKET) && !$SOCKET_STATUS['timed_out'] )
                        {
                            $content .= fgets($SOCKET, 1024*8);
                            $SOCKET_STATUS = socket_get_status($SOCKET);
                        };
                        //--- time results ---
                        $TIME_END = explode(' ', microtime());
                        $TIME_TOTAL = ($TIME_END[0]+$TIME_END[1])-($TIME_START[0]+$TIME_START[1]);
                        //--- output ---
                        $URL_RESULT['header'] = $header;
                        $URL_RESULT['content'] = $content;
                        $URL_RESULT['time'] = $TIME_TOTAL;
                        $URL_RESULT['description'] = '';
                        $URL_RESULT['keywords'] = '';
                        //--- title ---
                        $URL_RESULT['title'] =( preg_match('~<title>(.*)<\/title>~U', $content, $res))?strval($res[1]):'';
                        //--- meta tags ---
                        if( preg_match_all('~<meta\s+name\s*=\s*["\']?([^"\']+)["\']?\s+content\s*=["\']?([^"\']+)["\']?[^>]+>~', $content, $res, PREG_SET_ORDER) > 0 )
                        {
                            foreach($res as $meta)
                                $URL_RESULT[strtolower($meta[1])] = $meta[2];
                        };
                    }
                    elseif( $SERVER_CODE == self::ABI_URL_STATUS_REDIRECT_301 || $SERVER_CODE == self::ABI_URL_STATUS_REDIRECT_302 )
                    {
                        if( preg_match('~location\:\s*(.*?)\\r\\n~si', $header, $res))
                        {
                            $REDIRECT_URL = rtrim($res[1]);
                            $URL_PARTS = @parse_url($REDIRECT_URL);
                            if( isset($URL_PARTS['scheme'])&& isset($URL_PARTS['host']))
                                $url = $REDIRECT_URL;
                            else
                                $url = $URL_SCHEME.'://'.$URL_HOST.'/'.ltrim($REDIRECT_URL, '/');
                        }
                        else
                        {
                            break;
                        };
                    };
                };// GET request is OK
                fclose($SOCKET);
            }// socket open is OK
            else
            {
                break;
            };
            $TRY_ID++;
        }
        while( $TRY_ID <= self::MAX_REDIRECTS_NUM && $URL_RESULT === false );
        return $URL_RESULT;
    }
}