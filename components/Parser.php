<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 19.05.2018
 * Time: 8:04
 */

namespace app\components;

use app\models\Match;
use app\models\MatchesLineups;
use app\models\MatchMarks;
use app\models\Player;
use app\models\ReplacementMatches;
use app\models\Select;
use app\models\Team;
use yii\data\ArrayDataProvider;

class Parser
{
    const SOCCERWAY_URL = 'https://us.soccerway.com/';

    private static $firstLvl = [
        'competitions/club-domestic/' => 'CLUB DOMESTIC',
        'competitions/club-international/' => 'CLUB INTERNATIONAL',
        'competitions/national/' => 'NATIONAL TEAMS',
    ];

    private $teams = [];
    private $matchUrl;
    private $countMatchHistory;
    private $matchInfo = [];
    private $team_1 = [];
    private $team_2 = [];
    private $transfermarkt;
    private $lineups_id;

    public function __construct($matchUrl, $countMatchHistory, $transfermarkt, $lineups, $replacement)
    {
        $this->matchUrl = self::SOCCERWAY_URL . $matchUrl;
        $this->matchInfo['url'] = $matchUrl;
        $this->countMatchHistory = $countMatchHistory;
        $this->transfermarkt = $transfermarkt;
        $this->lineups_id = $lineups;
        $this->replacement_id = $replacement;
    }

    public function getMatchInfo()
    {
        $html = SimpleHtmlDom::file_get_html($this->matchUrl);
        $html = $html->find('div[class=content-column]')[0];

        $explodeUrl = explode('/', $this->matchUrl);
        $this->matchInfo['soccerway_url'] = $this->matchUrl;
        $this->matchInfo['site_id'] = $explodeUrl[count($explodeUrl) - 2];

        $this->matchInfo['team_1'] = trim($html->find('[class=thick]', 0)->plaintext);
        $this->matchInfo['team_1_logo'] = $html->find('div[class=logo] img', 0)->src;

        $this->matchInfo['date_game'] = $html->find('[class=timestamp]', 0)->getAttribute('data-value');
        $this->matchInfo['goalsOrTime'] = (strpos($html->find('[class=thick]', 1)->plaintext, '-')) ? $html->find('[class=thick]', 1)->plaintext : (new \DateTime('@'.$this->matchInfo['date_game']))->setTimezone((new \DateTimeZone(\Yii::$app->user->identity->timezone)))->format('H:i');
        $this->teams[1]['url'] = $html->find('[class=thick] a', 0)->href;
        $explodeUrl = explode('/', $html->find('[class=thick] a', 0)->href);
        $this->teams[1]['id'] = $this->matchInfo['id_club_1'] = $explodeUrl[count($explodeUrl) - 2];

        $this->matchInfo['team_2'] = trim($html->find('[class=thick]', -1)->plaintext);
        $this->matchInfo['team_2_logo'] = $html->find('div[class=logo] img', 1)->src;
        $this->teams[2]['url'] = $html->find('[class=thick] a', -1)->href;
        $explodeUrl = explode('/', $html->find('[class=thick] a', -1)->href);
        $this->teams[2]['id'] = $this->matchInfo['id_club_2'] = $explodeUrl[count($explodeUrl) - 2];

        $detailsContainers = $html->find('div[class=container middle] div[class=details clearfix]');
        $this->matchInfo['venue'] = $detailsContainers[count($detailsContainers)-1]->find('dd', 0)->plaintext;
        $this->matchInfo['venue'] = trim(explode('(' ,$this->matchInfo['venue'])[0]);

        $topClerfix = $html->find('div[id=page_match_1_block_match_info_4] div[class=clearfix]', 0);
        $team1HistoryMarks = $topClerfix->find('div[class=container left] div[class=form clearfix]', 0);
        $this->getHistoryMarks($team1HistoryMarks, 'team_1');
        $team2HistoryMarks = $topClerfix->find('div[class=container right] div[class=form clearfix]', 0);
        $this->getHistoryMarks($team2HistoryMarks, 'team_2');

        $match = Match::findOne(['site_id' => $this->matchInfo['site_id']]);
        if (!$match) {
            $match = new Match();
        }

        $match->setAttributes($this->matchInfo);
        $match->date_save = time();
        $match->save();
        $this->matchInfo['id'] = $match->id;

        $this->getTeamInfo(1);
        $this->getTeamInfo(2);

        $this->matchInfo['neutral_venue'] = ($this->teams[1]['venue'] != $this->matchInfo['venue']);

        $this->team_1 = $this->getCommandPlayers($this->teams[1]['id']);
        $this->team_2 = $this->getCommandPlayers($this->teams[2]['id']);

        $this->getLastH2H();
        $this->getMatchHistory();
        if ($this->lineups_id) $this->setFreshLineups();
        if ($this->replacement_id) $this->setIncidentsMarks();

        if ($this->team_1) {
            $info['team_1'] = new ArrayDataProvider([
                'allModels' => $this->team_1,
                'sort' => [
                    'attributes' => [
                        'minutes_played' => [
                            'default' => SORT_DESC
                        ],
                    ],
                    'defaultOrder' => [
                        'minutes_played' => SORT_DESC
                    ],
                ],
                'pagination' => [
                    'pageSize' => 100,
                ],
            ]);
        } else {
            $info['team_1'] = false;
        }

        if ($this->team_2) {
            $info['team_2'] = new ArrayDataProvider([
                'allModels' => $this->team_2,
                'sort' => [
                    'attributes' => [
                        'minutes_played' => [
                            'default' => SORT_DESC
                        ],
                    ],
                    'defaultOrder' => [
                        'minutes_played' => SORT_DESC
                    ],
                ],
                'pagination' => [
                    'pageSize' => 0,
                ],
            ]);
        } else {
            $info['team_2'] = false;
        }

        $info['matchInfo'] = $this->matchInfo;

        return $info;
    }

    private function getHistoryMarks($html, $team)
    {
        $this->matchInfo[$team.'_history_marks'] = [];
        foreach ($html->find('a') as $a) {
            $class = $a->getAttribute('class');
            if (strpos($class, 'form-win')) $this->matchInfo[$team.'_history_marks'][] = 'win';
            if (strpos($class, 'form-draw')) $this->matchInfo[$team.'_history_marks'][] = 'draw';
            if (strpos($class, 'form-loss')) $this->matchInfo[$team.'_history_marks'][] = 'loss';
        }
    }

    public function getCommandPlayers($id)
    {
        $url = self::SOCCERWAY_URL . '/a/block_team_squad?block_id=page_team_1_block_team_squad_8&callback_params=%7B%22team_id%22%3A%22'.$id.'%22%7D&action=changeView&params=%7B%22view%22%3A1%7D';
        $json = file_get_contents($url);
        $table = json_decode($json, true);
        $table = SimpleHtmlDom::str_get_html($table['commands'][0]['parameters']['content']);

        $players = [];
        if (!$table) return false;
        foreach ($table->find('tbody tr[class!=sub-head]') as $tr) {
            $playerId = $tr->getAttribute('data-people_id');
            $players[$playerId]['flag'] = $tr->find('td[class=flag] span')[0]->class;
            $players[$playerId]['name'] = $tr->find('td[class=name large-link] a')[0]->plaintext;
            $players[$playerId]['url'] = $tr->find('td[class=name large-link] a')[0]->href;
            $players[$playerId]['minutes_played'] = $tr->find('td[class=game-minutes]')[0]->plaintext  ?? '-';
            $players[$playerId]['appearances'] = $tr->find('td[class=appearances]')[0]->plaintext;
            $players[$playerId]['lineups'] = $tr->find('td[class=lineups]')[0]->plaintext;
            $players[$playerId]['age'] = $tr->find('td[class=age]')[0]->plaintext;
            $players[$playerId]['position'] = $tr->find('td[class=position]')[0]->plaintext;
            $players[$playerId]['subs_on_bench'] = $tr->find('td[class=subs-on-bench]')[0]->plaintext;
            $players[$playerId]['goals'] = $tr->find('td[class=goals]')[0]->plaintext;
            $players[$playerId]['assists'] = $tr->find('td[class=assists]')[0]->plaintext ?? 0;
            $players[$playerId]['yellow_cards'] = $tr->find('td[class=yellow-cards]')[0]->plaintext;
            $players[$playerId]['nd_yellow_cards'] = $tr->find('td[class=2nd-yellow-cards]')[0]->plaintext;
            $players[$playerId]['red_cards'] = $tr->find('td[class=red-cards]')[0]->plaintext;
            $players[$playerId]['shirtnumber'] = $tr->find('td[class=shirtnumber]')[0]->plaintext ?? '-';
            $players[$playerId]['position'] = $tr->find('td[class=position]')[0]->plaintext;
            $players[$playerId]['market_value'] = '-';

            $players[$playerId]['tooltip_name'] = '';
            $tooltip_name = explode('-', explode('/', $players[$playerId]['url'])[2]);
            foreach ($tooltip_name as $part_name) {
                $players[$playerId]['tooltip_name'] .= ucfirst($part_name) . ' ';
            }
            $players[$playerId]['tooltip_name'] = trim($players[$playerId]['tooltip_name']);

            $players[$playerId]['names'] = [$players[$playerId]['name'], $players[$playerId]['tooltip_name']];


            $player = Player::findOne(['player_id' => $playerId]);
            if (!$player) {
                $player = new Player();
            } else {
                foreach (preg_split('/[ ]*,/', $player->names) as $name) {
                    if ($name != '') {
                        $players[$playerId]['names'][] = trim($name);

                    }
                }
            }

            $players[$playerId]['names'] = array_unique($players[$playerId]['names']);
            $player->setAttributes($players[$playerId]);
            $player->player_id = $playerId;
            $player->names = $players[$playerId]['namesStr'] = implode(',', array_unique($players[$playerId]['names']));
            $player->save();

            $players[$playerId]['id'] = $player->id;
        }

        return $players;
    }

    public function getLastH2H()
    {
        if (!$this->team_1 || !$this->team_2) return;

        $urlH2H = explode('?', $this->matchUrl)[0] . 'head2head/';
        $html = SimpleHtmlDom::file_get_html($urlH2H);
        $table = $html->find('table tbody', 0);
        foreach ($table->find('tr') as $e){
            $date = $e->getAttribute('data-timestamp');
            if ($date && $date < (time() + 108000) && $date != $this->matchInfo['date_game']){
                $lastMatchUrl = $e->find('td[class=score-time] a', 0)->href;
                $date = (new \DateTime('@'.$date))->format('d.m');
                $this->matchInfo['h2h_competition'] = $e->find('td[class=competition] a', 0)->plaintext;
                $this->matchInfo['h2h_date'] = $date;
                break;
            }
        }

        if (!isset($lastMatchUrl)) return;

        $lastMatchUrl = self::SOCCERWAY_URL . $lastMatchUrl;
        $html = SimpleHtmlDom::file_get_html($lastMatchUrl);

        $containers = $html->find('div[class=combined-lineups-container]');

        if (empty($containers)) return;

        foreach ($containers[1]->find('td[class=player] a') as $player){ //substitutions
            $explodeUrl = explode('/', $player->href);
            $id = $explodeUrl[count($explodeUrl) - 2];

            if (array_key_exists($id, $this->team_1)) {
                $this->team_1[$id]['h2h'] = 2;
            }

            if (array_key_exists($id, $this->team_2)) {
                $this->team_2[$id]['h2h'] = 2;
            }
        }

        foreach ($containers[0]->find('td[class=player] a') as $player){ //lineups
            $explodeUrl = explode('/', $player->href);
            $id = $explodeUrl[count($explodeUrl) - 2];

            if (array_key_exists($id, $this->team_1)) {
                $this->team_1[$id]['h2h'] = 1;
            }

            if (array_key_exists($id, $this->team_2)) {
                $this->team_2[$id]['h2h'] = 1;
            }
        }
    }

    public function getMatchHistory()
    {
        if (!$this->team_1 && !$this->team_2) return;

        $html = SimpleHtmlDom::file_get_html($this->matchUrl);
        $team1Container = $html->find('div[id=page_match_1_block_match_team_matches_14-wrapper]', 0);
        $team2Container = $html->find('div[id=page_match_1_block_match_team_matches_15-wrapper]', 0);

        if (!$team1Container) {
            $team1Container = $html->find('div[id=page_match_1_block_match_team_matches_15-wrapper]', 0);
            $team2Container = $html->find('div[id=page_match_1_block_match_team_matches_16-wrapper]', 0);
        }

        if (!$team1Container) {
            $team1Container = $html->find('div[id=page_match_1_block_match_team_matches_9-wrapper]', 0);
            $team2Container = $html->find('div[id=page_match_1_block_match_team_matches_10-wrapper]', 0);
        }

        $team_1_link = $team1Container->find('h2 a', 0)->href;
        $team_2_link = $team2Container->find('h2 a', 0)->href;

        $team_1_history = [];
        $team_1_table = SimpleHtmlDom::file_get_html(self::SOCCERWAY_URL . $team_1_link)->find('table[class=matches]', 0);
        $mathces = array_reverse($team_1_table->find('td[class=score]'));
        foreach ($mathces as $td) {
            $tr = $td->parent();
            $date = (new \DateTime('@'.$tr->getAttribute('data-timestamp')))->format('d.m');
            $competition = $tr->find('td[class=competition] a', 0)->plaintext;
            $this->matchInfo['team_1_history_dates'][] = $date;
            $this->matchInfo['team_1_history_competition'][] = $competition;
            $team_1_history[] = $td->find('a', 0)->href;
        }

        $team_2_history = [];
        $team_2_table = SimpleHtmlDom::file_get_html(self::SOCCERWAY_URL . $team_2_link)->find('table[class=matches]', 0);
        $mathces = array_reverse($team_2_table->find('td[class=score]'));
        foreach ($mathces as $td) {
            $tr = $td->parent();
            $date = (new \DateTime('@'.$tr->getAttribute('data-timestamp')))->format('d.m');
            $competition = $tr->find('td[class=competition] a', 0)->plaintext;
            $this->matchInfo['team_2_history_dates'][] = $date;
            $this->matchInfo['team_2_history_competition'][] = $competition;
            $team_2_history[] = $td->find('a', 0)->href;
        }

        if (!empty($team_1_history)) {
            $i = 0;
            while ($i < $this->countMatchHistory && isset($team_1_history[$i])) {
                $lastMatchUrl = self::SOCCERWAY_URL . $team_1_history[$i];
                $html = SimpleHtmlDom::file_get_html($lastMatchUrl);
                $containers = $html->find('div[class=combined-lineups-container]');

                if (empty($containers)) {
                    $i++;
                    continue;
                }

                foreach ($containers[1]->find('td[class=player] a') as $player){ //substitutions
                    $explodeUrl = explode('/', $player->href);
                    $id = $explodeUrl[count($explodeUrl) - 2];

                    if (array_key_exists($id, $this->team_1)) {
                        $this->team_1[$id]['mh'.$i] = 2;
                    }
                }

                foreach ($containers[0]->find('td[class=player] a') as $player){ //lineups
                    $explodeUrl = explode('/', $player->href);
                    $id = $explodeUrl[count($explodeUrl) - 2];

                    if (array_key_exists($id, $this->team_1)) {
                        $this->team_1[$id]['mh'.$i] = 1;
                    }
                }
                $i++;
            }
        }


        if (!empty($team_2_history)) {
            $i = 0;
            while ($i < $this->countMatchHistory && isset($team_1_history[$i])) {
                $lastMatchUrl = self::SOCCERWAY_URL . $team_2_history[$i];
                $html = SimpleHtmlDom::file_get_html($lastMatchUrl);
                $containers = $html->find('div[class=combined-lineups-container]');

                if (empty($containers)) {
                    $i++;
                    continue;
                }

                foreach ($containers[1]->find('td[class=player] a') as $player) { //substitutions
                    $explodeUrl = explode('/', $player->href);
                    $id = $explodeUrl[count($explodeUrl) - 2];

                    if (array_key_exists($id, $this->team_2)) {
                        $this->team_2[$id]['mh' . $i] = 2;
                    }
                }

                foreach ($containers[0]->find('td[class=player] a') as $player) { //lineups
                    $explodeUrl = explode('/', $player->href);
                    $id = $explodeUrl[count($explodeUrl) - 2];

                    if (array_key_exists($id, $this->team_2)) {
                        $this->team_2[$id]['mh' . $i] = 1;
                    }
                }
                $i++;
            }
        }
    }

    private function setFreshLineups()
    {
        $match = MatchesLineups::findOne($this->lineups_id);
        if (!is_null($match->players_json)){
            $playersArr = json_decode($match->players_json, true);
            if (!empty($this->team_1)) {
                foreach ($playersArr['team_1'] as $name => $arr) {
                    foreach ($this->team_1 as $id => $player) {
                        if (isset($player['names']) && !empty($player['names'])) {
                            if (in_array($name, $player['names'])) {
                                $this->team_1[$id]['fl'] = $arr['mark'];
                                break;
                            }
                        }
                        if (LineupsMatchParser::normolizeName($player['name']) == $name) {
                            $this->team_1[$id]['fl'] = $arr['mark'];
                            break;
                        }
                    }
                }
            }

            if (!empty($this->team_2)) {
                foreach ($playersArr['team_2'] as $name => $arr) {
                    foreach ($this->team_2 as $id => $player) {
                        if (isset($player['names']) && !empty($player['names'])) {
                            if (in_array($name, $player['names'])) {
                                $this->team_2[$id]['fl'] = $arr['mark'];
                                break;
                            }
                        }
                        if (LineupsMatchParser::normolizeName($player['name']) == $name) {
                            $this->team_2[$id]['fl'] = $arr['mark'];
                            break;
                        }
                    }
                }
            }

            \Yii::$app->db->createCommand()
                ->update('user2matches_lineups', ['status' => 3], ['user_id' => \Yii::$app->user->id, 'match_id' => $match->id])
                ->execute();
        }
    }

    private function setIncidentsMarks()
    {
        $match = ReplacementMatches::findOne($this->replacement_id);
        if (!is_null($match->team_1_incidents)) {
            $playersArr = json_decode($match->team_1_incidents, true)['players_marks'];
            foreach ($playersArr as $name => $mark) {
                foreach ($this->team_1 as $id => $player) {
                    if (isset($player['names']) && !empty($player['names'])) {
                        if (in_array($name, $player['names'])) {
                            $this->team_1[$id]['rp'] = $mark;
                            break;
                        }
                    }
                    if (LineupsMatchParser::normolizeName($player['name']) == $name) {
                        $this->team_1[$id]['rp'] = $mark;
                        break;
                    }
                }
            }
        }

        if (!is_null($match->team_2_incidents)) {
            $playersArr = json_decode($match->team_2_incidents, true)['players_marks'];
            foreach ($playersArr as $name => $mark) {
                foreach ($this->team_2 as $id => $player) {
                    if (isset($player['names']) && !empty($player['names'])) {
                        if (in_array($name, $player['names'])) {
                            $this->team_2[$id]['rp'] = $mark;
                            break;
                        }
                    }
                    if (LineupsMatchParser::normolizeName($player['name']) == $name) {
                        $this->team_2[$id]['rp'] = $mark;
                        break;
                    }
                }
            }
        }

        \Yii::$app->db->createCommand()
            ->update('user2teams_replacements', ['status' => 3], ['user_id' => \Yii::$app->user->id, 'replacement_match_id' => $this->replacement_id])
            ->execute();
    }

    private function getTeamInfo($team)
    {
        $html = SimpleHtmlDom::file_get_html(self::SOCCERWAY_URL . $this->teams[$team]['url']);
        $teamObj = Team::findOne(['soccerway_id' => $this->teams[$team]['id']]);
        if ($teamObj && !is_null($teamObj->team_site)) {
            $this->matchInfo['team_'.$team.'_site'] = $teamObj->team_site;
        } else {
            $siteLink = $html->find('p[class=center website] a', 0);

            $this->matchInfo['team_' . $team . '_site'] = ($siteLink) ? str_replace(['http://', 'https://'], '',  $siteLink->getAttribute('href')) : '';
        }

        $cityStr = $html->find('div[id=page_team_1_block_team_venue_4] dd', 1);
        $this->matchInfo['team_'.$team.'_city'] = ($cityStr) ? trim($cityStr->plaintext) : '';

        if ($team == 1) {
            $venueStr = $html->find('div[id=page_team_1_block_team_venue_4] dd', 0);
            $this->teams[$team]['venue'] = ($venueStr) ? trim($venueStr->plaintext) : '';
        }
    }

    public static function getBetexplorerLink($team_1, $team_2, $date)
    {
        $date = new \DateTime('@'.$date);
        $url = 'http://www.betexplorer.com/next/soccer/?year='. $date->format('Y') .'&month=' . $date->format('n') . '&day=' . $date->format('d');

        $options = array(
            "http"=>array(
                "header"=>"
                User-Agent: Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.102011-10-16 20:23:10\r\n
                "
            )
        );

        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        $html = SimpleHtmlDom::str_get_html($result);
        $html = $html->find('table[class=table-matches]', 0);
        $thisMatch = $team_1 . ' - ' . $team_2;
        foreach ($html->find('tr[data-dt]') as $tr) {
            $match = trim(preg_replace("/\s{2,}/",' ', $tr->find('a', 0)->plaintext));
            if ($match == $thisMatch) {
                return $tr->find('a', 0)->href;
            }
        }
        return '';
    }

    public static function getOddsLink($team_1, $team_2, $date)
    {
        $thisMatch = $team_1 . ' - ' . $team_2;
        $url = 'http://www.oddsportal.com/search/' . urlencode($thisMatch) . '/soccer/';

        $options = array(
            "http"=>array(
                "header"=>"
                Cookie: op_user_time_zone=0\r\n
                "
            )
        );

        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        $html = SimpleHtmlDom::str_get_html($result);
        $html = $html->find('div[id=col-content]', 0);
        $searchAttr = 't' . $date . '-8-1-0-0';
        foreach ($html->find('tr[xeid]') as $tr) {
            if ($tr->find('td[class=' . $searchAttr . ']', 0)) {
                $match = trim(preg_replace("/\s{2,}/", ' ', $tr->find('a[!class]', 0)->plaintext));
                if ($match == $thisMatch) {
                    return $tr->find('a[!class]', 0)->href;
                }
            }
        }
        return '';
    }

    public static function updateSelect(){
        $selectArr = [];
        $id = 0;
        foreach (self::$firstLvl as $url => $value) {
            $id++;
            $firstLvlId = $id;
            $selectArr[] = [$id, $value, $url, null, null];

            $address = self::SOCCERWAY_URL . $url;
            $html = SimpleHtmlDom::file_get_html($address);
            foreach ($html->find('div[class=row] a') as $element) {
                $id++;
                $secondLvlId = $id;
                $url = implode('/', explode('/', $element->href, -2));
                $urlArr = explode('/', $element->href);
                $site_id = $urlArr[count($urlArr) - 2];
                $selectArr[] = [$id, trim($element->innertext), $url, $site_id, $firstLvlId];

                $uid_category = substr(strrchr(substr($element->href, 0, -1), 47), 2);
                if ($value == 'CLUB DOMESTIC') {
                    $rrg = '/a/block_competitions_index_club_domestic?block_id=page_competitions_1_block_competitions_index_club_domestic_4&callback_params=%7B%22level%22%3A1%7D&action=expandItem&params=%7B%22area_id%22%3A%22' . $uid_category . '%22%2C%22level%22%3A2%2C%22item_key%22%3A%22area_id%22%7D';
                } elseif ($value == 'CLUB INTERNATIONAL') {
                    $rrg = '/a/block_competitions_index_club_international?block_id=page_competitions_1_block_competitions_index_club_international_4&callback_params=%7B%22level%22%3A1%7D&action=expandItem&params=%7B%22area_id%22%3A%22'. $uid_category .'%22%2C%22level%22%3A2%2C%22item_key%22%3A%22area_id%22%7D';
                } elseif ($value == 'NATIONAL TEAMS') {
                    $rrg = '/a/block_competitions_index_national?block_id=page_competitions_1_block_competitions_index_national_4&callback_params=%7B%22level%22%3A1%7D&action=expandItem&params=%7B%22area_id%22%3A%22'. $uid_category .'%22%2C%22level%22%3A2%2C%22item_key%22%3A%22area_id%22%7D';
                }

                $json_adr = self::SOCCERWAY_URL . $rrg;
                $json_ = file_get_contents($json_adr);

                $obj = json_decode($json_);

                $json_content = SimpleHtmlDom::str_get_html($obj->{'commands'}[0]->{'parameters'}->{'content'});

                foreach ($json_content->find('a') as $element2) {
                    $id++;
                    $url = implode('/', explode('/', $element2->href, -2));
                    $urlArr = explode('/', $element2->href);
                    $site_id = $urlArr[count($urlArr) - 2];
                    $selectArr[] = [$id, trim($element2->plaintext), $url, $site_id, $secondLvlId];
                }
            }
        }


        $db = \Yii::$app->db;
        $transaction = $db->beginTransaction();

        try {
            Select::deleteAll();
            $db->createCommand()->batchInsert('select', ['id', 'name', 'url', 'site_id', 'parent_id'], $selectArr)->execute();
            $transaction->commit();
        } catch(\Throwable $e) {
            $transaction->rollBack();
            echo $e->getCode() . ' ' . $e->getLine() . ' ' . $e->getMessage();
        }
    }

    public static function getMatches($url)
    {
        $html = SimpleHtmlDom::file_get_html(self::SOCCERWAY_URL . $url);
        $level_2 = $html->find('ul[class=level-2 expanded]', 0);
        $resa = '';
        if (!$level_2) {
            $html = $html->find('div[id=submenu] li', 1);
            $url = $html->find('a', 0)->href;
            $html = SimpleHtmlDom::file_get_html(self::SOCCERWAY_URL . $url);

            foreach ($html->find('tr[data-timestamp]') as $tr) {
                if ($tr->getAttribute('data-timestamp') + 10800 > time()) {
                    $team_1 = $tr->find('td[class=team-a]', 0)->plaintext;
                    $team_2 = $tr->find('td[class=team-b]', 0)->plaintext;
                    $time_score = $tr->find('td[class=score-time]', 0)->plaintext;
                    $match_link = $tr->find('td[class=score-time] a', 0)->href;
                    $date = (new \DateTime('@' . $tr->getAttribute('data-timestamp')))->setTimezone((new \DateTimeZone(\Yii::$app->user->identity->timezone)));
                    $time_score = (strpos($time_score, '-')) ? $time_score : $date->format('H:i');
                    if (explode('?', $match_link)[0] == '') {
                        $resa .= '<li>' . $team_1 . ' ' . $time_score . ' ' . $team_2 . ' | ' . $date->format('Y-m-d') . '</li>';
                    } else {
                        $resa .= '<li><a data-url="' . $match_link . '" href="/?matchUrl=' . $match_link . '">' . $team_1 . ' ' . $time_score . ' ' . $team_2 . '</a> | ' . $date->format('Y-m-d') . '</li>';
                    }
                }
            }
        } else {
            $level_2 = $level_2->find('li[class=leaf] a');
            foreach ($level_2 as $a) {
                $html = SimpleHtmlDom::file_get_html(self::SOCCERWAY_URL . $a->href . 'matches/');
                $matches = '';
                foreach ($html->find('tr[data-timestamp]') as $tr) {
                    if ($tr->getAttribute('data-timestamp') + 10800 > time()) {
                        $team_1 = $tr->find('td[class=team-a]', 0)->plaintext;
                        $team_2 = $tr->find('td[class=team-b]', 0)->plaintext;
                        $time_score = $tr->find('td[class=score-time]', 0)->plaintext;
                        $match_link = $tr->find('td[class=score-time] a', 0)->href;
                        $date = (new \DateTime('@' . $tr->getAttribute('data-timestamp')))->setTimezone((new \DateTimeZone(\Yii::$app->user->identity->timezone)));
                        $time_score = (strpos($time_score, '-')) ? $time_score : $date->format('H:i');
                        if (explode('?', $match_link)[0] == '') {
                            $matches .= '<li>' . $team_1 . ' ' . $time_score . ' ' . $team_2 . ' | ' . $date->format('Y-m-d') . '</li>';
                        } else {
                            $matches .= '<li><a data-url="' . $match_link . '" href="/?matchUrl=' . $match_link . '">' . $team_1 . ' ' . $time_score . ' ' . $team_2 . '</a> | ' . $date->format('Y-m-d') . '</li>';
                        }
                    }
                }
                if ($matches != '') {
                    $resa .= '<ul><h4>' . $a->plaintext . '</h4>' . $matches . '</ul>';
                }
            }
        }
        if ($resa == '') {
            $resa = '<p>Нет матчей в ближайшее время</p>';
        } else {
            $resa = '<ul>' . $resa . '</ul>';
        }
        return $resa;
    }
}